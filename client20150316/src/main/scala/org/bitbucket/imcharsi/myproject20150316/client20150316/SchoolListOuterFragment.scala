/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.app.ActionBar.{ Tab, TabListener }
import android.app.{ ActionBar, Fragment, FragmentTransaction }
import android.os.Bundle
import android.view.{ LayoutInflater, View, ViewGroup }
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.SchoolType

/**
 * Created by i on 3/26/15.
 */
class SchoolListOuterFragment extends Fragment {
  val schoolTypeList: List[(Int, String, Int)] = List(
    (R.string.textAllSchool, SchoolListFragment.TAG_ALL, SchoolType.unknown),
    (R.string.textElementarySchool, SchoolListFragment.TAG_ELEMENTARY, SchoolType.elementarySchool),
    (R.string.textMiddleSchool, SchoolListFragment.TAG_MIDDLE, SchoolType.middleSchool),
    (R.string.textHighSchool, SchoolListFragment.TAG_HIGH, SchoolType.highSchool))
  // 아래의 상태를 사용해야 기계를 회전시켜도 바르게 동작한다. 문제는,
  // 첫번째 tab 이 아닌 tab 을 보고 있을 때 기계를 회전시키면, 항상 첫번째 tab 을 선택하도록 했어도 첫번째 tab 조차도 나오지 않는다는 것이다.
  // 이와 같이 상태를 두면, 회전뿐만 아니라 back stack 에 의해 가려졌다가 다시 드러나도 tab 이 유지된다.
  private var selectedTab: Option[Int] = None

  // 아무리 해도 잘 안돼서 아래를 참고했다. 한번은 SchoolListFragment 열어서 기계를 회전시킬 때마다 갱신이 되고,
  // 닫았다가 다시 열어서 기계를 회전시키면 갱신이 안되고 또 다시 닫았다가 다시 열어서 회전시키견 또 되는 이상한 문제가 있었다.
  // 결론은, add/attach/detach 를 써야 하는 상황에서 replace/replace/remove 를 썼다.
  // http://wptrafficanalyzer.in/blog/adding-navigation-tabs-containing-listview-to-action-bar-in-android/

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    setHasOptionsMenu(true)
    selectedTab = Option(savedInstanceState).map(_.getInt(SchoolListOuterFragment.selectedTab))
  }

  override def onSaveInstanceState(outState: Bundle): Unit = {
    super.onSaveInstanceState(outState)
    selectedTab.foreach(outState.putInt(SchoolListOuterFragment.selectedTab, _))
  }

  override def onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle): View = {
    super.onCreateView(inflater, container, savedInstanceState)
    inflater.inflate(R.layout.school_list_outer_fragment_layout, container, false)
  }

  override def onActivityCreated(savedInstanceState: Bundle): Unit = {
    super.onActivityCreated(savedInstanceState)
    // StudentListOuterFragment 로 갔다가 다시 되돌아 올때, StudentListOuterFragment 가 더했던 tab 들이 남아있다. 지워야 한다.
    getActivity.getActionBar.removeAllTabs()
    // 아래의 구절들이 onCreateView 에 있든, 여기에 있든 그것은 문제가 아니었다.
    getActivity.getActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS)
    schoolTypeList.foreach {
      case (id, tag, schoolType) =>
        val newTab = getActivity.getActionBar.newTab().setTabListener(new SchoolListTabListener).setText(id).setTag(schoolType)
        getActivity.getActionBar.addTab(newTab, false)
    }
    getActivity.getActionBar.selectTab(getActivity.getActionBar.getTabAt(selectedTab.getOrElse(0)))
  }

  private class SchoolListTabListener extends TabListener {
    private var fragment: Option[Fragment] = None

    override def onTabSelected(tab: Tab, ft: FragmentTransaction): Unit = {
      val map = schoolTypeList.map { case (_, a, b) => (b, a) }.toMap

      fragment = Option(tab.getTag).map(_.asInstanceOf[Int]).flatMap { schoolType =>
        Option(getFragmentManager.findFragmentByTag(map(schoolType))) match {
          case fragment @ Some(_) => fragment.chain(ft.attach(_))
          case None =>
            Some(new SchoolListFragment).chain { fragment =>
              Option(new Bundle()).
                chain(_.putInt(SchoolListFragment.searchCondition, schoolType)).
                foreach(fragment.setArguments(_))
            }.chain { fragment =>
              ft.add(R.id.school_list_outer_fragment_layout, fragment, map(schoolType))
            }
        }
      }

      selectedTab = Option(tab.getPosition)
    }

    override def onTabReselected(tab: Tab, ft: FragmentTransaction): Unit = {}

    override def onTabUnselected(tab: Tab, ft: FragmentTransaction): Unit = {
      fragment.foreach(ft.detach(_))
    }
  }
}

object SchoolListOuterFragment {
  val TAG = classOf[SchoolListOuterFragment].getName
  val selectedTab = "selectedTab"
}
