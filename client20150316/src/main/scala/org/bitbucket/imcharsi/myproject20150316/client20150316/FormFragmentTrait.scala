/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.app.Fragment
import android.content.AsyncQueryHandler
import android.net.Uri
import android.view.Menu
import android.widget.Toast

/**
 * Created by i on 4/8/15.
 */
trait FormFragmentTrait extends Fragment {
  val menuItemList = List(R.id.menuItemSubmit, R.id.menuItemCancel, R.id.menuItemRandom, R.id.menuItemDelete)
  protected var menu: Menu

  protected def processReadonlyMode(): Unit = {
    // 모든 form fragment 의 submit/delete option menu 를 상황에 따라 비활성시킨다.
    // 이 method 는 이 trait 를 사용하는 모든 fragment 의 option menu create 단계에서 사용되어야 한다. 누락이 없도록 주의하기.
    Option(getFragmentManager.findFragmentByTag(MainFragment.TAG)).map(_.asInstanceOf[MainFragment]).map(_.readonly).foreach { flag =>
      Option(menu.findItem(R.id.menuItemSubmit)).foreach(_.setEnabled(!flag)) // flag==true 이면 읽기전용이다. 따라서, !flag==true 일때 menu 가 활성화되어야 한다.
      Option(menu.findItem(R.id.menuItemDelete)).foreach(_.setEnabled(!flag))
    }
  }

  protected class SampleAsyncQueryHandler(checkId: () => Boolean) extends AsyncQueryHandler(getActivity.getContentResolver) {
    override def onInsertComplete(token: Int, cookie: scala.Any, uri: Uri): Unit = {
      super.onInsertComplete(token, cookie, uri)
      // submit 을 누르고 여기에 이르기 전의 사이 시간에 back button 을 누르면 null 예외가 생긴다. 아마도 이미 없어진 Fragment 를 기준으로 뭘 하려니 문제가 되는 듯 하다.
      menuItemList.filter(_ != R.id.menuItemDelete).map(id => menu.findItem(id)).foreach(_.setEnabled(true))
      menu.findItem(R.id.menuItemDelete).setEnabled(checkId())
      processReadonlyMode() // 애초에 읽기전용 방식에서는 이들 menu 가 실행될 일이 없기 때문에, 이들 menu 를 상황에 따라 비활성화시켜야 할 일도 없지만, 일관성을 위해 써두자.
      Option(uri) match {
        case Some(uri) =>
          Toast.makeText(getActivity, uri.toString, Toast.LENGTH_SHORT).show()
          getFragmentManager.popBackStack()
        case None => Toast.makeText(getActivity, getResources.getString(R.string.failure), Toast.LENGTH_SHORT).show()
      }
    }

    override def onDeleteComplete(token: Int, cookie: scala.Any, result: Int): Unit = {
      super.onDeleteComplete(token, cookie, result)
      menuItemList.filter(_ != R.id.menuItemDelete).map(id => menu.findItem(id)).foreach(_.setEnabled(true))
      menu.findItem(R.id.menuItemDelete).setEnabled(checkId())
      processReadonlyMode()
      result match {
        case 1 =>
          Toast.makeText(getActivity, getResources.getString(R.string.success), Toast.LENGTH_SHORT).show()
          getFragmentManager.popBackStack()
        case _ => Toast.makeText(getActivity, getResources.getString(R.string.failure), Toast.LENGTH_SHORT).show()
      }
    }

    override def onUpdateComplete(token: Int, cookie: scala.Any, result: Int): Unit = {
      super.onUpdateComplete(token, cookie, result)
      menuItemList.filter(_ != R.id.menuItemDelete).map(id => menu.findItem(id)).foreach(_.setEnabled(true))
      menu.findItem(R.id.menuItemDelete).setEnabled(checkId())
      processReadonlyMode()
      result match {
        case 1 =>
          Toast.makeText(getActivity, getResources.getString(R.string.success), Toast.LENGTH_SHORT).show()
          getFragmentManager.popBackStack()
        case _ => Toast.makeText(getActivity, getResources.getString(R.string.failure), Toast.LENGTH_SHORT).show()
      }
    }
  }

}
