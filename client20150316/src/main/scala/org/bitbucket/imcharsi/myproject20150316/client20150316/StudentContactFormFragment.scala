/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.app.Fragment
import android.content.{ AsyncQueryHandler, ContentValues }
import android.net.Uri
import android.os.Bundle
import android.view._
import android.widget.{ EditText, ToggleButton }
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.{ StudentContactModel, Version }

import scala.util.Random

/**
 * Created by i on 3/24/15.
 */
class StudentContactFormFragment extends Fragment with FormFragmentTrait with ReadonlyOptionTrait {
  private var studentContactModel: Option[StudentContactModel] = None
  private var editTextStudentContactId: Option[EditText] = None
  private var editTextStudentName: Option[EditText] = None
  private var editTextCallNumber: Option[EditText] = None
  private var editTextAddress: Option[EditText] = None
  private var toggleValidity: Option[ToggleButton] = None
  private var toggleCurrentStudentContact: Option[ToggleButton] = None
  private var asyncQueryHandler: AsyncQueryHandler = null
  protected override var menu: Menu = null

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    studentContactModel = Option(savedInstanceState).
      orElse(Option(getArguments)).
      filter(_.containsKey(StudentContactFormFragment.studentContactModel)).
      map(_.getSerializable(StudentContactFormFragment.studentContactModel)).
      map(_.asInstanceOf[StudentContactModel])
    asyncQueryHandler = new SampleAsyncQueryHandler(() => studentContactModel.flatMap(_.id).isDefined)
    setHasOptionsMenu(true)
  }

  override def onSaveInstanceState(outState: Bundle): Unit = {
    super.onSaveInstanceState(outState)
    studentContactModel.foreach(outState.putSerializable(StudentContactFormFragment.studentContactModel, _))
  }

  override def onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle): View = {
    super.onCreateView(inflater, container, savedInstanceState)
    Some(inflater.inflate(R.layout.student_contact_form_fragment_layout, container, false)).chain { view =>
      def a(m: Option[StudentContactModel]): Option[String] = m.flatMap(_.id).map(_.toString)
      def b(m: Option[StudentContactModel]): Option[String] = m.flatMap(_.student).flatMap(_.name)
      def c(m: Option[StudentContactModel]): Option[String] = m.flatMap(_.callNumber)
      def d(m: Option[StudentContactModel]): Option[String] = m.flatMap(_.address)
      def a1(v: Option[EditText]): Unit = editTextStudentContactId = v
      def b1(v: Option[EditText]): Unit = editTextStudentName = v
      def c1(v: Option[EditText]): Unit = editTextCallNumber = v
      def d1(v: Option[EditText]): Unit = editTextAddress = v
      // 그냥 이렇게 해봤다. 똑같은 문장을 반복할수도 있었다.
      List(
        (R.id.editTexStudentContactId, a(_)),
        (R.id.editTextStudentName, b(_)),
        (R.id.editTextCallNumber, c(_)),
        (R.id.editTextAddress, d(_))).map {
          case (a, b) =>
            Option(view.findViewById(a)).
              map(_.asInstanceOf[EditText]).
              chain { editText =>
                b(studentContactModel).foreach(editText.setText(_))
              }
        }.zip(List(a1(_), b1(_), c1(_), d1(_))).
        foreach {
          case (a, b) =>
            b(a)
        }
      toggleValidity = Option(view.findViewById(R.id.toggleValidity)).
        map(_.asInstanceOf[ToggleButton]).
        chain { toggle =>
          toggle.setChecked(studentContactModel.flatMap(_.validity).getOrElse(false))
        }
      toggleCurrentStudentContact = Option(view.findViewById(R.id.toggleCurrentStudentContact)).
        map(_.asInstanceOf[ToggleButton]).
        chain { toggle =>
          toggle.setChecked(studentContactModel.flatMap(_.currentStudentContact).getOrElse(false))
        }
    }.get
  }

  override def onActivityCreated(savedInstanceState: Bundle): Unit = {
    super.onActivityCreated(savedInstanceState)
    getActivity.getActionBar.setTitle(R.string.titleStudentContactFormFragment)
  }

  override def onCreateOptionsMenu(menu: Menu, inflater: MenuInflater): Unit = {
    super.onCreateOptionsMenu(menu, inflater)
    inflater.inflate(R.menu.submit_record, menu)
    menu.findItem(R.id.menuItemDelete).setEnabled(studentContactModel.flatMap(_.id).isDefined)
    this.menu = menu
    processReadonlyMode()
  }

  private def menuItemCancelListener(item: MenuItem): Boolean = {
    getFragmentManager.popBackStack()
    true
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    item.getItemId match {
      case R.id.menuItemRandom => menuItemRandomListener(item)
      case R.id.menuItemSubmit => menuItemSubmitListener(item)
      case R.id.menuItemDelete => menuItemDeleteListener(item)
      case R.id.menuItemCancel => menuItemCancelListener(item)
      case _ => super.onOptionsItemSelected(item)
    }
  }

  private def menuItemRandomListener(item: MenuItem): Boolean = {
    editTextCallNumber.foreach(_.setText(Random.nextInt().toString))
    editTextAddress.foreach(_.setText(Random.nextInt().toString))
    true
  }

  private def menuItemSubmitListener(item: MenuItem): Boolean = {
    menuItemList.map(menu.findItem(_)).foreach(_.setEnabled(false))
    studentContactModel.map(_.copy(
      callNumber = editTextCallNumber.map(_.getText).map(_.toString).filterNot(_.isEmpty),
      address = editTextAddress.map(_.getText).map(_.toString).filterNot(_.isEmpty),
      validity = toggleValidity.map(_.isChecked),
      currentStudentContact = toggleCurrentStudentContact.map(_.isChecked))).
      map(SampleObjectMapper.writeValueAsString(_)).
      flatMap { json =>
        Some(new ContentValues()).chain(_.put(StudentContactProvider.raw, json))
      }.foreach { values =>
        studentContactModel.flatMap(_.id) match {
          case None =>
            studentContactModel.map { model =>
              f"${StudentProvider.url}/${model.studentId.get}/studentContact"
            }.map(Uri.parse(_)).
              foreach(asyncQueryHandler.startInsert(Random.nextInt(), null, _, values))
          case Some(id) =>
            studentContactModel.map { model =>
              f"${StudentProvider.url}/${model.studentId.get}/studentContact/${id}"
            }.map(Uri.parse(_)).
              foreach(asyncQueryHandler.startUpdate(Random.nextInt(), null, _, values, null, null))
        }
      }
    true
  }

  private def menuItemDeleteListener(item: MenuItem): Boolean = {
    menuItemList.map(menu.findItem(_)).foreach(_.setEnabled(false))
    studentContactModel.
      filter(_.studentId.isDefined).
      filter(_.id.isDefined).
      map { model =>
        f"${StudentProvider.url}/${model.studentId.get}/studentContact/${model.id.get}"
      }.map(Uri.parse(_)).
      map(_.buildUpon()).
      map(_.appendQueryParameter(Version.version, studentContactModel.flatMap(_.version).map(_.toString).get)).
      map(_.build()).
      foreach(asyncQueryHandler.startDelete(Random.nextInt(), null, _, null, null))
    true
  }
}

object StudentContactFormFragment {
  val TAG = classOf[StudentContactFormFragment].getName
  val studentContactModel = "studentContactModel"
}
