/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import java.text.{ DateFormat, NumberFormat }
import java.util
import java.util.concurrent.TimeUnit
import java.util.{ Calendar, Date, Locale }

import android.app.DatePickerDialog.OnDateSetListener
import android.app.LoaderManager.LoaderCallbacks
import android.app.{ ActionBar, DatePickerDialog, ListFragment }
import android.content.{ AsyncQueryHandler, CursorLoader, Loader }
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.view.View.OnTouchListener
import android.view._
import android.widget._
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.{ StudentDuesModel, StudentDuesSummaryModel }

import scala.util.Random

/**
 * Created by i on 4/5/15.
 */
class StudentDuesListByMonthFragment extends ListFragment with TimeZoneTrait with ListFragmentTrait with ReadonlyOptionTrait {
  protected override var simpleCursorAdapter: SimpleCursorAdapter = null
  private var loaderCallbacks: LoaderCallbacks[Cursor] = null
  private var loaderCallbacks2: LoaderCallbacks[Cursor] = null
  private var dateFormat: DateFormat = null
  private var calendar: Calendar = null
  private var axisYearMonth: Option[Date] = None
  override protected var menu: Menu = null
  private var asyncQueryHandler: AsyncQueryHandler = null
  private var currencyFormat: NumberFormat = null

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    simpleCursorAdapter = new SampleSimpleCursorAdapter
    loaderCallbacks = new SampleLoaderCallbacks
    loaderCallbacks2 = new SampleLoaderCallbacks2
    asyncQueryHandler = new SampleAsyncQueryHandler
    dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.getDefault)
    dateFormat.setTimeZone(initTimeZone())
    calendar = Calendar.getInstance(initTimeZone(), Locale.getDefault)
    axisYearMonth = Option(savedInstanceState).
      orElse(Option(getArguments)).
      filter(_.containsKey(StudentDuesListByMonthFragment.axisYearMonth)).
      map(_.getSerializable(StudentDuesListByMonthFragment.axisYearMonth)).
      flatMap(_.asInstanceOf[Option[Date]]).orElse(Option {
        calendar.setTime(new util.Date())
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        calendar.getTime
      })
    currencyFormat = NumberFormat.getCurrencyInstance(Locale.getDefault)
    setHasOptionsMenu(true)
  }

  override def onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle): View = {
    super.onCreateView(inflater, container, savedInstanceState)
    inflater.inflate(R.layout.student_dues_list_layout, container, false)
  }

  override def onSaveInstanceState(outState: Bundle): Unit = {
    super.onSaveInstanceState(outState)
    outState.putSerializable(StudentDuesListByMonthFragment.axisYearMonth, axisYearMonth)
  }

  override def onCreateOptionsMenu(menu: Menu, inflater: MenuInflater): Unit = {
    super.onCreateOptionsMenu(menu, inflater)
    this.menu = menu
    inflater.inflate(R.menu.student_dues_list_by_month, menu)
    refreshAxisYearMonthOptionMenu()
    Option(menu.findItem(R.id.menuItemBatchGenerating)).foreach(_.setEnabled(false)) // 일괄생성 menu 는 항상 비활성상태로 시작하여, 상황에 따라 활성상태로 바뀐다.
  }

  private def refreshAxisYearMonthOptionMenu(): Unit = {
    axisYearMonth.map(dateFormat.format(_)).foreach(x => Option(menu.findItem(R.id.menuItemAxisYearMonth)).foreach(_.setTitle(x)))
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    item.getItemId match {
      case R.id.menuItemAxisYearMonth =>
        axisYearMonthOptionMenu()
        true
      case R.id.menuItemBatchGenerating =>
        Option(menu.findItem(R.id.menuItemBatchGenerating)).foreach(_.setEnabled(false))
        batchGeneratingOptionMenu()
        true
      case _ => super.onOptionsItemSelected(item)
    }
  }

  private def batchGeneratingOptionMenu(): Unit = {
    val uri = Uri.parse(f"content://${StudentDuesListByMonthProvider.authority}/${StudentDuesListByMonthProvider.studentDues}/${calendar.get(Calendar.YEAR)}/${calendar.get(Calendar.MONTH) + 1}")
    asyncQueryHandler.startInsert(Random.nextInt(), null, uri, null)
  }

  private class SampleAsyncQueryHandler extends AsyncQueryHandler(getActivity.getContentResolver) {
    override def onInsertComplete(token: Int, cookie: scala.Any, uri: Uri): Unit = {
      super.onInsertComplete(token, cookie, uri)
      getLoaderManager.restartLoader(0, null, loaderCallbacks)
      getLoaderManager.restartLoader(1, null, loaderCallbacks2)
    }
  }

  private def axisYearMonthOptionMenu(): Unit = {
    axisYearMonth.foreach(calendar.setTime(_))
    val dialog = Option(new DatePickerDialog(getActivity, new SampleOnDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)))
    dialog.chain(_.getDatePicker.setCalendarViewShown(false)).
      chain(_.getDatePicker.setSpinnersShown(true)).
      map(_.getDatePicker).
      flatMap { picker =>
        Option(picker.findViewById(getResources.getIdentifier("day", "id", "android")))
      }.foreach(_.setVisibility(View.GONE))
    dialog.foreach(_.show())
  }

  private class SampleOnDateSetListener extends OnDateSetListener {
    override def onDateSet(view: DatePicker, year: Int, monthOfYear: Int, dayOfMonth: Int): Unit = {
      calendar.set(Calendar.YEAR, year)
      calendar.set(Calendar.MONTH, monthOfYear)
      // http://stackoverflow.com/questions/21321789/android-datepicker-change-to-only-month-and-year
      // http://stackoverflow.com/questions/27563874/how-to-hide-calenderview-from-datepicker-in-api-21
      calendar.set(Calendar.DAY_OF_MONTH, 1) // lollipop 5.0 에서는 date picker 가 spinner 방식으로 동작하지 않는듯 하다? 해당 월의 아무날이나 고르면 1일을 선택한것으로 하자.
      calendar.set(Calendar.HOUR_OF_DAY, 0)
      calendar.set(Calendar.MINUTE, 0)
      calendar.set(Calendar.SECOND, 0)
      calendar.set(Calendar.MILLISECOND, 0)
      axisYearMonth = Option(calendar.getTime)
      refreshAxisYearMonthOptionMenu()
      getLoaderManager.restartLoader(0, null, loaderCallbacks)
      getLoaderManager.restartLoader(1, null, loaderCallbacks2)
    }
  }

  override def onActivityCreated(savedInstanceState: Bundle): Unit = {
    super.onActivityCreated(savedInstanceState)
    setListAdapter(simpleCursorAdapter)
    getActivity.getActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD)
    getActivity.getActionBar.setTitle(R.string.titleStudentDuesListByMonthFragment)
    val gesture = new GestureDetector(getActivity, new SampleGestureListener)
    getListView.setOnTouchListener(new OnTouchListener {
      override def onTouch(v: View, event: MotionEvent): Boolean = return gesture.onTouchEvent(event)
    })
  }

  override def onResume(): Unit = {
    super.onResume()
    getLoaderManager.restartLoader(0, null, loaderCallbacks)
    getLoaderManager.restartLoader(1, null, loaderCallbacks2)
  }

  override def onListItemClick(l: ListView, v: View, position: Int, id: Long): Unit = {
    super.onListItemClick(l, v, position, id)
    simpleCursorAdapter.getCursor.moveToPosition(position)
    Option(simpleCursorAdapter.getCursor.getColumnIndex(StudentDuesProvider.raw)).
      map(simpleCursorAdapter.getCursor.getString(_)).
      map(SampleObjectMapper.readValue[StudentDuesModel](_)).flatMap { model =>
        Option(new Bundle()).chain(_.putSerializable(StudentDuesFormFragment.studentDues, model))
      }.flatMap { bundle =>
        Option(new StudentDuesFormFragment).chain(_.setArguments(bundle))
      }.foreach { fragment =>
        getFragmentManager.beginTransaction().transaction { t =>
          t.replace(R.id.main_activity_layout, fragment, StudentDuesFormFragment.TAG)
          t.addToBackStack(null)
        }
      }
  }

  private class SampleSimpleCursorAdapter extends SimpleCursorAdapter(getActivity, R.layout.student_dues_list_item, null, Array(), Array(), 0) {
    override def getView(position: Int, convertView: View, parent: ViewGroup): View = {
      val view = super.getView(position, convertView, parent)
      val unknown = Option(getResources.getString(R.string.textUnknown))
      getCursor.moveToPosition(position)
      val studentDuesModel = Option(getCursor.getColumnIndex(StudentDuesListByMonthProvider.raw)).
        map(getCursor.getString(_)).
        map(SampleObjectMapper.readValue[StudentDuesModel](_))
      Option(view.findViewById(R.id.textStudentName)).
        map(_.asInstanceOf[TextView]).
        foreach(view => studentDuesModel.flatMap(_.student).flatMap(_.name).orElse(unknown).foreach(view.setText(_)))
      Option(view.findViewById(R.id.textAxisPayDate)).
        map(_.asInstanceOf[TextView]).
        foreach(view => studentDuesModel.flatMap(_.axisPayDate).map(dateFormat.format(_)).orElse(unknown).foreach(view.setText(_)))
      Option(view.findViewById(R.id.textPlannedPayDate)).
        map(_.asInstanceOf[TextView]).
        foreach(view => studentDuesModel.flatMap(_.plannedPayDate).map(dateFormat.format(_)).orElse(unknown).foreach(view.setText(_)))
      val drawableId = studentDuesModel.flatMap(_.actuallyPayDate) match {
        case Some(_) => R.drawable.check
        case _ =>
          // http://stackoverflow.com/questions/17940200/how-to-find-the-duration-of-difference-between-two-dates-in-java
          studentDuesModel.
            flatMap(_.plannedPayDate).
            map(x => TimeUnit.MILLISECONDS.toHours(x.getTime - (new Date).getTime)).
            map(Math.abs(_)).
            filter(_ <= 48) match {
              case Some(_) => R.drawable.alert
              case _ => R.drawable.question
            }
      }
      Option(view.findViewById(R.id.imageComplete)).
        map(_.asInstanceOf[ImageView]).foreach(_.setImageResource(drawableId))
      view
    }
  }

  private class SampleLoaderCallbacks2 extends LoaderCallbacks[Cursor] {
    override def onLoaderReset(loader: Loader[Cursor]): Unit = {}

    override def onLoadFinished(loader: Loader[Cursor], data: Cursor): Unit = {
      if (data != null) {
        data.moveToFirst()
        val model = Option(data.getColumnIndex(StudentDuesListByMonthProvider.raw)).map(data.getString(_)).map(SampleObjectMapper.readValue[StudentDuesSummaryModel](_))
        Option(getView.findViewById(R.id.textAllSum)).map(_.asInstanceOf[TextView]).foreach { view =>
          model.flatMap(_.allSum).orElse(Option(0)).map(currencyFormat.format(_)).foreach(view.setText(_))
        }
        Option(getView.findViewById(R.id.textCompleteSum)).map(_.asInstanceOf[TextView]).foreach { view =>
          model.flatMap(_.completeSum).orElse(Option(0)).map(currencyFormat.format(_)).foreach(view.setText(_))
        }
        Option(getView.findViewById(R.id.textIncompleteSum)).map(_.asInstanceOf[TextView]).foreach { view =>
          model.flatMap(_.incompleteSum).orElse(Option(0)).map(currencyFormat.format(_)).foreach(view.setText(_))
        }
      } // 이 fragment 에서는 두개의 요청을 보낸다. 한개가 실패하면 나머지 한개도 실패한거라고 가정하자. 따라서 오류가 있었음을 한번만 보이자.
    }

    override def onCreateLoader(id: Int, args: Bundle): Loader[Cursor] = {
      axisYearMonth.foreach(calendar.setTime(_))
      val uri = Uri.parse(f"content://${StudentDuesListByMonthProvider.authority}/${StudentDuesListByMonthProvider.studentDues}/${calendar.get(Calendar.YEAR)}/${calendar.get(Calendar.MONTH) + 1}/${StudentDuesListByMonthProvider.summary}")
      new CursorLoader(getActivity, uri, null, null, null, null)
    }
  }

  private class SampleLoaderCallbacks extends CommonLoaderCallbacks {
    override def onLoadFinished(loader: Loader[Cursor], data: Cursor): Unit = {
      super.onLoadFinished(loader, data)
      Option(getFragmentManager.findFragmentByTag(MainFragment.TAG)).
        map(_.asInstanceOf[MainFragment]).
        filterNot(_.readonly). // 읽기전용이 아닐 때에만, 화면새로고침 이후 일괄생성 option menu 를 활성화시킬수 있다.
        flatMap(_ => Option(data)).
        map(_.getCount == 0).orElse(Option(false)).
        foreach(result => Option(menu).flatMap(menu => Option(menu.findItem(R.id.menuItemBatchGenerating))).foreach(_.setEnabled(result)))
    }

    override def onCreateLoader(id: Int, args: Bundle): Loader[Cursor] = {
      setListShown(false)
      axisYearMonth.foreach(calendar.setTime(_))
      val uri = Uri.parse(f"content://${StudentDuesListByMonthProvider.authority}/${StudentDuesListByMonthProvider.studentDues}/${calendar.get(Calendar.YEAR)}/${calendar.get(Calendar.MONTH) + 1}")
      new CursorLoader(getActivity, uri, null, null, null, null)
    }
  }

  private class SampleGestureListener extends GestureDetector.SimpleOnGestureListener {

    override def onDown(e: MotionEvent): Boolean = true

    override def onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean = {
      super.onFling(e1, e2, velocityX, velocityY)
      axisYearMonth.foreach(calendar.setTime(_))
      if (e1.getX < e2.getX)
        calendar.add(Calendar.MONTH, 1)
      else
        calendar.add(Calendar.MONTH, -1)
      calendar.set(Calendar.DAY_OF_MONTH, 1)
      calendar.set(Calendar.HOUR_OF_DAY, 0)
      calendar.set(Calendar.MINUTE, 0)
      calendar.set(Calendar.SECOND, 0)
      calendar.set(Calendar.MILLISECOND, 0)
      axisYearMonth = Option(calendar.getTime)
      refreshAxisYearMonthOptionMenu()
      getLoaderManager.restartLoader(0, null, loaderCallbacks)
      getLoaderManager.restartLoader(1, null, loaderCallbacks2)
      true
    }
  }
}

object StudentDuesListByMonthFragment {
  val axisYearMonth: String = "axisYearMonth"
  val TAG = classOf[StudentDuesListByMonthFragment].getName
}
