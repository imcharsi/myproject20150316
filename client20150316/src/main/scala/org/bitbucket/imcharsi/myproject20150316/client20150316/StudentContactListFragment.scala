/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.app.{ ActionBar, ListFragment }
import android.content.{ CursorLoader, Intent, Loader }
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.view._
import android.widget._
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.{ StudentContactModel, StudentModel }

/**
 * Created by i on 3/24/15.
 */
class StudentContactListFragment extends ListFragment with ListFragmentTrait with ReadonlyOptionTrait {
  override protected var simpleCursorAdapter: SimpleCursorAdapter = null
  private var loaderCallbacks: StudentContactLoaderCallbacks = null
  private var student: Option[StudentModel] = None // 유지되어야 할 상태
  override protected var menu: Menu = null

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    loaderCallbacks = new StudentContactLoaderCallbacks
    student = Option(savedInstanceState).
      orElse(Option(getArguments)).
      filter(_.containsKey(StudentContactListFragment.student)).
      map(_.getSerializable(StudentContactListFragment.student)).
      map(_.asInstanceOf[StudentModel])
    setHasOptionsMenu(true)
    simpleCursorAdapter = new SampleSimpleCursorAdapter
    setListAdapter(simpleCursorAdapter)
    getActivity.getActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD)
  }

  override def onViewCreated(view: View, savedInstanceState: Bundle): Unit = {
    super.onViewCreated(view, savedInstanceState)
    // 여기서 보내는 상태는 Callback 안에서도 볼 수 있다. 그렇지만 Callback 안에서도 받아서 쓰기로 하자.
    student.flatMap { student =>
      Some(new Bundle()).chain(_.putSerializable(StudentContactListFragment.student, student))
    }.foreach(getLoaderManager.restartLoader(0, _, loaderCallbacks))
  }

  override def onCreateOptionsMenu(menu: Menu, inflater: MenuInflater): Unit = {
    super.onCreateOptionsMenu(menu, inflater)
    inflater.inflate(R.menu.basic_record_list, menu)
    this.menu = menu
    processReadonlyMode()
  }

  override def onListItemClick(l: ListView, v: View, position: Int, id: Long): Unit = {
    super.onListItemClick(l, v, position, id)
    Some(simpleCursorAdapter.getCursor).map { cursor =>
      cursor.moveToPosition(position)
      Some(cursor.getColumnIndex(StudentContactProvider.raw)).
        map(cursor.getString(_)).
        map(SampleObjectMapper.readValue[StudentContactModel](_)).flatMap { model =>
          Some(new Bundle()).chain(_.putSerializable(StudentContactFormFragment.studentContactModel, model))
        }.flatMap { bundle =>
          Some(new StudentContactFormFragment).
            chain(_.setArguments(bundle)).
            map((_, StudentContactFormFragment.TAG))
        }.foreach {
          case (fragment, tag) =>
            getFragmentManager.beginTransaction().transaction { t =>
              t.replace(R.id.main_activity_layout, fragment, tag)
              t.addToBackStack(null)
            }
        }
    }
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    item.getItemId match {
      case R.id.menuItemCreate =>
        student.map { student =>
          StudentContactModel(None, None, None, None, student.id, None, Option(false), Option(student))
        }.flatMap { studentContact =>
          Some(new Bundle()).chain(_.putSerializable(StudentContactFormFragment.studentContactModel, studentContact))
        }.flatMap { bundle =>
          Some(new StudentContactFormFragment).chain(_.setArguments(bundle))
        }.map { fragment =>
          getFragmentManager.beginTransaction().transaction { t =>
            t.replace(R.id.main_activity_layout, fragment, StudentContactFormFragment.TAG)
            t.addToBackStack(null)
          }
          true
        }.getOrElse(false)
      case _ => super.onOptionsItemSelected(item)
    }
  }

  override def onSaveInstanceState(outState: Bundle): Unit = {
    super.onSaveInstanceState(outState)
    student.foreach(outState.putSerializable(StudentContactListFragment.student, _))
  }

  override def onActivityCreated(savedInstanceState: Bundle): Unit = {
    super.onActivityCreated(savedInstanceState)
    getActivity.getActionBar.setTitle(R.string.titleStudentContactListFragment)
  }

  private class StudentContactLoaderCallbacks extends CommonLoaderCallbacks {
    override def onCreateLoader(id: Int, args: Bundle): Loader[Cursor] = {
      setListShown(false)
      Option(args.getSerializable(StudentContactListFragment.student)).
        map(_.asInstanceOf[StudentModel]).
        flatMap(_.id).
        // 아래와 같이 해야 한다. 문제는, Scala object 는 program 이 시작하는 시점에서 미리 초기화되지 않는다는 것이다.
        // 그래서 각 object 에 들어있는 uriMatcher 초기화도 동작하지 않게 되고, uri 를 대조하는 시점에서는 대조가 실패하게 된다.
        // 한편, 대조를 진행하는 시점에서 각 object 의 상수를 확인하게 되고 이때 초기화가 시작되는데, 그때는 이미 늦은 것이다.
        map(id => f"${StudentProvider.url}/${id}/${StudentContactProvider.studentContact}").
        map(Uri.parse(_)).
        map(new CursorLoader(getActivity, _, null, null, null, null)).get
    }
  }

  private class SampleSimpleCursorAdapter extends SimpleCursorAdapter(getActivity, R.layout.student_contact_list_layout, null,
    Array(), Array(), 0) {
    override def getView(position: Int, convertView: View, parent: ViewGroup): View = {
      getCursor.moveToPosition(position)
      val model = Option(getCursor.getColumnIndex(StudentContactProvider.raw)).
        map(getCursor.getString(_)).
        map(SampleObjectMapper.readValue[StudentContactModel](_))
      val unknown = Option(getResources.getString(R.string.textUnknown))
      Option(super.getView(position, convertView, parent)).chain { view =>
        Option(view.findViewById(R.id.textStudentName)).map(_.asInstanceOf[TextView]).
          chain(textView => model.flatMap(_.student).flatMap(_.name).orElse(unknown).foreach(textView.setText(_)))
        Option(view.findViewById(R.id.textCallNumber)).map(_.asInstanceOf[TextView]).
          chain(textView => model.flatMap(_.callNumber).orElse(unknown).foreach(textView.setText(_)))
        Option(view.findViewById(R.id.textAddress)).map(_.asInstanceOf[TextView]).
          chain(textView => model.flatMap(_.address).orElse(unknown).foreach(textView.setText(_)))
        Option(view.findViewById(R.id.buttonCallNumber)).
          map(_.asInstanceOf[Button]).
          chain(_.setOnClickListener(buttonCallNumberListener(position)(_: View))).chain { view =>
            // 이 연락처가 사용함으로 표시되어 있어야 하고, 연락처의 내용이 비어있지 않아야 한다.
            view.setEnabled(model.flatMap(_.validity).getOrElse(false) && model.flatMap(_.callNumber).filterNot(_.isEmpty).isDefined)
            val r: RadioButton = null
          }
        Option(view.findViewById(R.id.imageCurrentStudentContact)).map(_.asInstanceOf[ImageView]).chain { imageView =>
          model.flatMap(_.currentStudentContact).
            orElse(Option(false)).
            filter(_ == true) match {
              case Some(_) => imageView.setImageResource(R.drawable.check)
              case None => imageView.setImageResource(-1)
            }
        }
      }.get
    }

    private def buttonCallNumberListener(position: Int)(v: View): Unit = {
      student.flatMap(_.currentStudentContact).
        flatMap(_.callNumber).
        foreach { callNumber =>
          startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(f"tel:${callNumber}")))
        }
    }

    private def radioCurrentContactListener(position: Int)(v: CompoundButton, c: Boolean): Unit = {
      getCursor.moveToPosition(position)
      val studentContactId = Option(getCursor.getColumnIndex(StudentContactProvider.raw)).map(getCursor.getString(_)).map(SampleObjectMapper.readValue[StudentContactModel](_)).flatMap(_.id)
      student.map(_.copy(currentStudentContactId = studentContactId))
      notifyDataSetChanged()
    }
  }
}

object StudentContactListFragment {
  val TAG = classOf[StudentContactListFragment].getName
  val student = "student"
}
