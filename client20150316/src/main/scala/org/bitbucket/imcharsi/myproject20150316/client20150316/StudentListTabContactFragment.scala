/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.app.{ ListFragment, LoaderManager }
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.view.{ View, ViewGroup }
import android.widget._
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.StudentModel

/**
 * Created by i on 4/2/15.
 */
class StudentListTabContactFragment extends ListFragment with StudentListTabCommonTrait {
  override protected var simpleCursorAdapter: SimpleCursorAdapter = null
  private var loaderCallbacks: LoaderManager.LoaderCallbacks[Cursor] = null

  override def onActivityCreated(savedInstanceState: Bundle): Unit = {
    super.onActivityCreated(savedInstanceState)
    simpleCursorAdapter = new SampleSimpleCursorAdapter
    loaderCallbacks = new CommonLoaderCallbacks
    setListAdapter(simpleCursorAdapter)
  }

  override def onStart(): Unit = {
    super.onStart()
    getLoaderManager.restartLoader(0, null, loaderCallbacks)
  }

  private class SampleSimpleCursorAdapter extends SimpleCursorAdapter(getActivity, R.layout.student_list_tab_contact_item_layout, null,
    Array(), Array(), 0) {

    // list fragment 의 각 항목의 button 각각에 대해 listener 를 붙이고 싶으면 아래와 같이 한다.
    // http://stackoverflow.com/questions/19487253/how-to-handle-button-clicks-listfragment
    override def getView(position: Int, convertView: View, parent: ViewGroup): View = {
      val cursor = getCursor
      cursor.moveToPosition(position)
      // 실제로 화면출력에서 사용하는 값과 여기서 활성화여부를 판단하는데 사용하는 값의 의미와 근원출처는 같지만, 각 값의 저장장소가 다르다. 불일치의 문제가 있을수 있는데, 일단 넘어가자.
      val model = Option(cursor.getColumnIndex(StudentProvider.raw)).
        map(cursor.getString(_)).
        map(SampleObjectMapper.readValue[StudentModel](_))
      val unknown = Option(getResources.getString(R.string.textUnknown))
      Some(super.getView(position, convertView, parent)).chain { view =>
        Option(view.findViewById(R.id.textStudentName)).
          map(_.asInstanceOf[TextView]).
          chain(textView => model.flatMap(_.name).orElse(unknown).foreach(textView.setText(_)))
        Option(view.findViewById(R.id.textCallNumber)).
          map(_.asInstanceOf[TextView]).
          chain(textView => model.flatMap(_.currentStudentContact).flatMap(_.callNumber).orElse(unknown).foreach(textView.setText(_)))
        Option(view.findViewById(R.id.buttonCallNumber)).map(_.asInstanceOf[Button]).
          chain(_.setOnClickListener(buttonCallNumberListener(position)(_: View))).
          chain { button =>
            // 유효한 연락처이면서, 연락처번호가 있어야 한다. 연락처 기록은 있는데, 연락처번호가 비어있을수도 있다.
            button.setEnabled(model.flatMap(_.currentStudentContact).flatMap(_.validity).getOrElse(false) &&
              model.flatMap(_.currentStudentContact).flatMap(_.callNumber).filterNot(_.isEmpty).isDefined)
          }
        Option(view.findViewById(R.id.imageCurrentAttendingStudent)).map(_.asInstanceOf[ImageView]).foreach { view =>
          model.flatMap(_.currentAttendingStudent) match {
            case Some(true) => view.setImageResource(R.drawable.user)
            case _ => view.setImageResource(-1)
          }
        }
      }.get
    }

    private def buttonCallNumberListener(position: Int)(v: View): Unit = {
      val cursor = simpleCursorAdapter.getCursor
      cursor.moveToPosition(position)
      Option(cursor.getColumnIndex(StudentProvider.raw)).
        map(cursor.getString(_)).
        map(SampleObjectMapper.readValue[StudentModel](_)).
        flatMap(_.currentStudentContact).
        flatMap(_.callNumber).
        foreach { callNumber =>
          startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(f"tel:${callNumber}")))
        }
    }
  }

  override def onListItemClick(l: ListView, v: View, position: Int, id: Long): Unit = {
    super.onListItemClick(l, v, position, id)
    val cursor = simpleCursorAdapter.getCursor
    Option(cursor.getColumnIndex(StudentProvider.raw)).
      map(cursor.getString(_)).
      map(SampleObjectMapper.readValue[StudentModel](_)).flatMap { model =>
        Option(new Bundle()).chain(_.putSerializable(StudentContactListFragment.student, model))
      }.flatMap { bundle =>
        Option(new StudentContactListFragment).chain(_.setArguments(bundle))
      }.foreach { fragment =>
        getFragmentManager.beginTransaction().transaction { t =>
          t.replace(R.id.main_activity_layout, fragment, StudentContactListFragment.TAG)
          t.addToBackStack(null)
        }
      }
  }
}

object StudentListTabContactFragment {
  val TAG = classOf[StudentListTabContactFragment].getName
}
