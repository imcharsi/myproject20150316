/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import java.security.KeyStore

import android.content.{ ContentProvider, ContentValues }
import android.net.Uri
import android.preference.PreferenceManager
import android.util.Base64
import org.apache.http.HttpResponse
import org.apache.http.client.methods.HttpUriRequest
import org.apache.http.conn.scheme.{ PlainSocketFactory, Scheme, SchemeRegistry }
import org.apache.http.conn.ssl.SSLSocketFactory
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager
import org.apache.http.params.BasicHttpParams
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._

import scala.util.{ Failure, Success, Try }

/**
 * Created by i on 3/25/15.
 */
abstract class AbstractContentProvider extends ContentProvider {
  protected def template[Ret](uri: Uri,
    values: ContentValues,
    requestProc: RequestProc,
    postProc: PostProc[Ret], orElse: Ret): Ret = {
    val preferences = PreferenceManager.getDefaultSharedPreferences(getContext)
    val serverIp = preferences.getString(
      getContext.getResources.getString(R.string.prefServerIpKey),
      getContext.getResources.getString(R.string.prefServerIpDefault))
    val serverPort = preferences.getString(
      getContext.getResources.getString(R.string.prefServerPortKey),
      getContext.getResources.getString(R.string.prefServerPortDefault))
    val urlScheme = preferences.getString(
      getContext.getResources.getString(R.string.prefUrlScheme),
      getContext.getResources.getString(R.string.prefUrlSchemeDefault))
    val contextRoot = preferences.getString(
      getContext.getResources.getString(R.string.prefContextRoot),
      getContext.getResources.getString(R.string.prefContextRootDefault))
    val loginUsername = preferences.getString(getContext.getResources.getString(R.string.prefLoginUsername), null)
    val loginPassword = preferences.getString(getContext.getResources.getString(R.string.prefLoginPassword), null)
    val preferenceValue = PreferenceValue(Option(serverIp), Option(serverPort), Option(urlScheme), Option(contextRoot), Option(loginUsername), Option(loginPassword))

    // http://www.compiletimeerror.com/2013/01/login-application-for-android-android.html#.VSIS2HWUcW0 그대로 베꼈다.
    Option(getContext.getResources.openRawResource(R.raw.keystore)).flatMap { inputStream =>
      Option(KeyStore.getInstance(KeyStore.getDefaultType)). // android 에서는 기본으로, bouncy castle 이라는 방식을 쓴다고 한다.
        chain { keyStore =>
          keyStore.load(inputStream, getContext.getResources.getString(R.string.ssl_password).toCharArray)
          inputStream.close()
        }
    }.flatMap { keyStore =>
      Option(new SchemeRegistry).chain { schemeRegistry =>
        urlScheme match {
          case "http" =>
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory, serverPort.toInt))
          case "https" =>
            val factory = new SSLSocketFactory(keyStore)
            factory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER)
            schemeRegistry.register(new Scheme("https", factory, serverPort.toInt))
        }
      }
    }.map(schemeRegistry => new ThreadSafeClientConnManager(new BasicHttpParams, schemeRegistry)).
      map(connManager => new DefaultHttpClient(connManager, new BasicHttpParams)).
      chain { httpClient =>
      }.flatMap { httpClient =>
        Try {
          requestProc(preferenceValue, uri, values).
            chain(preferenceValue.authenticationHeader(_)).
            map(httpClient.execute(_)).
            flatMap(postProc(_))
        } match {
          case Success(x) => x
          case Failure(x) => Some(orElse) // 왜 Some 이어야 하는가. android 표준에 따라, cursor 나 uri 등 반환결과로서 null 을 전달해야 하는 경우도 있고. 그래서 orElse 가 null 일수도 있다.
        }
      }.get
  }

  type RequestProc = (PreferenceValue, Uri, ContentValues) => Option[HttpUriRequest]
  type PostProc[Ret] = HttpResponse => Option[Ret]

  case class PreferenceValue(serverIp: Option[String], serverPort: Option[String], schemeName: Option[String], contextRoot: Option[String], loginUsername: Option[String], loginPassword: Option[String]) {
    def urlHead: String = f"${schemeName.get}://${serverIp.get}:${serverPort.get}/${contextRoot.get}"

    def generateAuthenticationHeader: String = {
      if (loginUsername.isEmpty || loginPassword.isEmpty)
        throw new RuntimeException
      // http://www.programcreek.com/java-api-examples/index.php?api=android.util.Base64
      // 왜 Base64.DEFAULT 라고 하면 안되고 Base64.NO_WRAP 이라고 하면 되나?
      Base64.encodeToString(f"${loginUsername.get}:${loginPassword.get}".getBytes, Base64.NO_WRAP)
    }

    def authenticationHeader(message: HttpUriRequest): Unit = {
      message.addHeader("Authorization", "Basic " + generateAuthenticationHeader)
    }
  }

}

object AbstractContentProvider {
  val contentType = "application/json;charset=UTF-8"
  val accept = "application/json;charset=UTF-8"
}
