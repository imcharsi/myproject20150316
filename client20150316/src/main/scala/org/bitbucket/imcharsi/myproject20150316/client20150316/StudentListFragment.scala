/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.app.ListFragment
import android.app.LoaderManager.LoaderCallbacks
import android.content.{ CursorLoader, Loader }
import android.database.Cursor
import android.os.Bundle
import android.view.ContextMenu.ContextMenuInfo
import android.view._
import android.widget.AdapterView.AdapterContextMenuInfo
import android.widget.{ ListView, SimpleCursorAdapter, TextView }
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.{ StudentModel, StudentModelEx }

import scala.util.Random

/**
 * Created by i on 3/22/15.
 */
class StudentListFragment extends ListFragment with ListFragmentTrait with ReadonlyOptionTrait {
  override protected var simpleCursorAdapter: SimpleCursorAdapter = null
  private var loaderCallbacks: LoaderCallbacks[Cursor] = null
  override protected var menu: Menu = null

  // fixme 지금으로서는 context menu 가 뜨지 않고 floating menu 가 뜬다. 왜?

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    loaderCallbacks = new StudentLoaderCallbacks
    setHasOptionsMenu(true)
  }

  override def onCreateOptionsMenu(menu: Menu, inflater: MenuInflater): Unit = {
    super.onCreateOptionsMenu(menu, inflater)
    inflater.inflate(R.menu.basic_record_list, menu)
    menu.findItem(R.id.menuItemCreate).setOnMenuItemClickListener(createRecordListener(_: MenuItem))
    this.menu = menu
    processReadonlyMode()
  }

  override def onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenuInfo): Unit = {
    super.onCreateContextMenu(menu, v, menuInfo)
    getActivity.getMenuInflater.inflate(R.menu.student_model_context_menu, menu)
    // 아래와 같이 하니깐 잘 안된다. 원래 용법이 아닌건가.
    //    menu.getItem(R.id.menuItemStudentContactList).setOnMenuItemClickListener(studentContactListListener(_: MenuItem))
  }

  override def onContextItemSelected(item: MenuItem): Boolean = {
    item.getItemId match {
      case R.id.menuItemStudentContactList => studentContactListListener(item)
      case R.id.menuItemStudentSchoolHistoryList => studentSchoolHistoryListListener(item)
      case _ => super.onContextItemSelected(item)
    }
  }

  private def studentSchoolHistoryListListener(item: MenuItem): Boolean = {
    Some(simpleCursorAdapter.getCursor).flatMap { cursor =>
      cursor.moveToPosition(item.getMenuInfo.asInstanceOf[AdapterContextMenuInfo].position)
      Some(cursor.getColumnIndex(StudentProvider.raw)).
        map(cursor.getString(_)).
        map(SampleObjectMapper.readValue[StudentModel](_)).
        flatMap { model =>
          Some(new Bundle()).
            chain(_.putSerializable(StudentSchoolHistoryListFragment.student, model))
        }
    }.flatMap { bundle =>
      Some(new StudentSchoolHistoryListFragment).
        chain(_.setArguments(bundle))
    }.foreach { fragment =>
      getFragmentManager.beginTransaction().transaction { t =>
        t.replace(R.id.main_activity_layout, fragment, StudentSchoolHistoryListFragment.TAG)
        t.addToBackStack(null)
      }
    }
    true
  }

  private def studentContactListListener(m: MenuItem): Boolean = {
    Some(simpleCursorAdapter.getCursor).flatMap { cursor =>
      cursor.moveToPosition(m.getMenuInfo.asInstanceOf[AdapterContextMenuInfo].position)
      Some(cursor.getColumnIndex(StudentProvider.raw)).
        map(cursor.getString(_)).
        map(SampleObjectMapper.readValue[StudentModel](_)).
        flatMap { model =>
          // 그냥 model 을 통째로 보내주는 것이 낫겠다.
          Some(new Bundle()).
            chain(_.putSerializable(StudentContactListFragment.student, model))
        }
    }.flatMap { bundle =>
      Some(new StudentContactListFragment).
        chain(_.setArguments(bundle))
    }.foreach { fragment =>
      getFragmentManager.beginTransaction().transaction { t =>
        t.replace(R.id.main_activity_layout, fragment, StudentContactListFragment.TAG)
        t.addToBackStack(null)
      }
    }
    true
  }

  override def onActivityCreated(savedInstanceState: Bundle): Unit = {
    super.onActivityCreated(savedInstanceState)
    getActivity.getActionBar.setTitle(R.string.titleStudentListFragment)
    simpleCursorAdapter = new SampleSimpleCursorAdapter
    setListAdapter(simpleCursorAdapter)
  }

  override def onStart(): Unit = {
    super.onStart()
    registerForContextMenu(getListView)
  }

  override def onListItemClick(l: ListView, v: View, position: Int, id: Long): Unit = {
    super.onListItemClick(l, v, position, id)
    val cursor: Cursor = simpleCursorAdapter.getCursor
    cursor.moveToPosition(position)
    Some(cursor.getColumnIndex(StudentProvider.raw)).
      map(cursor.getString(_)).
      map(SampleObjectMapper.readValue[StudentModel](_)).
      flatMap { model =>
        Some(new Bundle()).chain { bundle =>
          bundle.putSerializable(StudentFormFragment.studentModel, model)
        }
      }.flatMap { bundle =>
        Some(new StudentFormFragment).chain(_.setArguments(bundle))
      }.foreach { fragment =>
        getFragmentManager.beginTransaction().transaction { t =>
          t.replace(R.id.main_activity_layout, fragment, StudentFormFragment.TAG)
          t.addToBackStack(null)
        }
      }
  }

  override def onResume(): Unit = {
    super.onResume()
    // 여기서 자료를 읽어야 확실하게 보인다.
    getLoaderManager.restartLoader(0, null, loaderCallbacks)
  }

  private def createRecordListener(m: MenuItem): Boolean = {
    Some(StudentModelEx(None, None, Some(Random.nextInt().toString), None, None, None, None, None)).flatMap { model =>
      Some(new Bundle()).chain(_.putSerializable(StudentFormFragment.studentModel, model))
    }.flatMap { bundle =>
      Some((new StudentFormFragment, StudentFormFragment.TAG)).chain(_._1.setArguments(bundle))
    }.foreach {
      case (fragment, tag) =>
        getFragmentManager.beginTransaction().transaction { t =>
          t.replace(R.id.main_activity_layout, fragment, tag)
          t.addToBackStack(null)
        }
    }
    true
  }

  private class SampleSimpleCursorAdapter extends SimpleCursorAdapter(getActivity, android.R.layout.simple_list_item_1, null,
    Array(), Array(), 0) {
    override def getView(position: Int, convertView: View, parent: ViewGroup): View = {
      val view: View = super.getView(position, convertView, parent)
      getCursor.moveToPosition(position)
      val studentModel = Option(getCursor.getColumnIndex(StudentProvider.raw)).
        map(getCursor.getString(_)).
        map(SampleObjectMapper.readValue[StudentModel](_))
      Option(view.findViewById(android.R.id.text1)).
        map(_.asInstanceOf[TextView]).
        chain { textView => studentModel.flatMap(_.name).orElse(Some("")).foreach(textView.setText(_)) }
      view
    }
  }

  private class StudentLoaderCallbacks extends CommonLoaderCallbacks {
    override def onCreateLoader(id: Int, args: Bundle): Loader[Cursor] = new CursorLoader(getActivity, StudentProvider.uri, null, null, null, null)
  }
}

object StudentListFragment {
  val TAG = classOf[StudentListFragment].getName
}
