/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.app.{ ActionBar, Fragment }
import android.content._
import android.os.Bundle
import android.util.Log
import android.view._
import android.widget.{ EditText, ToggleButton }
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.{ StudentModel, Version }

import scala.util.Random

/**
 * Created by i on 3/20/15.
 */
class StudentFormFragment extends Fragment with FormFragmentTrait with ReadonlyOptionTrait {
  private var asyncQueryHandler: AsyncQueryHandler = null
  private var editTextStudentId: Option[EditText] = None
  private var editTextStudentName: Option[EditText] = None
  private var editTextCurrentDuesAmount: Option[EditText] = None
  private var editTextDistanceOfPDD: Option[EditText] = None
  private var toggleCurrentAttending: Option[ToggleButton] = None
  override protected var menu: Menu = null
  private var studentModel: Option[StudentModel] = None

  override def onActivityCreated(savedInstanceState: Bundle): Unit = {
    super.onActivityCreated(savedInstanceState)
    setHasOptionsMenu(true)
    getActivity.getActionBar.setTitle(R.string.titleStudentFormFragment)
    getActivity.getActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD)
  }

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    asyncQueryHandler = new SampleAsyncQueryHandler(() => studentModel.flatMap(_.id).isDefined)
    studentModel = Option(savedInstanceState).map(_.getSerializable(StudentFormFragment.studentModel)).
      orElse(Option(getArguments).map(_.getSerializable(StudentFormFragment.studentModel))).
      map(_.asInstanceOf[StudentModel])
  }

  override def onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle): View = {
    super.onCreateView(inflater, container, savedInstanceState)
    Some(inflater.inflate(R.layout.student_form_fragment_layout, container, false)).chain { view =>
      editTextStudentId = Option(view.findViewById(R.id.editTextStudentId).asInstanceOf[EditText]).
        chain { editText => studentModel.flatMap(_.id).foreach(id => editText.setText(id.toString)) }
      editTextStudentName = Option(view.findViewById(R.id.editTextStudentName).asInstanceOf[EditText]).
        chain { editText => studentModel.flatMap(_.name).foreach(name => editText.setText(name)) }
      editTextCurrentDuesAmount = Option(view.findViewById(R.id.editTextCurrentDuesAmount)).map(_.asInstanceOf[EditText]).
        chain { editText => studentModel.flatMap(_.currentDuesAmount).map(_.toString).foreach(editText.setText(_)) }
      editTextDistanceOfPDD = Option(view.findViewById(R.id.editTextDistanceOfPlannedDuesDate)).map(_.asInstanceOf[EditText]).
        chain { picker => studentModel.flatMap(_.distanceOfPlannedDuesDate).orElse(Some(0)).map(_.toString).foreach(picker.setText(_)) }
      toggleCurrentAttending = Option(view.findViewById(R.id.toggleCurrentAttending)).map(_.asInstanceOf[ToggleButton]).
        chain { toggle => studentModel.flatMap(_.currentAttendingStudent).orElse(Some(true)).foreach(toggle.setChecked(_)) }
    }.get
  }

  override def onCreateOptionsMenu(menu: Menu, inflater: MenuInflater): Unit = {
    super.onCreateOptionsMenu(menu, inflater)
    inflater.inflate(R.menu.submit_record, menu)
    this.menu = menu
    menu.findItem(R.id.menuItemDelete).setEnabled(studentModel.flatMap(_.id).isDefined)
    processReadonlyMode()
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    item.getItemId match {
      case R.id.menuItemSubmit => menuItemSubmitListener(item)
      case R.id.menuItemCancel => menuItemCancelListener(item)
      case R.id.menuItemRandom => menuItemRandomListener(item)
      case R.id.menuItemDelete => menuItemDeleteListener(item)
      case _ => super.onOptionsItemSelected(item)
    }
  }

  override def onSaveInstanceState(outState: Bundle): Unit = {
    super.onSaveInstanceState(outState)
    prepareStudentModel().foreach(outState.putSerializable(StudentFormFragment.studentModel, _))
  }

  private def prepareStudentModel(): Option[StudentModel] =
    studentModel.map(_.copy(
      id = editTextStudentId.map(_.getText.toString).filter(_.isEmpty == false).map(_.toInt),
      name = editTextStudentName.map(_.getText.toString),
      currentDuesAmount = editTextCurrentDuesAmount.map(_.getText).map(_.toString).filterNot(_.isEmpty).map(_.toInt),
      distanceOfPlannedDuesDate = editTextDistanceOfPDD.map(_.getText).map(_.toString).filterNot(_.isEmpty).map(_.toInt),
      currentAttendingStudent = toggleCurrentAttending.map(_.isChecked)))

  private def menuItemSubmitListener(m: MenuItem): Boolean = {
    menuItemList.foreach(menu.findItem(_).setEnabled(false))
    Some(new ContentValues()).foreach { values =>
      prepareStudentModel().
        map(SampleObjectMapper.writeValueAsString(_)).
        foreach(values.put(StudentProvider.raw, _))
      if (studentModel.get.id.isEmpty) {
        asyncQueryHandler.startInsert(Random.nextInt(), null, StudentProvider.uri, values)
      } else {
        studentModel.
          flatMap(_.id).
          map(ContentUris.withAppendedId(StudentProvider.uri, _)).
          foreach { uri =>
            asyncQueryHandler.startUpdate(Random.nextInt(), null, uri, values, null, null)
          }
      }
    }
    true
  }

  private def menuItemCancelListener(m: MenuItem): Boolean = {
    getFragmentManager.popBackStack()
    true
  }

  private def menuItemRandomListener(m: MenuItem): Boolean = {
    editTextStudentName.chain(_.setText(Random.nextInt().toString))
    editTextCurrentDuesAmount.chain(_.setText(Random.nextInt(100).toString))
    editTextDistanceOfPDD.chain(_.setText(Random.nextInt(10).toString))
    toggleCurrentAttending.chain(_.setChecked(Random.nextBoolean()))
    true
  }

  private def menuItemDeleteListener(m: MenuItem): Boolean = {
    Log.i(getClass.getName, studentModel.get.id.toString)
    studentModel.
      flatMap(_.id).
      map(ContentUris.withAppendedId(StudentProvider.uri, _)).
      map(_.buildUpon()).
      map(_.appendQueryParameter(Version.version, studentModel.flatMap(_.version).map(_.toString).get)).
      map(_.build()).
      foreach { uri =>
        asyncQueryHandler.startDelete(Random.nextInt(), null, uri, null, null)
      }
    true
  }

}

object StudentFormFragment {
  val TAG = classOf[StudentFormFragment].getName
  val studentModel = "studentModel"
}
