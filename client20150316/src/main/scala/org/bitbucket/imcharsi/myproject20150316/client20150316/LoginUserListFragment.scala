/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.app.LoaderManager.LoaderCallbacks
import android.app.{ ActionBar, ListFragment }
import android.content.{ CursorLoader, Loader }
import android.database.Cursor
import android.os.Bundle
import android.preference.PreferenceManager
import android.view._
import android.widget.{ ImageView, ListView, SimpleCursorAdapter, TextView }
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.LoginUserModel

/**
 * Created by i on 4/8/15.
 */
class LoginUserListFragment extends ListFragment with ListFragmentTrait with ReadonlyOptionTrait {
  protected override var simpleCursorAdapter: SimpleCursorAdapter = null
  private var loaderCallbacks: LoaderCallbacks[Cursor] = null
  private var loginUsername: Option[String] = None
  override protected var menu: Menu = null

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    simpleCursorAdapter = new SampleSimpleCursorAdapter
    loaderCallbacks = new SampleLoaderCallbacks
    setListAdapter(simpleCursorAdapter)
    setHasOptionsMenu(true)
    loginUsername = Option(PreferenceManager.getDefaultSharedPreferences(getActivity).getString(getResources.getString(R.string.prefLoginUsername), null))
  }

  override def onCreateOptionsMenu(menu: Menu, inflater: MenuInflater): Unit = {
    super.onCreateOptionsMenu(menu, inflater)
    inflater.inflate(R.menu.basic_record_list, menu)
    this.menu = menu
    processReadonlyMode()
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    item.getItemId match {
      case R.id.menuItemCreate =>
        openForm(Option(LoginUserModel(None, None, None, None, None, Option(true))))
        true
      case _ => super.onOptionsItemSelected(item)
    }
  }

  override def onActivityCreated(savedInstanceState: Bundle): Unit = {
    super.onActivityCreated(savedInstanceState)
    getActivity.getActionBar.setTitle(R.string.titleLoginUserListFragment)
    getActivity.getActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD)
  }

  override def onResume(): Unit = {
    super.onResume()
    getLoaderManager.restartLoader(0, null, loaderCallbacks)
  }

  override def onListItemClick(l: ListView, v: View, position: Int, id: Long): Unit = {
    super.onListItemClick(l, v, position, id)
    val cursor = simpleCursorAdapter.getCursor
    cursor.moveToPosition(position)
    openForm(Option(cursor.getColumnIndex(LoginUserProvider.raw)).
      map(cursor.getString(_)).
      map(SampleObjectMapper.readValue[LoginUserModel](_)))
  }

  private def openForm(model: Option[LoginUserModel]): Unit = {
    model.flatMap { model =>
      Option(new Bundle()).chain(_.putSerializable(LoginUserProvider.loginUser, model))
    }.flatMap { bundle =>
      Option(new LoginUserFormFragment).chain(_.setArguments(bundle))
    }.foreach { fragment =>
      getFragmentManager.beginTransaction().transaction { t =>
        t.replace(R.id.main_activity_layout, fragment, LoginUserFormFragment.TAG)
        t.addToBackStack(null)
      }
    }
  }

  private class SampleLoaderCallbacks extends CommonLoaderCallbacks {
    override def onCreateLoader(id: Int, args: Bundle): Loader[Cursor] = {
      setListShown(false)
      new CursorLoader(getActivity, LoginUserProvider.uri, null, null, null, null)
    }
  }

  private class SampleSimpleCursorAdapter extends SimpleCursorAdapter(getActivity, R.layout.login_user_list_item, null, Array(), Array(), 0) {
    override def getView(position: Int, convertView: View, parent: ViewGroup): View = {
      val view = super.getView(position, convertView, parent)
      getCursor.moveToPosition(position)
      val model = Option(getCursor.getColumnIndex(LoginUserProvider.raw)).
        map(getCursor.getString(_)).
        map(SampleObjectMapper.readValue[LoginUserModel](_))
      Option(view.findViewById(R.id.textLoginUserName)).map(_.asInstanceOf[TextView]).foreach { view =>
        model.flatMap(_.userName).orElse(Option("")).foreach(view.setText(_))
      }
      Option(view.findViewById(R.id.imageAvailable)).map(_.asInstanceOf[ImageView]).foreach { view =>
        val drawableId = model.flatMap(_.available).orElse(Option(false)) match {
          case Some(true) => R.drawable.check
          case _ => R.drawable.question
        }
        view.setImageResource(drawableId)
      }
      Option(view.findViewById(R.id.imageIsThisMe)).map(_.asInstanceOf[ImageView]).foreach { view =>
        val drawableId = model.flatMap(_.userName) match {
          case a @ Some(_) if a == loginUsername => R.drawable.user
          case _ => -1
        }
        view.setImageResource(drawableId)
      }
      view
    }
  }

}

object LoginUserListFragment {
  val TAG = classOf[LoginUserListFragment].getName
}
