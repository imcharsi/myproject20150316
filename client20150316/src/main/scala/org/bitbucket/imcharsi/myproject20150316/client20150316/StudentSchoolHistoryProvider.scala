/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.content.{ ContentUris, ContentValues }
import android.database.{ Cursor, MatrixCursor }
import android.net.Uri
import org.apache.http.client.methods._
import org.apache.http.entity.StringEntity
import org.apache.http.protocol.HTTP
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.{ SchoolType, StudentSchoolHistoryModel, Version }

import scala.collection.JavaConversions

/**
 * Created by i on 4/4/15.
 */
trait StudentSchoolHistoryProvider extends AbstractContentProvider {
  protected def updateStudentSchoolHistoryModel(uri: Uri, values: ContentValues): Int = {
    val pathSegments = JavaConversions.collectionAsScalaIterable(uri.getPathSegments).toList
    val (a, studentId, b, studentSchoolHistoryId) = pathSegments match {
      case (a @ StudentProvider.student) :: studentId :: (b @ StudentSchoolHistoryProvider.studentSchoolHistory) :: studentSchoolHistoryId :: Nil => (a, studentId, b, studentSchoolHistoryId)
    }

    def requestProc: RequestProc = (preferenceValue, uri, values) => {
      Some(new HttpPut(f"${preferenceValue.urlHead}/${a}/${studentId.toInt}/${b}/${studentSchoolHistoryId}")).chain { httpPut =>
        Some(values.getAsString(StudentSchoolHistoryProvider.raw)).
          map(new StringEntity(_, HTTP.UTF_8)).
          foreach(httpPut.setEntity(_))
        httpPut.setHeader("Content-Type", AbstractContentProvider.contentType)
      }
    }
    def postProc: PostProc[Int] = (httpResponse) => {
      httpResponse.getStatusLine.getStatusCode match {
        case 200 => Some(1)
        //        case 405 => Some(0)
      }
    }
    template[Int](uri, values, requestProc, postProc, 0)
  }

  protected def insertStudentSchoolHistoryModel(uri: Uri, values: ContentValues): Uri = {
    val pathSegments = JavaConversions.collectionAsScalaIterable(uri.getPathSegments).toList
    val (a, studentId, b) = pathSegments match {
      case (a @ StudentProvider.student) :: studentId :: (b @ StudentSchoolHistoryProvider.studentSchoolHistory) :: Nil => (a, studentId, b)
    }

    def requestProc: RequestProc = (preferenceValue, uri, values) => {
      Some(new HttpPost(f"${preferenceValue.urlHead}/${a}/${studentId.toInt}/${b}")).chain { httpPost =>
        Some(values.getAsString(StudentSchoolHistoryProvider.raw)).
          map(new StringEntity(_, HTTP.UTF_8)).
          foreach(httpPost.setEntity(_))
        httpPost.setHeader("Accept", AbstractContentProvider.accept)
        httpPost.setHeader("Content-Type", AbstractContentProvider.contentType)
      }
    }
    def postProc: PostProc[Uri] = (httpResponse) => {
      SampleObjectMapper.readValue[StudentSchoolHistoryModel](httpResponse.getEntity.getContent).id.map { id =>
        ContentUris.withAppendedId(Uri.parse(f"${StudentProvider.url}/${studentId}/${StudentSchoolHistoryProvider.studentSchoolHistory}"), id)
      }
    }
    template[Uri](uri, values, requestProc, postProc, null)
  }

  protected def deleteStudentSchoolHistoryModel(uri: Uri, values: ContentValues): Int = {
    val pathSegments = JavaConversions.collectionAsScalaIterable(uri.getPathSegments).toList
    val (a, studentId, b, studentSchoolHistoryId) = pathSegments match {
      case (a @ StudentProvider.student) :: studentId :: (b @ StudentSchoolHistoryProvider.studentSchoolHistory) :: studentSchoolHistoryId :: Nil => (a, studentId, b, studentSchoolHistoryId)
    }
    def requestProc: RequestProc = (preferenceValue, uri, _) => {
      Option(f"${preferenceValue.urlHead}/${a}/${studentId}/${b}/${studentSchoolHistoryId}").
        map(Uri.parse(_)).
        map(_.buildUpon()).
        map(_.appendQueryParameter(Version.version, uri.getQueryParameter(Version.version))).
        map(_.build()).
        map(uri => new HttpDelete(uri.toString))
    }
    def postProc: PostProc[Int] = (httpResponse) => {
      Some(SampleObjectMapper.readValue[Int](httpResponse.getEntity.getContent))
    }
    template[Int](uri, values, requestProc, postProc, 0)
  }

  protected def queryStudentSchoolHistoryAll(uri: Uri): Cursor = {
    val pathSegments = JavaConversions.collectionAsScalaIterable(uri.getPathSegments).toList
    val (a, studentId, b) = pathSegments match {
      case (a @ StudentProvider.student) :: studentId :: (b @ StudentSchoolHistoryProvider.studentSchoolHistory) :: Nil => (a, studentId, b)
    }

    def requestProc: RequestProc = (preferenceValue, uri, _) => {
      Some(new HttpGet(f"${preferenceValue.urlHead}/${a}/${studentId.toInt}/${b}")).
        chain(_.addHeader("Accept", AbstractContentProvider.accept))
    }
    def postProc: PostProc[Cursor] = httpResponse => {
      getContext.getResources.getString(R.string.textElementarySchool)
      Some(new MatrixCursor(List(
        StudentSchoolHistoryProvider.studentSchoolHistoryId,
        StudentSchoolHistoryProvider.raw).toArray)).
        chain { matrixCursor =>
          SampleObjectMapper.readValue[List[StudentSchoolHistoryModel]](httpResponse.getEntity.getContent).
            foreach { model =>
              matrixCursor.
                newRow().
                add(model.id.orNull).
                add(SampleObjectMapper.writeValueAsString(model))
            }
        }
    }
    template[Cursor](uri, null, requestProc, postProc, null)
  }
}

object StudentSchoolHistoryProvider {
  val studentSchoolHistoryAll: Int = StudentContactProvider.studentContactEach + 1
  val studentSchoolHistoryEach = studentSchoolHistoryAll + 1

  val studentSchoolHistoryId = "_id"
  val raw = "raw"

  val studentSchoolHistory = "studentSchoolHistory"

  val schoolType2StringIdMap = Map(
    (SchoolType.unknown, R.string.textUnknown),
    (SchoolType.elementarySchool, R.string.textElementarySchool),
    (SchoolType.middleSchool, R.string.textMiddleSchool),
    (SchoolType.highSchool, R.string.textHighSchool))

  StudentProvider.uriMatcher.addURI(StudentProvider.authority, f"${StudentProvider.student}/#/${studentSchoolHistory}", studentSchoolHistoryAll)
  StudentProvider.uriMatcher.addURI(StudentProvider.authority, f"${StudentProvider.student}/#/${studentSchoolHistory}/#", studentSchoolHistoryEach)
}

