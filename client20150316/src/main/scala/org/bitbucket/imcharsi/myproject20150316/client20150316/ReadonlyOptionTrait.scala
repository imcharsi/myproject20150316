package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.app.Fragment
import android.view.MenuItem

/**
 * Created by i on 4/15/15.
 */
trait ReadonlyOptionTrait extends Fragment {
  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    // 어떠한 fragment 를 보고 있더라도, app icon 을 누르면 읽기전용방식으로 바뀔수 있도록 한다.
    item.getItemId match {
      case android.R.id.home =>
        Option(getFragmentManager.findFragmentByTag(MainFragment.TAG)).
          map(_.asInstanceOf[MainFragment]).
          foreach(_.flipReadonlyStatus(this))
        true
      case _ => super.onOptionsItemSelected(item)
    }
  }
}
