/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.app.{ ListFragment, LoaderManager }
import android.content.{ CursorLoader, Loader }
import android.database.Cursor
import android.os.Bundle
import android.view.ContextMenu.ContextMenuInfo
import android.view.{ ContextMenu, MenuItem, View }
import android.widget.AdapterView.AdapterContextMenuInfo
import android.widget.SimpleCursorAdapter
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.StudentModel

/**
 * Created by i on 4/3/15.
 */
trait StudentListTabCommonTrait extends ListFragment {
  protected var simpleCursorAdapter: SimpleCursorAdapter

  // StudentList 의 tab 3개에서 보여지는 학생목록에 대해 오래누르기를 하면, 학생기본정보에 대한 편집을 할수 있게 하고싶은데,
  // 각 list fragment 에 똑같은 걸 반복하지 않기 위해 이 trait 를 쓴다.

  override def onStart(): Unit = {
    super.onStart()
    registerForContextMenu(getListView)
  }

  override def onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenuInfo): Unit = {
    super.onCreateContextMenu(menu, v, menuInfo)
    getActivity.getMenuInflater.inflate(R.menu.edit_context_menu, menu)
  }

  override def onContextItemSelected(item: MenuItem): Boolean = {
    item.getItemId match {
      case R.id.menuItemEdit =>
        val cursor = simpleCursorAdapter.getCursor
        cursor.moveToPosition(item.getMenuInfo.asInstanceOf[AdapterContextMenuInfo].position)
        Option(cursor.getColumnIndex(StudentProvider.raw)).
          map(cursor.getString(_)).
          map(SampleObjectMapper.readValue[StudentModel](_)).
          flatMap { model =>
            Option(new Bundle()).chain(_.putSerializable(StudentFormFragment.studentModel, model))
          }.flatMap { bundle =>
            Option(new StudentFormFragment).chain(_.setArguments(bundle))
          }.foreach { fragment =>
            getFragmentManager.beginTransaction().transaction { t =>
              t.replace(R.id.main_activity_layout, fragment, StudentFormFragment.TAG)
              t.addToBackStack(null)
            }
          }
        true
      case _ => super.onContextItemSelected(item)
    }
  }

  // tab 3개에 대한 검색조건이 같다.
  class CommonLoaderCallbacks extends LoaderManager.LoaderCallbacks[Cursor] {
    override def onLoaderReset(loader: Loader[Cursor]): Unit = {}

    override def onLoadFinished(loader: Loader[Cursor], data: Cursor): Unit = {
      simpleCursorAdapter.changeCursor(data)
      setListShown(true)
    }

    override def onCreateLoader(id: Int, args: Bundle): Loader[Cursor] = {
      setListShown(false)
      val searchCondition = Option(getFragmentManager.findFragmentByTag(StudentListOuterFragment.TAG)).
        map(_.asInstanceOf[StudentListOuterFragment]).flatMap(_.getSearchCondition)
      val uri = Option(StudentProvider.uri.buildUpon()).flatMap { builder =>
        searchCondition.flatMap(_.currentAttendingStudent).map(_.toString).
          map(builder.appendQueryParameter(StudentProvider.currentAttendingStudent, _)).orElse(Option(builder))
      }.flatMap { builder =>
        searchCondition.flatMap(_.search).
          map(builder.appendQueryParameter(StudentProvider.search, _)).orElse(Option(builder))
      }.flatMap { builder =>
        searchCondition.flatMap(_.schoolId).
          map(_.toString).
          map(builder.appendQueryParameter(StudentProvider.schoolId, _)).orElse(Option(builder))
      }.map(_.build())
      new CursorLoader(getActivity, uri.get, null, null, null, null)
    }
  }
}
