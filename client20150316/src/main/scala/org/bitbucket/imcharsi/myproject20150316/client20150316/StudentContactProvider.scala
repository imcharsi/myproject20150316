/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.content.{ ContentUris, ContentValues }
import android.database.{ Cursor, MatrixCursor }
import android.net.Uri
import org.apache.http.client.methods._
import org.apache.http.entity.StringEntity
import org.apache.http.protocol.HTTP
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.{ StudentContactModel, Version }

import scala.collection.JavaConversions

/**
 * Created by i on 4/4/15.
 */
trait StudentContactProvider extends AbstractContentProvider {
  protected def insertStudentContactModel(uri: Uri, values: ContentValues): Uri = {
    val pathSegments = JavaConversions.collectionAsScalaIterable(uri.getPathSegments).toList
    val (a, studentId, b) = pathSegments match {
      case (a @ StudentProvider.student) :: studentId :: (b @ StudentContactProvider.studentContact) :: Nil => (a, studentId, b)
    }

    def requestProc: RequestProc = (preferenceValue, uri, values) => {
      Some(new HttpPost(f"${preferenceValue.urlHead}/${a}/${studentId.toInt}/${b}")).chain { httpPost =>
        Some(values.getAsString(StudentContactProvider.raw)).
          map(new StringEntity(_, HTTP.UTF_8)).
          foreach(httpPost.setEntity(_))
        httpPost.setHeader("Accept", AbstractContentProvider.accept)
        httpPost.setHeader("Content-Type", AbstractContentProvider.contentType)
      }
    }
    def postProc: PostProc[Uri] = (httpResponse) => {
      SampleObjectMapper.readValue[StudentContactModel](httpResponse.getEntity.getContent).id.map { id =>
        ContentUris.withAppendedId(Uri.parse(f"${StudentProvider.url}/${studentId}/${StudentContactProvider.studentContact}"), id)
      }
    }
    template[Uri](uri, values, requestProc, postProc, null)
  }

  protected def updateStudentContactModel(uri: Uri, values: ContentValues): Int = {
    val pathSegments = JavaConversions.collectionAsScalaIterable(uri.getPathSegments).toList
    val (a, studentId, b, studentContactId) = pathSegments match {
      case (a @ StudentProvider.student) :: studentId :: (b @ StudentContactProvider.studentContact) :: studentContactId :: Nil => (a, studentId, b, studentContactId)
    }

    def requestProc: RequestProc = (preferenceValue, uri, values) => {
      Some(new HttpPut(f"${preferenceValue.urlHead}/${a}/${studentId.toInt}/${b}/${studentContactId}")).chain { httpPut =>
        Some(values.getAsString(StudentContactProvider.raw)).
          map(new StringEntity(_, HTTP.UTF_8)).
          foreach(httpPut.setEntity(_))
        httpPut.setHeader("Content-Type", AbstractContentProvider.contentType)
      }
    }
    def postProc: PostProc[Int] = (httpResponse) => {
      httpResponse.getStatusLine.getStatusCode match {
        case 200 => Some(1)
        //        case 405 => Some(0)
      }
    }
    template[Int](uri, values, requestProc, postProc, 0)
  }

  protected def deleteStudentContactModel(uri: Uri, values: ContentValues): Int = {
    val pathSegments = JavaConversions.collectionAsScalaIterable(uri.getPathSegments).toList
    val (a, studentId, b, studentContactId) = pathSegments match {
      case (a @ StudentProvider.student) :: studentId :: (b @ StudentContactProvider.studentContact) :: studentContactId :: Nil => (a, studentId, b, studentContactId)
    }
    def requestProc: RequestProc = (preferenceValue, uri, _) => {
      Option(f"${preferenceValue.urlHead}/${a}/${studentId}/${b}/${studentContactId}").
        map(Uri.parse(_)).
        map(_.buildUpon()).
        map(_.appendQueryParameter(Version.version, uri.getQueryParameter(Version.version))).
        map(_.build()).
        map(uri => new HttpDelete(uri.toString))
    }
    def postProc: PostProc[Int] = (httpResponse) => {
      Some(SampleObjectMapper.readValue[Int](httpResponse.getEntity.getContent))
    }
    template[Int](uri, values, requestProc, postProc, 0)
  }

  protected def queryStudentContactAll(uri: Uri): Cursor = {
    val pathSegments = JavaConversions.collectionAsScalaIterable(uri.getPathSegments).toList
    // todo 일단은, 이렇게 해도 되는데, combinator parser 로 바꾸기로 하자. 이와 유사한 유형이 다른 곳에도 있다. 전부 찾아서 바꾸기.
    val (a, studentId, b) = pathSegments match {
      case (a @ StudentProvider.student) :: studentId :: (b @ StudentContactProvider.studentContact) :: Nil => (a, studentId, b)
    }

    def requestProc: RequestProc = (preferenceValue, uri, _) => {
      Some(new HttpGet(f"${preferenceValue.urlHead}/${a}/${studentId.toInt}/${b}")).
        chain(_.addHeader("Accept", "application/json"))
    }
    def postProc: PostProc[Cursor] = httpResponse => {
      Some(new MatrixCursor(List(
        StudentContactProvider.studentContactId,
        StudentContactProvider.raw).toArray)).
        chain { matrixCursor =>
          SampleObjectMapper.readValue[List[StudentContactModel]](httpResponse.getEntity.getContent).
            foreach { model =>
              matrixCursor.
                newRow().
                add(model.id.orNull).
                add(SampleObjectMapper.writeValueAsString(model)) // ContentValues 와 Cursor 를 사용한 주고받기가 너무 번거롭다. 차라리 이렇게 하는것이 낫겠다.
            }
        }
    }
    template[Cursor](uri, null, requestProc, postProc, null)
  }
}

object StudentContactProvider {
  val studentContactAll: Int = StudentProvider.studentEach + 1
  val studentContactEach: Int = StudentContactProvider.studentContactAll + 1

  val studentContactId = "_id"
  val raw = "raw"

  val studentContact = "studentContact"

  StudentProvider.uriMatcher.addURI(StudentProvider.authority, f"${StudentProvider.student}/#/${studentContact}", studentContactAll)
  StudentProvider.uriMatcher.addURI(StudentProvider.authority, f"${StudentProvider.student}/#/${studentContact}/#", studentContactEach)
}

