/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.app.ListFragment
import android.app.LoaderManager.LoaderCallbacks
import android.content.Loader
import android.database.Cursor
import android.view.Menu
import android.widget.{ SimpleCursorAdapter, Toast }

/**
 * Created by i on 4/9/15.
 */
trait ListFragmentTrait extends ListFragment {
  protected var simpleCursorAdapter: SimpleCursorAdapter
  protected var menu: Menu

  protected def processReadonlyMode(): Unit = {
    // list fragment 의 create option menu 를 상황에 따라 비활성시킨다.
    Option(getFragmentManager.findFragmentByTag(MainFragment.TAG)).map(_.asInstanceOf[MainFragment]).map(_.readonly).foreach { flag =>
      Option(menu.findItem(R.id.menuItemCreate)).foreach(_.setEnabled(!flag))
    }
  }

  abstract class CommonLoaderCallbacks extends LoaderCallbacks[Cursor] {
    override def onLoaderReset(loader: Loader[Cursor]): Unit = {}

    override def onLoadFinished(loader: Loader[Cursor], data: Cursor): Unit = {
      // http://stackoverflow.com/questions/5318175/fragment-loading-spinner-dialog-in-honeycomb
      // list fragment 의 loading progress 를 보이고 싶을 때.
      simpleCursorAdapter.changeCursor(data)
      setListShown(true)
      if (data == null) {
        Toast.makeText(getActivity, getResources.getString(R.string.failure), Toast.LENGTH_SHORT).show()
      }
    }
  }

}
