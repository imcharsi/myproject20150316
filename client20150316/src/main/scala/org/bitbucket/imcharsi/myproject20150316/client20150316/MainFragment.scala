/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.app.LoaderManager.LoaderCallbacks
import android.app.{ ActionBar, Fragment }
import android.content._
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.view._
import android.widget.CompoundButton.OnCheckedChangeListener
import android.widget.{ CompoundButton, ToggleButton }
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil.{ FragmentTransactionUtil, OptionChain, onClickListenerUtil }

/**
 * Created by i on 3/20/15.
 */
class MainFragment extends Fragment with ReadonlyOptionTrait {
  private var asyncQueryHandler: AsyncQueryHandler = null
  private var loaderCallbacks: LoaderCallbacks[Cursor] = null
  private var readonlyStatus: Boolean = false
  def readonly: Boolean = readonlyStatus

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    asyncQueryHandler = new SampleAsyncQueryHandler
    loaderCallbacks = new SampleLoaderCallbacks
    Option(savedInstanceState).filter(_.containsKey(MainFragment.readonly)).map(_.getBoolean(MainFragment.readonly)).foreach(readonlyStatus = _)
    setHasOptionsMenu(true)
  }

  override def onActivityCreated(savedInstanceState: Bundle): Unit = {
    super.onActivityCreated(savedInstanceState)
    getActivity.getActionBar.setTitle(R.string.titleMainFragment)
    getActivity.getActionBar.removeAllTabs()
    getActivity.getActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD)
    getActivity.getActionBar.setHomeButtonEnabled(true)
  }

  override def onSaveInstanceState(outState: Bundle): Unit = {
    super.onSaveInstanceState(outState)
    outState.putBoolean(MainFragment.readonly, readonlyStatus)
  }

  private def buttonSchoolListListener(view: View) = {
    getFragmentManager.beginTransaction().transaction { t =>
      //      t.replace(R.id.main_activity_layout, new SchoolListFragment, SchoolListFragment.TAG)
      // 학교 유형별로 tab 을 나눠서 볼 수 있도록 하기.
      t.replace(R.id.main_activity_layout, new SchoolListOuterFragment, SchoolListOuterFragment.TAG)
      t.addToBackStack(null)
    }
  }

  def justFlipReadonlyStatus(): Unit = {
    readonlyStatus = !readonlyStatus
    readonlyStatus match {
      case false => getActivity.getActionBar.setIcon(android.R.drawable.sym_def_app_icon)
      case true => getActivity.getActionBar.setIcon(R.drawable.gnome_emblem_readonly)
    }
  }

  def flipReadonlyStatus(f: Fragment): Unit = {
    justFlipReadonlyStatus()
    getFragmentManager.beginTransaction().transaction(_.detach(f))
    getFragmentManager.beginTransaction().transaction(_.attach(f))
  }

  override def onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle) = {
    // 왜 여기서는 super 를 이어서 전달하지 않는가. super 는 null 을 돌려준다.
    super.onCreateView(inflater, container, savedInstanceState)
    Some(inflater.inflate(R.layout.main_fragment_layout, container, false)).chain { view =>
      Some(view.findViewById(R.id.buttonStudentList)).chain(_.setOnClickListener(buttonStudentListListener(_: View)))
      Some(view.findViewById(R.id.buttonStudentDuesListByMonth)).chain(_.setOnClickListener(buttonStudentDuesListByMonthListener(_: View)))
      Some(view.findViewById(R.id.buttonSchoolList)).chain(_.setOnClickListener(buttonSchoolListListener(_: View)))
      Some(view.findViewById(R.id.buttonLoginUserList)).chain(_.setOnClickListener(buttonLoginUserListListener(_: View)))
      Some(view.findViewById(R.id.toggleReadonly)).map(_.asInstanceOf[ToggleButton]).chain(_.setChecked(readonly)).chain(_.setOnCheckedChangeListener(new ReadonlyListener))
      Some(view.findViewById(R.id.buttonPreferences)).chain(_.setOnClickListener(buttonPreferencesListener(_: View)))
    }.get
  }

  override def onCreateOptionsMenu(menu: Menu, inflater: MenuInflater): Unit = {
    super.onCreateOptionsMenu(menu, inflater)
    // 현재 화면에 여러개의 fragment 가 사용된다면 사용되는 모든 fragment 의 menu item 이 더해진다.
    // 한편, 각 fragment 가 back stack 에 따라 구분되면, menu item 도 구분된다.
    // 따라서 아래에 있는 fragment 의 menu item 은 위에 있는 현재 보이는 fragment 의 menu 에 보이지 않는 것이다.
  }

  private def buttonLoginUserListListener(v: View): Unit = {
    getFragmentManager.beginTransaction().transaction { t =>
      t.replace(R.id.main_activity_layout, new LoginUserListFragment, LoginUserListFragment.TAG)
      t.addToBackStack(null)
    }
  }

  private def buttonStudentListListener(v: View): Unit = {
    getFragmentManager.beginTransaction().transaction { t =>
      t.replace(R.id.main_activity_layout, new StudentListOuterFragment, StudentListOuterFragment.TAG)
      t.addToBackStack(null)
    }
  }

  private def buttonStudentDuesListByMonthListener(v: View): Unit = {
    getFragmentManager.beginTransaction().transaction { t =>
      t.replace(R.id.main_activity_layout, new StudentDuesListByMonthFragment, StudentDuesListByMonthFragment.TAG)
      t.addToBackStack(null)
    }
  }

  private def buttonPreferencesListener(v: View): Unit = {
    getFragmentManager.beginTransaction().transaction { t =>
      // 아래를 이 Fragment 의 layout 을 대상으로 하면 어떻게 되나.
      // .replace 명령의 동작방식에 관한 설명을 읽어보면, .remove 를 쓴 후 .add 를 사용한다고 되어 있다.
      // 결론은 layout 의 원래 내용은 지워지지 않고 새 Fragment 들이 더해지는 것이다.
      t.replace(R.id.main_activity_layout, new SamplePreferenceFragment, SamplePreferenceFragment.TAG)
      t.addToBackStack(null)
    }
  }

  private class SampleAsyncQueryHandler extends AsyncQueryHandler(getActivity.getContentResolver) {
    override def onInsertComplete(token: Int, cookie: scala.Any, uri: Uri): Unit = {
      super.onInsertComplete(token, cookie, uri)
      //      val bundle = new Bundle()
      //      bundle.putInt(StudentContactProvider.studentId, 1)
      getLoaderManager.restartLoader(0, null, loaderCallbacks)
    }
  }

  private class SampleLoaderCallbacks extends LoaderCallbacks[Cursor] {
    override def onCreateLoader(id: Int, args: Bundle): Loader[Cursor] = {
      new CursorLoader(getActivity, SchoolProvider.uri, null, null, null, null)
    }

    override def onLoaderReset(loader: Loader[Cursor]): Unit = {}

    override def onLoadFinished(loader: Loader[Cursor], data: Cursor): Unit = {
      data
    }
  }

  private class ReadonlyListener extends OnCheckedChangeListener {
    override def onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean): Unit = {
      flipReadonlyStatus(MainFragment.this)
    }
  }
}

object MainFragment {
  val TAG = classOf[MainFragment].getName
  val readonly = "readonly"
}
