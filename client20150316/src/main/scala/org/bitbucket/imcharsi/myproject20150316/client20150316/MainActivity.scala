/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.app.Activity
import android.os.Bundle
import android.view.Menu
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._

/**
 * Created by i on 2015-03-16.
 */
class MainActivity extends Activity {
  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.main_activity_layout)
    // 이와 같이 하지 않으면 기계가 회전한다든지 하는 여러가지 이유로 activity 를 다시 시작하게 될 때 back stack 이 계속 쌓이게 된다.
    if (getFragmentManager.findFragmentByTag(MainFragment.TAG) == null) {
      getFragmentManager.beginTransaction().transaction { t =>
        t.replace(R.id.main_activity_layout, new MainFragment, MainFragment.TAG)
      }
    }
  }

  override def onCreateOptionsMenu(menu: Menu): Boolean = {
    super.onCreateOptionsMenu(menu)
    menu.clear()
    true
  }
}
