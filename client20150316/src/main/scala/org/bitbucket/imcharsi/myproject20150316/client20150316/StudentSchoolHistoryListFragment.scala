/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import java.text.DateFormat
import java.util.Locale

import android.app.LoaderManager.LoaderCallbacks
import android.app.{ ActionBar, ListFragment }
import android.content.{ CursorLoader, Loader }
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.view._
import android.widget.{ ImageView, ListView, SimpleCursorAdapter, TextView }
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.{ StudentModel, StudentSchoolHistoryModel }

/**
 * Created by i on 3/27/15.
 */
class StudentSchoolHistoryListFragment extends ListFragment with TimeZoneTrait with ListFragmentTrait with ReadonlyOptionTrait {
  private var loaderCallbacks: LoaderCallbacks[Cursor] = null
  override protected var simpleCursorAdapter: SimpleCursorAdapter = null
  private var student: Option[StudentModel] = None
  private var dateFormat: DateFormat = null
  override protected var menu: Menu = null

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    loaderCallbacks = new StudentSchoolHistoryLoaderCallbacks
    student = Option(savedInstanceState).
      orElse(Option(getArguments)).
      filter(_.containsKey(StudentSchoolHistoryListFragment.student)).
      map(_.getSerializable(StudentSchoolHistoryListFragment.student)).
      map(_.asInstanceOf[StudentModel])
    setHasOptionsMenu(true)
    dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.getDefault)
    dateFormat.setTimeZone(initTimeZone())
  }

  override def onCreateOptionsMenu(menu: Menu, inflater: MenuInflater): Unit = {
    super.onCreateOptionsMenu(menu, inflater)
    inflater.inflate(R.menu.basic_record_list, menu)
    this.menu = menu
    processReadonlyMode()
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    item.getItemId match {
      case R.id.menuItemCreate =>
        Some(new StudentSchoolHistoryFormFragment).chain { fragment =>
          Some(new Bundle()).
            chain(_.putSerializable(StudentSchoolHistoryFormFragment.studentSchoolHistoryModel, StudentSchoolHistoryModel(None, None, student.flatMap(_.id), None, None, None, None, None, student, None))).
            chain(fragment.setArguments(_))
        }.foreach { fragment =>
          getFragmentManager.beginTransaction().transaction { t =>
            t.replace(R.id.main_activity_layout, fragment, StudentSchoolHistoryFormFragment.TAG)
            t.addToBackStack(null)
          }
        }
        true
      case _ => super.onOptionsItemSelected(item)
    }
  }

  override def onActivityCreated(savedInstanceState: Bundle): Unit = {
    super.onActivityCreated(savedInstanceState)
    simpleCursorAdapter = new SampleSimpleCursorAdapter
    setListAdapter(simpleCursorAdapter)
    getActivity.getActionBar.setTitle(getResources.getString(R.string.titleStudentSchoolHistoryListFragment))
    getActivity.getActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD)
  }

  private class SampleSimpleCursorAdapter extends SimpleCursorAdapter(getActivity, R.layout.student_school_history_item, null, Array(), Array(), 0) {
    override def getView(position: Int, convertView: View, parent: ViewGroup): View = {
      val view: View = super.getView(position, convertView, parent)
      getCursor.moveToPosition(position)
      val studehtSchoolHistoryModel = Option(getCursor.getColumnIndex(StudentSchoolHistoryProvider.raw)).
        map(getCursor.getString(_)).
        map(SampleObjectMapper.readValue[StudentSchoolHistoryModel](_))
      val unknown = Option(getResources.getString(R.string.textUnknown))
      Option(view.findViewById(R.id.textStudentName)).
        map(_.asInstanceOf[TextView]).
        chain(textView => studehtSchoolHistoryModel.flatMap(_.student).flatMap(_.name).orElse(unknown).foreach(textView.setText(_)))
      Option(view.findViewById(R.id.textSchoolName)).
        map(_.asInstanceOf[TextView]).
        chain(textView => studehtSchoolHistoryModel.flatMap(_.school).flatMap(_.name).orElse(unknown).foreach(textView.setText(_)))
      Option(view.findViewById(R.id.textCurrentGrade)).
        map(_.asInstanceOf[TextView]).
        chain(textView => studehtSchoolHistoryModel.flatMap(_.currentGrade).map(_.toString).orElse(unknown).foreach(textView.setText(_)))
      Option(view.findViewById(R.id.textCurrentDate)).
        map(_.asInstanceOf[TextView]).
        chain(textView => studehtSchoolHistoryModel.flatMap(_.currentDate).map(dateFormat.format(_)).orElse(unknown).foreach(textView.setText(_)))
      studehtSchoolHistoryModel.flatMap(_.currentStudentSchoolHistory).orElse(Some(false)).
        map {
          case true => R.drawable.check
          case _ => -1
        }.foreach { id =>
          Option(view.findViewById(R.id.imageCurrentStudentSchoolHistory)).map(_.asInstanceOf[ImageView]).foreach(_.setImageResource(id))
        }
      view
    }
  }

  override def onSaveInstanceState(outState: Bundle): Unit = {
    super.onSaveInstanceState(outState)
    student.foreach(outState.putSerializable(StudentSchoolHistoryListFragment.student, _))
  }

  override def onResume(): Unit = {
    super.onResume()
    student.flatMap { student =>
      Option(new Bundle()).chain(_.putSerializable(StudentSchoolHistoryListFragment.student, student))
    }.foreach(getLoaderManager.restartLoader(0, _, loaderCallbacks))
  }

  override def onListItemClick(l: ListView, v: View, position: Int, id: Long): Unit = {
    super.onListItemClick(l, v, position, id)
    Some(new StudentSchoolHistoryFormFragment).chain { fragment =>
      val cursor = simpleCursorAdapter.getCursor
      Option(cursor.getColumnIndex(StudentSchoolHistoryProvider.raw)).
        map(cursor.getString(_)).
        map(json => SampleObjectMapper.readValue[StudentSchoolHistoryModel](json)).
        flatMap { model =>
          Some(new Bundle()).chain(_.putSerializable(StudentSchoolHistoryFormFragment.studentSchoolHistoryModel, model))
        }.chain(fragment.setArguments(_))
    }.foreach { fragment =>
      getFragmentManager.beginTransaction().transaction { t =>
        t.replace(R.id.main_activity_layout, fragment, StudentSchoolHistoryFormFragment.TAG)
        t.addToBackStack(null)
      }
    }
  }

  private class StudentSchoolHistoryLoaderCallbacks extends CommonLoaderCallbacks {
    override def onCreateLoader(id: Int, args: Bundle): Loader[Cursor] = {
      setListShown(false)
      Option(args).
        map(_.getSerializable(StudentSchoolHistoryListFragment.student)).
        map(_.asInstanceOf[StudentModel]).
        flatMap(_.id).
        map(studentId => f"content://${StudentProvider.authority}/${StudentProvider.student}/${studentId}/${StudentSchoolHistoryProvider.studentSchoolHistory}").
        map(Uri.parse(_)).
        map(new CursorLoader(getActivity, _, null, null, null, null)).get
    }
  }
}

object StudentSchoolHistoryListFragment {
  val TAG = classOf[StudentSchoolHistoryListFragment].getName
  val student = "student"
}
