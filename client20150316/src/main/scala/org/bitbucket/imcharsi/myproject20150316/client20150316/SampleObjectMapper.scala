/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import java.text.SimpleDateFormat
import java.util.TimeZone

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper

/**
 * Created by i on 3/25/15.
 */
object SampleObjectMapper extends ObjectMapper with ScalaObjectMapper {
  // http://stackoverflow.com/questions/3907929/should-i-make-jacksons-objectmapper-as-static-final
  registerModule(DefaultScalaModule)
  private val dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  dateFormat.setTimeZone(TimeZone.getTimeZone("UTC")) // server 에서 주고 받을 때는 utc 로 주고 받는다. 그래서 android 에서 json 을 주고 받을 때도 utc 로 주고 받는다.
  setDateFormat(dateFormat)
}
