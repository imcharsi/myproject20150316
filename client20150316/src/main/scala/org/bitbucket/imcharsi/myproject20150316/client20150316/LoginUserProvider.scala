/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.content.{ ContentUris, ContentValues, UriMatcher }
import android.database.{ Cursor, MatrixCursor }
import android.net.Uri
import org.apache.http.client.methods.{ HttpDelete, HttpGet, HttpPost, HttpPut }
import org.apache.http.entity.StringEntity
import org.apache.http.protocol.HTTP
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.{ LoginUserModel, Version }

import scala.collection.JavaConversions

/**
 * Created by i on 4/8/15.
 */
class LoginUserProvider extends AbstractContentProvider {
  override def onCreate(): Boolean = true

  override def getType(uri: Uri): String = LoginUserProvider.uriMatcher.`match`(uri) match {
    case LoginUserProvider.loginUserAll => f"vnd.android.cursor.dir/${classOf[LoginUserModel].getName}"
  }

  override def update(uri: Uri, values: ContentValues, selection: String, selectionArgs: Array[String]): Int = {
    LoginUserProvider.uriMatcher.`match`(uri) match {
      case LoginUserProvider.loginUserEach => updateLoginUser(uri, values)
    }
  }

  override def insert(uri: Uri, values: ContentValues): Uri = {
    LoginUserProvider.uriMatcher.`match`(uri) match {
      case LoginUserProvider.loginUserAll => insertLoginUser(uri, values)
    }
  }

  override def delete(uri: Uri, selection: String, selectionArgs: Array[String]): Int = {
    LoginUserProvider.uriMatcher.`match`(uri) match {
      case LoginUserProvider.loginUserEach => deleteLoginUser(uri)
    }
  }

  override def query(uri: Uri, projection: Array[String], selection: String, selectionArgs: Array[String], sortOrder: String): Cursor = {
    LoginUserProvider.uriMatcher.`match`(uri) match {
      case LoginUserProvider.loginUserAll => queryLoginUserAll(uri)
    }
  }

  private def queryLoginUserAll(uri: Uri): Cursor = {
    def requestProc: RequestProc = (preferenceValue, uri, _) => {
      Option(Uri.parse(f"${preferenceValue.urlHead}/${LoginUserProvider.loginUser}").buildUpon()).
        map(uri => new HttpGet(uri.toString)).
        chain(_.addHeader("Accept", AbstractContentProvider.accept))
    }
    def postProc: PostProc[Cursor] = httpResponse => {
      Some(new MatrixCursor(List(LoginUserProvider.loginUserId, LoginUserProvider.raw).toArray)).
        chain { matrixCursor =>
          SampleObjectMapper.readValue[List[LoginUserModel]](httpResponse.getEntity.getContent).
            foreach { model =>
              matrixCursor.
                newRow().
                add(model.id.orNull).
                add(SampleObjectMapper.writeValueAsString(model))
            }
        }
    }
    template(uri, null, requestProc, postProc, null)
  }

  private def insertLoginUser(uri: Uri, values: ContentValues): Uri = {
    def requestProc: RequestProc = (preferenceValue, uri, values) => {
      Some(new HttpPost(f"${preferenceValue.urlHead}/${LoginUserProvider.loginUser}")).chain { httpPost =>
        Some(values.getAsString(LoginUserProvider.raw)).
          map(new StringEntity(_, HTTP.UTF_8)).
          foreach(httpPost.setEntity(_))
      }.chain(_.addHeader("Accept", AbstractContentProvider.accept)).
        chain(_.addHeader("Content-Type", AbstractContentProvider.contentType))
    }
    def postProc: PostProc[Uri] = (httpResponse) => {
      SampleObjectMapper.readValue[LoginUserModel](httpResponse.getEntity.getContent).id.map { id =>
        ContentUris.withAppendedId(LoginUserProvider.uri, id)
      }
    }
    template(uri, values, requestProc, postProc, null)
  }

  private def updateLoginUser(uri: Uri, values: ContentValues): Int = {
    def requestProc: RequestProc = (preferenceValue, uri, values) => {
      val (loginUser, id) = JavaConversions.collectionAsScalaIterable(uri.getPathSegments).toList match {
        case (loginUser @ LoginUserProvider.loginUser) :: id :: Nil => (loginUser, id)
      }
      Some(new HttpPut(f"${preferenceValue.urlHead}/${loginUser}/${id}")).chain { httpPut =>
        Some(values.getAsString(LoginUserProvider.raw)).
          map(new StringEntity(_, HTTP.UTF_8)).
          foreach(httpPut.setEntity(_))
      }.chain(_.addHeader("Content-Type", AbstractContentProvider.contentType))
    }
    def postProc: PostProc[Int] = (httpResponse) => {
      httpResponse.getStatusLine.getStatusCode match {
        case 200 => Some(1)
        //        case 404 => Some(0)
      }
    }
    template(uri, values, requestProc, postProc, 0)
  }

  private def deleteLoginUser(uri: Uri): Int = {
    def requestProc: RequestProc = (preferenceValue, uri, values) => {
      val (loginUser, id) = JavaConversions.collectionAsScalaIterable(uri.getPathSegments).toList match {
        case (loginUser @ LoginUserProvider.loginUser) :: id :: Nil => (loginUser, id)
      }
      Option(f"${preferenceValue.urlHead}/${loginUser}/${id}").
        map(Uri.parse(_)).
        map(_.buildUpon()).
        map(_.appendQueryParameter(Version.version, uri.getQueryParameter(Version.version))).
        map(_.build()).
        map(uri => new HttpDelete(uri.toString))
    }
    def postProc: PostProc[Int] = (httpResponse) => {
      httpResponse.getStatusLine.getStatusCode match {
        case 200 => Some(1)
        //        case 404 => Some(0)
      }
    }
    template(uri, null, requestProc, postProc, 0)
  }
}

object LoginUserProvider {
  val authority = classOf[LoginUserProvider].getName

  val loginUserAll = 1
  val loginUserEach = loginUserAll + 1

  val loginUser = "loginUser"

  val url = f"content://${authority}/${loginUser}"
  val uri = Uri.parse(url)
  val uriMatcher = new UriMatcher(UriMatcher.NO_MATCH)

  val loginUserId = "_id"
  val raw = "raw"

  uriMatcher.addURI(authority, f"${loginUser}", loginUserAll)
  uriMatcher.addURI(authority, f"${loginUser}/#", loginUserEach)
}
