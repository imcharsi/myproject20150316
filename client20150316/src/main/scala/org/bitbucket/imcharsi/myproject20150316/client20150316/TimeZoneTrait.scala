/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import java.util.TimeZone

import android.app.Fragment
import android.preference.PreferenceManager

/**
 * Created by i on 4/5/15.
 */
trait TimeZoneTrait extends Fragment {
  // json 을 주고 받는 단계에서는 utc 를 쓰고 그 외에는 database 가 사용하는 timezone 을 써야 한다. 긴 설명은 StudentDuesListFragment 에 있다.
  protected def initTimeZone(): TimeZone = {
    TimeZone.getTimeZone(
      PreferenceManager.getDefaultSharedPreferences(getActivity).getString(
        getResources.getString(R.string.prefTimeZone),
        getResources.getString(R.string.prefTimeZoneDefault)))
  }
}
