/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import java.text.NumberFormat
import java.util.{ Currency, Locale }

import android.app.ListFragment
import android.app.LoaderManager.LoaderCallbacks
import android.database.Cursor
import android.os.Bundle
import android.view.{ View, ViewGroup }
import android.widget.{ ImageView, ListView, SimpleCursorAdapter, TextView }
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.StudentModel

/**
 * Created by i on 4/3/15.
 */
class StudentListTabDuesFragment extends ListFragment with StudentListTabCommonTrait {
  override protected var simpleCursorAdapter: SimpleCursorAdapter = null
  private var loaderCallbacks: LoaderCallbacks[Cursor] = null
  private var currentCurrency: Currency = null
  private var currencyFormat: NumberFormat = null

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    simpleCursorAdapter = new SampleSimpleCursorAdapter
    loaderCallbacks = new CommonLoaderCallbacks
    currentCurrency = Currency.getInstance(Locale.getDefault)
    currencyFormat = NumberFormat.getCurrencyInstance(Locale.getDefault)
    setListAdapter(simpleCursorAdapter)
  }

  override def onActivityCreated(savedInstanceState: Bundle): Unit = {
    super.onActivityCreated(savedInstanceState)
  }

  override def onResume(): Unit = {
    super.onResume()
    getLoaderManager.restartLoader(0, null, loaderCallbacks)
  }

  override def onListItemClick(l: ListView, v: View, position: Int, id: Long): Unit = {
    super.onListItemClick(l, v, position, id)
    val cursor = simpleCursorAdapter.getCursor
    cursor.moveToPosition(position)
    Option(cursor.getColumnIndex(StudentProvider.raw)).map(cursor.getString(_)).map(SampleObjectMapper.readValue[StudentModel](_)).flatMap { model =>
      Option(new Bundle()).chain(_.putSerializable(StudentDuesListFragment.student, model))
    }.flatMap { bundle =>
      Option(new StudentDuesListFragment).chain(_.setArguments(bundle))
    }.foreach { fragment =>
      getFragmentManager.beginTransaction().transaction { t =>
        t.replace(R.id.main_activity_layout, fragment, StudentDuesListFragment.TAG)
        t.addToBackStack(null)
      }
    }
  }

  private class SampleSimpleCursorAdapter extends SimpleCursorAdapter(getActivity, R.layout.student_list_tab_dues_item, null, Array(), Array(), 0) {
    override def getView(position: Int, convertView: View, parent: ViewGroup): View = {
      val view = super.getView(position, convertView, parent)
      getCursor.moveToPosition(position)
      val studentModel = Option(getCursor.getColumnIndex(StudentProvider.raw)).
        map(getCursor.getString(_)).
        map(SampleObjectMapper.readValue[StudentModel](_))
      val unknown = Option(getResources.getString(R.string.textUnknown))
      Option(view.findViewById(R.id.textStudentName)).map(_.asInstanceOf[TextView]).foreach { view =>
        studentModel.flatMap(_.name).orElse(unknown).foreach(view.setText(_))
      }
      Option(view.findViewById(R.id.textRemainingDuesAmount)).map(_.asInstanceOf[TextView]).foreach { view =>
        studentModel.flatMap(_.remainingDuesAmount).orElse(Option(0)).map(currencyFormat.format(_)).foreach(view.setText(_))
      }
      Option(view.findViewById(R.id.imageRemainingDuesAmount)).map(_.asInstanceOf[ImageView]).foreach { view =>
        val drawableId = studentModel.flatMap(_.remainingDuesAmount) match {
          case Some(x) if x > 0 => R.drawable.question
          case _ => R.drawable.check
        }
        view.setImageResource(drawableId)
      }
      Option(view.findViewById(R.id.imageCurrentAttendingStudent)).map(_.asInstanceOf[ImageView]).foreach { view =>
        studentModel.flatMap(_.currentAttendingStudent) match {
          case Some(true) => view.setImageResource(R.drawable.user)
          case _ => view.setImageResource(-1)
        }
      }
      view
    }
  }

}

object StudentListTabDuesFragment {
  val TAG = classOf[StudentListTabDuesFragment].getName
}
