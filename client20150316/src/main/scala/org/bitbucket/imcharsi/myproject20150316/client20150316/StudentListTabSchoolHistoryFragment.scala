/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.app.ListFragment
import android.app.LoaderManager.LoaderCallbacks
import android.database.Cursor
import android.os.Bundle
import android.view.{ View, ViewGroup }
import android.widget.{ ImageView, ListView, SimpleCursorAdapter, TextView }
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.StudentModel

/**
 * Created by i on 4/3/15.
 */
class StudentListTabSchoolHistoryFragment extends ListFragment with StudentListTabCommonTrait {
  override protected var simpleCursorAdapter: SimpleCursorAdapter = null
  private var loaderCallbacks: LoaderCallbacks[Cursor] = null

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    simpleCursorAdapter = new SampleSimpleCursorAdapter
    loaderCallbacks = new CommonLoaderCallbacks
    setListAdapter(simpleCursorAdapter)
  }

  override def onResume(): Unit = {
    super.onResume()
    getLoaderManager.restartLoader(0, null, loaderCallbacks)
  }

  override def onListItemClick(l: ListView, v: View, position: Int, id: Long): Unit = {
    super.onListItemClick(l, v, position, id)
    val cursor = simpleCursorAdapter.getCursor
    cursor.moveToPosition(position)
    Option(cursor.getColumnIndex(StudentProvider.raw)).
      map(cursor.getString(_)).
      map(SampleObjectMapper.readValue[StudentModel](_)).
      flatMap { model =>
        Option(new Bundle()).chain(_.putSerializable(StudentSchoolHistoryListFragment.student, model))
      }.flatMap { bundle =>
        Option(new StudentSchoolHistoryListFragment).chain(_.setArguments(bundle))
      }.foreach { fragment =>
        getFragmentManager.beginTransaction().transaction { t =>
          t.replace(R.id.main_activity_layout, fragment, StudentSchoolHistoryListFragment.TAG)
          t.addToBackStack(null)
        }
      }
  }

  private class SampleSimpleCursorAdapter extends SimpleCursorAdapter(getActivity, R.layout.student_list_tab_school_history_item, null, Array(), Array(), 0) {
    override def getView(position: Int, convertView: View, parent: ViewGroup): View = {
      val view = super.getView(position, convertView, parent)
      getCursor.moveToPosition(position)
      val studentModel = Option(getCursor.getColumnIndex(StudentProvider.raw)).
        map(getCursor.getString(_)).
        map(SampleObjectMapper.readValue[StudentModel](_))
      val unknown = Option(getResources.getString(R.string.textUnknown))
      Option(view.findViewById(R.id.textStudentName)).
        map(_.asInstanceOf[TextView]).
        foreach(view => studentModel.flatMap(_.name).orElse(unknown).foreach(view.setText(_)))
      Option(view.findViewById(R.id.textSchoolName)).
        map(_.asInstanceOf[TextView]).
        foreach(view => studentModel.flatMap(_.currentStudentSchoolHistory).flatMap(_.school).flatMap(_.name).orElse(unknown).foreach(view.setText(_)))
      Option(view.findViewById(R.id.textCurrentGrade)).
        map(_.asInstanceOf[TextView]).
        foreach(view => studentModel.flatMap(_.currentStudentSchoolHistory).flatMap(_.currentGrade).map(_.toString).orElse(unknown).foreach(view.setText(_)))
      Option(view.findViewById(R.id.imageCurrentAttendingStudent)).map(_.asInstanceOf[ImageView]).foreach { view =>
        studentModel.flatMap(_.currentAttendingStudent) match {
          case Some(true) => view.setImageResource(R.drawable.user)
          case _ => view.setImageResource(-1)
        }
      }
      view
    }
  }

}

object StudentListTabSchoolHistoryFragment {
  val TAG = classOf[StudentListTabSchoolHistoryFragment].getName
  val student = "student"
}
