/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import java.text.DateFormat
import java.util.{ Calendar, Date, Locale }

import android.app.DatePickerDialog.OnDateSetListener
import android.app.{ DatePickerDialog, Fragment }
import android.content.{ AsyncQueryHandler, ContentValues }
import android.net.Uri
import android.os.Bundle
import android.view._
import android.widget.{ DatePicker, EditText, ImageButton }
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.StudentDuesModel

import scala.util.Random

/**
 * Created by i on 4/3/15.
 */
class StudentDuesFormFragment extends Fragment with TimeZoneTrait with FormFragmentTrait with ReadonlyOptionTrait {
  private var editTextStudentDuesId: Option[EditText] = None
  private var editTextStudentName: Option[EditText] = None
  private var editTextAxisPayDate: Option[EditText] = None
  private var editTextPlannedPayDate: Option[EditText] = None
  private var editTextActuallyPayDate: Option[EditText] = None
  private var editTextAmount: Option[EditText] = None
  // 실제납부일을 제외한 나머지는 바꾸는 것은 할수 있어도 빈칸으로 두는건 할수 없어야 한다.
  private var buttonActuallyPayDate: Option[ImageButton] = None
  private var dateFormat: DateFormat = null
  private var calendar: Calendar = null
  private var studentDuesModel: Option[StudentDuesModel] = None
  private var asyncQueryHandler: AsyncQueryHandler = null
  override protected var menu: Menu = null

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    setHasOptionsMenu(true)
    studentDuesModel = Option(savedInstanceState).
      orElse(Option(getArguments)).
      filter(_.containsKey(StudentDuesFormFragment.studentDues)).
      map(_.getSerializable(StudentDuesFormFragment.studentDues)).
      map(_.asInstanceOf[StudentDuesModel])
    dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.getDefault)
    dateFormat.setTimeZone(initTimeZone())
    calendar = Calendar.getInstance(Locale.getDefault)
    calendar.setTimeZone(initTimeZone())
    asyncQueryHandler = new SampleAsyncQueryHandler(() => studentDuesModel.flatMap(_.id).isDefined)
  }

  override def onSaveInstanceState(outState: Bundle): Unit = {
    super.onSaveInstanceState(outState)
    studentDuesModel.foreach(outState.putSerializable(StudentDuesFormFragment.studentDues, _))
  }

  override def onCreateOptionsMenu(menu: Menu, inflater: MenuInflater): Unit = {
    super.onCreateOptionsMenu(menu, inflater)
    this.menu = menu
    inflater.inflate(R.menu.submit_record, menu)
    menu.findItem(R.id.menuItemDelete).setEnabled(studentDuesModel.flatMap(_.id).isDefined)
    processReadonlyMode()
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    item.getItemId match {
      case R.id.menuItemSubmit =>
        submit()
        true
      case R.id.menuItemCancel =>
        getFragmentManager.popBackStack()
        true
      case R.id.menuItemRandom =>
        randomData()
        true
      case R.id.menuItemDelete =>
        delete()
        true
      case _ =>
        super.onOptionsItemSelected(item)
    }
  }

  private def randomData(): Unit = {
    calendar.setTime(new Date())
    calendar.set(Calendar.DAY_OF_YEAR, Random.nextInt(365))
    calendar.set(Calendar.DAY_OF_MONTH, 1)
    calendar.set(Calendar.MONTH, Random.nextInt(12) + 1)
    calendar.set(Calendar.HOUR_OF_DAY, 0)
    calendar.set(Calendar.MINUTE, 0)
    calendar.set(Calendar.SECOND, 0)
    calendar.set(Calendar.MILLISECOND, 0)
    editTextAxisPayDate.foreach(_.setText(dateFormat.format(calendar.getTime)))
    calendar.set(Calendar.DAY_OF_YEAR, Random.nextInt(365))
    editTextPlannedPayDate.foreach(_.setText(dateFormat.format(calendar.getTime)))
    calendar.set(Calendar.DAY_OF_YEAR, Random.nextInt(365))
    editTextActuallyPayDate.foreach(_.setText(dateFormat.format(calendar.getTime)))
    editTextAmount.foreach(_.setText(Random.nextInt(10000).toString))
  }

  private def submit(): Unit = {
    menuItemList.map(menu.findItem(_)).foreach(_.setEnabled(false))
    val newData = prepareData()
    val values = new ContentValues()
    values.put(StudentDuesProvider.raw, SampleObjectMapper.writeValueAsString(newData.get))
    newData.flatMap(_.id) match {
      case Some(_) =>
        val uri = Uri.parse(f"content://${StudentProvider.authority}/${StudentProvider.student}/${studentDuesModel.flatMap(_.studentId).get}/${StudentDuesProvider.studentDues}/${newData.flatMap(_.id).get}")
        asyncQueryHandler.startUpdate(Random.nextInt(), null, uri, values, null, null)
      case None =>
        val uri = Uri.parse(f"content://${StudentProvider.authority}/${StudentProvider.student}/${studentDuesModel.flatMap(_.studentId).get}/${StudentDuesProvider.studentDues}")
        asyncQueryHandler.startInsert(Random.nextInt(), null, uri, values)
    }
  }

  private def delete(): Unit = {
    menuItemList.map(menu.findItem(_)).foreach(_.setEnabled(false))
    val uri = Uri.parse(f"content://${StudentProvider.authority}/${StudentProvider.student}/${studentDuesModel.flatMap(_.studentId).get}/${StudentDuesProvider.studentDues}/${studentDuesModel.flatMap(_.id).get}").
      buildUpon().appendQueryParameter("version", studentDuesModel.flatMap(_.version).map(_.toString).get).build()
    asyncQueryHandler.startDelete(Random.nextInt(), null, uri, null, null)
  }

  private def prepareData(): Option[StudentDuesModel] = {
    val axisPayDate = editTextAxisPayDate.map(_.getText).map(_.toString).filterNot(_.isEmpty).map(dateFormat.parse(_))
    val plannedPayDate = editTextPlannedPayDate.map(_.getText).map(_.toString).filterNot(_.isEmpty).map(dateFormat.parse(_))
    val actuallyPayDate = editTextActuallyPayDate.map(_.getText).map(_.toString).filterNot(_.isEmpty).map(dateFormat.parse(_))
    val amount = editTextAmount.map(_.getText).map(_.toString).filterNot(_.isEmpty).map(_.toInt)
    studentDuesModel.map(_.copy(axisPayDate = axisPayDate, plannedPayDate = plannedPayDate, actuallyPayDate = actuallyPayDate, amount = amount))
  }

  override def onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle): View = {
    super.onCreateView(inflater, container, savedInstanceState)
    val unknown = Option(getResources.getString(R.string.textUnknown))
    val view = inflater.inflate(R.layout.student_dues_form_layout, container, false)
    val today = Option(new Date)
    editTextStudentDuesId = Option(view.findViewById(R.id.editTextStudentDuesId)).map(_.asInstanceOf[EditText]).chain { view =>
      studentDuesModel.flatMap(_.id).map(_.toString).foreach(view.setText(_))
    }
    editTextStudentName = Option(view.findViewById(R.id.editTextStudentName)).map(_.asInstanceOf[EditText]).chain { view =>
      studentDuesModel.flatMap(_.student).flatMap(_.name).orElse(unknown).foreach(view.setText(_))
    }
    editTextAxisPayDate = Option(view.findViewById(R.id.editTextAxisPayDate)).map(_.asInstanceOf[EditText]).chain { view =>
      studentDuesModel.flatMap(_.axisPayDate).map(dateFormat.format(_)).foreach(view.setText(_))
      view.setOnClickListener(editTextClickListener(studentDuesModel.flatMap(_.axisPayDate), View.GONE, Option(1))(_: View))
    }
    editTextPlannedPayDate = Option(view.findViewById(R.id.editTextPlannedPayDate)).map(_.asInstanceOf[EditText]).chain { view =>
      studentDuesModel.flatMap(_.plannedPayDate).map(dateFormat.format(_)).foreach(view.setText(_))
      view.setOnClickListener(editTextClickListener(studentDuesModel.flatMap(_.plannedPayDate))(_: View))
    }
    editTextActuallyPayDate = Option(view.findViewById(R.id.editTextActuallyPayDate)).map(_.asInstanceOf[EditText]).chain { view =>
      studentDuesModel.flatMap(_.actuallyPayDate).map(dateFormat.format(_)).foreach(view.setText(_))
      view.setOnClickListener(editTextClickListener(studentDuesModel.flatMap(_.actuallyPayDate))(_: View))
    }
    buttonActuallyPayDate = Option(view.findViewById(R.id.buttonActuallyPayDate)).map(_.asInstanceOf[ImageButton]).chain { view =>
      view.setOnClickListener(imageClickListener(editTextActuallyPayDate)(_: View))
    }
    editTextAmount = Option(view.findViewById(R.id.editTextAmount)).map(_.asInstanceOf[EditText]).chain { view =>
      studentDuesModel.flatMap(_.amount).map(_.toString).foreach(view.setText(_))
    }
    Option(view.findViewById(R.id.buttonToday)).map(_.asInstanceOf[ImageButton]).chain { view =>
      view.setOnClickListener(todayListener(_: View))
    }
    view
  }

  private def todayListener(v: View): Unit = {
    calendar.setTime(new Date())
    calendar.set(Calendar.HOUR_OF_DAY, 0)
    calendar.set(Calendar.MINUTE, 0)
    calendar.set(Calendar.SECOND, 0)
    calendar.set(Calendar.MILLISECOND, 0)
    editTextActuallyPayDate.foreach(_.setText(dateFormat.format(calendar.getTime)))
  }

  private def imageClickListener(editText: Option[EditText])(v: View): Unit = editText.foreach(_.setText(""))

  private def editTextClickListener(date: Option[Date], visibility: Int = View.VISIBLE, day: Option[Int] = None)(v: View): Unit = {
    date.foreach(calendar.setTime(_))
    val dialog = new DatePickerDialog(getActivity, new SampleOnDateSetListener(v.asInstanceOf[EditText], day), calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
    //    http://stackoverflow.com/questions/21321789/android-datepicker-change-to-only-month-and-year
    Option(getResources.getIdentifier("day", "id", "android")).
      filter(_ != -1).
      flatMap(id => Option(dialog.getDatePicker).flatMap(picker => Option(picker.findViewById(id)))).
      foreach(_.setVisibility(visibility))
    dialog.show()
  }

  private class SampleOnDateSetListener(editText: EditText, day: Option[Int]) extends OnDateSetListener {
    override def onDateSet(view: DatePicker, year: Int, monthOfYear: Int, dayOfMonth: Int): Unit = {
      calendar.set(Calendar.YEAR, year)
      calendar.set(Calendar.MONTH, monthOfYear)
      calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
      day.foreach(calendar.set(Calendar.DAY_OF_MONTH, _))
      calendar.set(Calendar.HOUR_OF_DAY, 0)
      calendar.set(Calendar.MINUTE, 0)
      calendar.set(Calendar.SECOND, 0)
      calendar.set(Calendar.MILLISECOND, 0)
      Option(dateFormat.format(calendar.getTime)).foreach(s => editText.setText(s))
    }
  }

  override def onActivityCreated(savedInstanceState: Bundle): Unit = {
    super.onActivityCreated(savedInstanceState)
    getActivity.getActionBar.setTitle(R.string.titleStudentDuesFormFragment)
  }

}

object StudentDuesFormFragment {
  val TAG = classOf[StudentDuesFormFragment].getName
  val studentDues = "studentDues"
}
