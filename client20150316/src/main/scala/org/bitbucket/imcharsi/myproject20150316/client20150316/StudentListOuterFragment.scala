/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.app.ActionBar.Tab
import android.app.{ ActionBar, Fragment, FragmentTransaction }
import android.os.Bundle
import android.view._
import android.widget.{ Button, EditText }
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.StudentModelEx

/**
 * Created by i on 4/2/15.
 */
class StudentListOuterFragment extends Fragment with ReadonlyOptionTrait {
  private var menu: Menu = null
  private var selectedTabPosition: Option[Int] = None
  private var searchCondition: Option[SearchCondition] = None
  private var currentTabTag: Option[String] = None

  def getSearchCondition: Option[SearchCondition] = this.searchCondition

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    selectedTabPosition = Option(savedInstanceState).
      orElse(Option(getArguments)).
      filter(_.containsKey(StudentListOuterFragment.selectedTabPosition)).
      map(_.getInt(StudentListOuterFragment.selectedTabPosition)).
      orElse(Option(0))
    searchCondition = Option(savedInstanceState).
      orElse(Option(getArguments)).
      filter(_.containsKey(StudentListOuterFragment.searchCondition)).
      map(_.getSerializable(StudentListOuterFragment.searchCondition)).
      map(_.asInstanceOf[SearchCondition]).
      orElse(Option(SearchCondition(None, None, None)))
    setHasOptionsMenu(true)
    // SchoolListFragment 에서 각 학교를 누르면 현재재학이력 기준으로 재학생을 보여주기로 했다.
    // 그런데, 아래와 같이 tab 을 지우지 않으면 SchoolListOuterFragment 에서 만든 tab 과 여기서 만드는 tab 이 쌓여서,
    // 여기서 선택하려는 tab 이 올바른 tab 이 아니게 되는 문제가 생긴다.
    // 예를 들면, 여기서 0번째 tab 을 선택했을 때, 앞에서 넣은 tab 4개와 여기서 넣은 tab 3개를 합해서
    // 7개중에서 가장 처음에 넣은, 앞에서 넣었던 tab 을 선택하게 되는 것이다.
    getActivity.getActionBar.removeAllTabs()
    List[(Int, String, Unit => Fragment)](
      (R.string.titleStudentListTabContactFragment, StudentListTabContactFragment.TAG, _ => new StudentListTabContactFragment),
      (R.string.titleStudentListTabSchoolHistoryFragment, StudentListTabSchoolHistoryFragment.TAG, _ => new StudentListTabSchoolHistoryFragment),
      (R.string.titleStudentListTabDuesFragment, StudentListTabDuesFragment.TAG, _ => new StudentListTabDuesFragment)).foreach {
        case (a, b, c) =>
          val tab = getActivity.getActionBar.newTab().setText(getResources.getString(a)).setTabListener(new SampleTabListener(b, c))
          getActivity.getActionBar.addTab(tab, false)
      }
  }

  override def onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle): View = {
    super.onCreateView(inflater, container, savedInstanceState)
    Option(inflater.inflate(R.layout.student_list_outer_fragment_layout, container, false)).chain { view =>
    }.get
  }

  override def onCreateOptionsMenu(menu: Menu, inflater: MenuInflater): Unit = {
    super.onCreateOptionsMenu(menu, inflater)
    this.menu = menu
    inflater.inflate(R.menu.basic_record_list, menu)
    inflater.inflate(R.menu.search, menu)
    setCurrentAttendingStudentMenuTitle()
    Option(menu.findItem(R.id.menuItemSearch)).map(_.getActionView).foreach { view =>
      Option(view.findViewById(R.id.buttonSearch)).map(_.asInstanceOf[Button]).foreach(_.setOnClickListener(searchListener(_: View)))
    }
    Option(getFragmentManager.findFragmentByTag(MainFragment.TAG)).map(_.asInstanceOf[MainFragment]).map(_.readonly).foreach { flag =>
      menu.findItem(R.id.menuItemCreate).setEnabled(!flag)
    }
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    item.getItemId match {
      case R.id.menuItemCreate =>
        Option(new Bundle()).
          chain(_.putSerializable(StudentFormFragment.studentModel, StudentModelEx(None, None, None, None, None, None, None, None))).
          flatMap { bundle =>
            Option(new StudentFormFragment).chain(_.setArguments(bundle))
          }.foreach { fragment =>
            getFragmentManager.beginTransaction().transaction { t =>
              t.replace(R.id.main_activity_layout, fragment, StudentFormFragment.TAG)
              t.addToBackStack(null)
            }
          }
        true
      case R.id.`menuItemCurrentAttendingStudent` =>
        searchCondition = searchCondition.flatMap(_.currentAttendingStudent) match {
          case None => searchCondition.map(_.copy(currentAttendingStudent = Some(true)))
          case Some(true) => searchCondition.map(_.copy(currentAttendingStudent = Some(false)))
          case Some(false) => searchCondition.map(_.copy(currentAttendingStudent = None))
        }
        setCurrentAttendingStudentMenuTitle()
        refresh()
        true
      case R.id.menuItemSearch =>
        Option(menu.findItem(R.id.menuItemSearch)).map(_.getActionView).foreach { view =>
          Option(view.findViewById(R.id.editTextSearch)).map(_.asInstanceOf[EditText]).foreach(_.setText(searchCondition.flatMap(_.search).getOrElse("")))
        }
        true
      case _ => super.onOptionsItemSelected(item)
    }
  }

  override def onSaveInstanceState(outState: Bundle): Unit = {
    super.onSaveInstanceState(outState)
    selectedTabPosition.foreach(outState.putInt(StudentListOuterFragment.selectedTabPosition, _))
    searchCondition.foreach(outState.putSerializable(StudentListOuterFragment.searchCondition, _))
  }

  // 다른 back stack 이 위에 올라갔다가 없어지면서 내려갔던 fragment 가 올라오면, onCreate 는 실행되지 않는다.
  // 그런데, 여기에서 navigation tab 을 추가하게 되면, 내려갔다 올라오면서 매번 tab 을 추가하게 된다.
  override def onActivityCreated(savedInstanceState: Bundle): Unit = {
    super.onActivityCreated(savedInstanceState)
    getActivity.getActionBar.setTitle(getResources.getString(R.string.titleStudentListOuterFragment))
    getActivity.getActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS)
    selectedTabPosition.foreach(getActivity.getActionBar.getTabAt(_).select())
  }

  private def setCurrentAttendingStudentMenuTitle(): Unit = {
    val nextMenuTitle = searchCondition.flatMap(_.currentAttendingStudent) match {
      case None => R.string.titleMenuItemAllStudent
      case Some(true) => R.string.titleMenuItemCurrentAttendingStudent
      case Some(false) => R.string.titleMenuItemNotCurrentAttendingStudent
    }
    menu.findItem(R.id.menuItemCurrentAttendingStudent).setTitle(nextMenuTitle)
  }

  private def searchListener(v: View): Unit = {
    Option(menu.findItem(R.id.menuItemSearch)).map(_.getActionView).foreach { view =>
      searchCondition = searchCondition.map(_.copy(search = Option(view.findViewById(R.id.editTextSearch)).map(_.asInstanceOf[EditText]).map(_.getText).map(_.toString).filterNot(_.isEmpty)))
      refresh()
    }
  }

  private def refresh(): Unit = {
    // fragment 를 갱신시키고 싶으면?
    // http://stackoverflow.com/questions/15262747/refresh-or-force-redraw-the-fragment
    currentTabTag.map(getFragmentManager.findFragmentByTag(_)).foreach { fragment =>
      getFragmentManager.beginTransaction().transaction { t =>
        t.detach(fragment)
      }
      getFragmentManager.beginTransaction().transaction { t =>
        t.attach(fragment)
      }
    }
  }

  private class SampleTabListener(tag: String, fragmentMaker: Unit => Fragment) extends ActionBar.TabListener {
    private var fragment: Option[Fragment] = None

    override def onTabReselected(tab: Tab, fragmentTransaction: FragmentTransaction): Unit = {}

    override def onTabUnselected(tab: Tab, fragmentTransaction: FragmentTransaction): Unit = {
      fragment.foreach(fragmentTransaction.detach(_))
      currentTabTag = None
    }

    override def onTabSelected(tab: Tab, fragmentTransaction: FragmentTransaction): Unit = {
      val fragment = Option(getFragmentManager.findFragmentByTag(tag))
      this.fragment = Either.cond(fragment.isDefined, fragment, Option(fragmentMaker())) match {
        case Left(fragment) => fragment.chain(fragmentTransaction.add(R.id.studentListOuterFragmentLayout, _, tag))
        case Right(fragment) => fragment.chain(fragmentTransaction.attach(_))
      }
      selectedTabPosition = Option(tab.getPosition)
      currentTabTag = Option(tag)
    }
  }
}

// 아래를 inner class 로 두니, 다른 때에는 문제를 볼수가 없었는데, 전화걸기를 하면서 다른 activity 를 띄우니, serialize 오류가 생긴다. static 으로 하든 밖으로 빼든 해야 한다.
case class SearchCondition(val currentAttendingStudent: Option[Boolean], val search: Option[String], val schoolId: Option[Int])

object StudentListOuterFragment {
  val searchCondition: String = "searchCondition"
  val TAG = classOf[StudentListOuterFragment].getName
  val selectedTabPosition = "selectedTabPosition"
}
