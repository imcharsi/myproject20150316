/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.content.{ ContentUris, ContentValues }
import android.database.{ Cursor, MatrixCursor }
import android.net.Uri
import org.apache.http.client.methods._
import org.apache.http.entity.StringEntity
import org.apache.http.protocol.HTTP
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.{ StudentDuesModel, StudentDuesSummaryModel }

import scala.collection.JavaConversions

/**
 * Created by i on 4/4/15.
 */
trait StudentDuesProvider extends AbstractContentProvider {
  protected def insertStudentDuesModel(uri: Uri, values: ContentValues): Uri = {
    val pathSegments = JavaConversions.collectionAsScalaIterable(uri.getPathSegments).toList
    val (a, studentId, b) = pathSegments match {
      case (a @ StudentProvider.student) :: studentId :: (b @ StudentDuesProvider.studentDues) :: Nil => (a, studentId, b)
    }

    def requestProc: RequestProc = (preferenceValue, uri, values) => {
      Some(new HttpPost(f"${preferenceValue.urlHead}/${a}/${studentId.toInt}/${b}")).chain { httpPost =>
        Some(values.getAsString(StudentDuesProvider.raw)).
          map(new StringEntity(_, HTTP.UTF_8)).
          foreach(httpPost.setEntity(_))
        httpPost.setHeader("Accept", AbstractContentProvider.accept)
        httpPost.setHeader("Content-Type", AbstractContentProvider.contentType)
      }
    }
    def postProc: PostProc[Uri] = (httpResponse) => {
      SampleObjectMapper.readValue[StudentDuesModel](httpResponse.getEntity.getContent).id.map { id =>
        ContentUris.withAppendedId(Uri.parse(f"${StudentProvider.url}/${studentId}/${StudentDuesProvider.studentDues}"), id)
      }
    }
    template[Uri](uri, values, requestProc, postProc, null)
  }

  protected def deleteStudentDuesModel(uri: Uri, values: ContentValues): Int = {
    val pathSegments = JavaConversions.collectionAsScalaIterable(uri.getPathSegments).toList
    val (a, studentId, b, studentDuesId) = pathSegments match {
      case (a @ StudentProvider.student) :: studentId :: (b @ StudentDuesProvider.studentDues) :: studentDuesId :: Nil => (a, studentId, b, studentDuesId)
    }
    def requestProc: RequestProc = (preferenceValue, uri, values) => {
      Some(f"${preferenceValue.urlHead}/${a}/${studentId}/${b}/${studentDuesId}").
        map(Uri.parse(_)).
        map(_.buildUpon()).
        map(_.appendQueryParameter("version", uri.getQueryParameter("version"))).
        map(_.build()).
        map(uri => new HttpDelete(uri.toString))
    }
    def postProc: PostProc[Int] = (httpResponse) => {
      Some(SampleObjectMapper.readValue[Int](httpResponse.getEntity.getContent))
    }
    template[Int](uri, values, requestProc, postProc, 0)
  }

  protected def queryStudentDuesAll(uri: Uri): Cursor = {
    val pathSegments = JavaConversions.collectionAsScalaIterable(uri.getPathSegments).toList
    val (a, studentId, b) = pathSegments match {
      case (a @ StudentProvider.student) :: studentId :: (b @ StudentDuesProvider.studentDues) :: Nil => (a, studentId, b)
    }

    def requestProc: RequestProc = (preferenceValue, uri, _) => {
      Some(new HttpGet(f"${preferenceValue.urlHead}/${a}/${studentId.toInt}/${b}")).
        chain(_.addHeader("Accept", "application/json"))
    }
    def postProc: PostProc[Cursor] = httpResponse => {
      getContext.getResources.getString(R.string.textElementarySchool)
      Some(new MatrixCursor(List(
        StudentDuesProvider.studentDuesId,
        StudentDuesProvider.raw).toArray)).
        chain { matrixCursor =>
          SampleObjectMapper.readValue[List[StudentDuesModel]](httpResponse.getEntity.getContent).
            foreach { model =>
              matrixCursor.
                newRow().
                add(model.id.orNull).
                add(SampleObjectMapper.writeValueAsString(model))
            }
        }
    }
    template[Cursor](uri, null, requestProc, postProc, null)
  }

  protected def queryStudentDuesSummary(uri: Uri): Cursor = {
    val pathSegments = JavaConversions.collectionAsScalaIterable(uri.getPathSegments).toList
    val (a, studentId, b, c) = pathSegments match {
      case (a @ StudentProvider.student) :: studentId :: (b @ StudentDuesProvider.studentDues) :: (c @ StudentDuesProvider.summary) :: Nil => (a, studentId, b, c)
    }

    def requestProc: RequestProc = (preferenceValue, uri, _) => {
      Some(new HttpGet(f"${preferenceValue.urlHead}/${a}/${studentId.toInt}/${b}/${c}")).
        chain(_.addHeader("Accept", AbstractContentProvider.accept))
    }
    def postProc: PostProc[Cursor] = httpResponse => {
      getContext.getResources.getString(R.string.textElementarySchool)
      Some(new MatrixCursor(List(
        StudentDuesProvider.studentDuesId,
        StudentDuesProvider.raw).toArray)).
        chain { matrixCursor =>
          val model = SampleObjectMapper.readValue[StudentDuesSummaryModel](httpResponse.getEntity.getContent)
          matrixCursor.
            newRow().
            add(1).
            add(SampleObjectMapper.writeValueAsString(model))
        }
    }
    template[Cursor](uri, null, requestProc, postProc, null)
  }

  protected def updateStudentDuesModel(uri: Uri, values: ContentValues): Int = {
    val pathSegments = JavaConversions.collectionAsScalaIterable(uri.getPathSegments).toList
    val (a, studentId, b, studentDuesId) = pathSegments match {
      case (a @ StudentProvider.student) :: studentId :: (b @ StudentDuesProvider.studentDues) :: studentDuesId :: Nil => (a, studentId, b, studentDuesId)
    }

    def requestProc: RequestProc = (preferenceValue, uri, values) => {
      Some(new HttpPut(f"${preferenceValue.urlHead}/${a}/${studentId.toInt}/${b}/${studentDuesId}")).chain { httpPut =>
        Some(values.getAsString(StudentDuesProvider.raw)).
          map(new StringEntity(_, HTTP.UTF_8)).
          foreach(httpPut.setEntity(_))
        httpPut.setHeader("Content-Type", AbstractContentProvider.contentType)
      }
    }
    def postProc: PostProc[Int] = (httpResponse) => {
      httpResponse.getStatusLine.getStatusCode match {
        case 200 => Some(1)
        //        case 405 => Some(0)
      }
    }
    template[Int](uri, values, requestProc, postProc, 0)
  }
}

object StudentDuesProvider {
  val studentDuesAll = StudentSchoolHistoryProvider.studentSchoolHistoryEach + 1
  val studentDuesEach = studentDuesAll + 1
  val studentDuesSummary = studentDuesEach + 1

  val studentDuesId = "_id"
  val raw = "raw"
  val studentDues = "studentDues"
  val summary = "summary"

  StudentProvider.uriMatcher.addURI(StudentProvider.authority, f"${StudentProvider.student}/#/${studentDues}", studentDuesAll)
  StudentProvider.uriMatcher.addURI(StudentProvider.authority, f"${StudentProvider.student}/#/${studentDues}/#", studentDuesEach)
  StudentProvider.uriMatcher.addURI(StudentProvider.authority, f"${StudentProvider.student}/#/${studentDues}/${summary}", studentDuesSummary)
}
