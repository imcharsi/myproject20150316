/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.content.{ ContentUris, ContentValues, UriMatcher }
import android.database.{ Cursor, MatrixCursor }
import android.net.Uri
import org.apache.http.client.methods._
import org.apache.http.entity.StringEntity
import org.apache.http.protocol.HTTP
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.{ SchoolModel, Version }

import scala.collection.JavaConversions

/**
 * Created by i on 3/25/15.
 */
class SchoolProvider extends AbstractContentProvider {
  override def onCreate(): Boolean = true

  override def getType(uri: Uri): String = SchoolProvider.uriMatcher.`match`(uri) match {
    case SchoolProvider.schoolAll => f"vnd.android.cursor.dir/${classOf[SchoolModel].getName}"
  }

  override def update(uri: Uri, values: ContentValues, selection: String, selectionArgs: Array[String]): Int = {
    SchoolProvider.uriMatcher.`match`(uri) match {
      case SchoolProvider.schoolEach => updateSchool(uri, values)
    }
  }

  override def insert(uri: Uri, values: ContentValues): Uri = {
    SchoolProvider.uriMatcher.`match`(uri) match {
      case SchoolProvider.schoolAll => insertSchool(uri, values)
    }
  }

  override def delete(uri: Uri, selection: String, selectionArgs: Array[String]): Int = {
    SchoolProvider.uriMatcher.`match`(uri) match {
      case SchoolProvider.schoolEach => deleteSchool(uri)
    }
  }

  override def query(uri: Uri, projection: Array[String], selection: String, selectionArgs: Array[String], sortOrder: String): Cursor = {
    SchoolProvider.uriMatcher.`match`(uri) match {
      case SchoolProvider.schoolAll => querySchoolAll(uri)
    }
  }

  private def querySchoolAll(uri: Uri): Cursor = {
    def requestProc: RequestProc = (preferenceValue, uri, _) => {
      Option(uri.getQueryParameter("searchCondition")).filterNot(_.isEmpty).flatMap { searchCondition =>
        Some(f"${preferenceValue.urlHead}/${SchoolProvider.school}").
          map(Uri.parse(_)).
          map(_.buildUpon()).
          chain { builder =>
            builder.appendQueryParameter("searchCondition", searchCondition)
          }
      }.orElse(Option(Uri.parse(f"${preferenceValue.urlHead}/school").buildUpon())).map(_.build()).
        map(uri => new HttpGet(uri.toString)).
        chain(_.addHeader("Accept", "application/json"))
    }
    def postProc: PostProc[Cursor] = httpResponse => {
      Some(new MatrixCursor(List(SchoolProvider.schoolId, SchoolProvider.raw).toArray)).
        chain { matrixCursor =>
          SampleObjectMapper.readValue[List[SchoolModel]](httpResponse.getEntity.getContent).
            foreach { model =>
              matrixCursor.
                newRow().
                add(model.id.orNull).
                add(SampleObjectMapper.writeValueAsString(model))
            }
        }
    }
    template(uri, null, requestProc, postProc, null)
  }

  private def insertSchool(uri: Uri, values: ContentValues): Uri = {
    def requestProc: RequestProc = (preferenceValue, uri, values) => {
      Some(new HttpPost(f"${preferenceValue.urlHead}/${SchoolProvider.school}")).chain { httpPost =>
        Some(values.getAsString(SchoolProvider.raw)).
          map(new StringEntity(_, HTTP.UTF_8)).
          foreach(httpPost.setEntity(_))
      }.chain(_.addHeader("Accept", AbstractContentProvider.accept)).
        chain(_.addHeader("Content-Type", AbstractContentProvider.contentType))
    }
    def postProc: PostProc[Uri] = (httpResponse) => {
      SampleObjectMapper.readValue[SchoolModel](httpResponse.getEntity.getContent).id.map { id =>
        ContentUris.withAppendedId(SchoolProvider.uri, id)
      }
    }
    template(uri, values, requestProc, postProc, null)
  }

  private def updateSchool(uri: Uri, values: ContentValues): Int = {
    def requestProc: RequestProc = (preferenceValue, uri, values) => {
      val (school, id) = JavaConversions.collectionAsScalaIterable(uri.getPathSegments).toList match {
        case (school @ SchoolProvider.school) :: id :: Nil => (school, id)
      }
      Some(new HttpPut(f"${preferenceValue.urlHead}/${school}/${id}")).chain { httpPut =>
        Some(values.getAsString(SchoolProvider.raw)).
          // utf-8 보내기가 잘 안됐다. http://stackoverflow.com/questions/12799868/android-httppost-json-string-in-utf-8-encoding-to-php
          map(new StringEntity(_, HTTP.UTF_8)).
          foreach(httpPut.setEntity(_))
      }.chain(_.addHeader("Content-Type", AbstractContentProvider.contentType))
    }
    def postProc: PostProc[Int] = (httpResponse) => {
      httpResponse.getStatusLine.getStatusCode match {
        case 200 => Some(1)
        //        case 404 => Some(0)
      }
    }
    template(uri, values, requestProc, postProc, 0)
  }

  private def deleteSchool(uri: Uri): Int = {
    def requestProc: RequestProc = (preferenceValue, uri, values) => {
      val (school, id) = JavaConversions.collectionAsScalaIterable(uri.getPathSegments).toList match {
        case (school @ SchoolProvider.school) :: id :: Nil => (school, id)
      }
      Option(f"${preferenceValue.urlHead}/${school}/${id}").
        map(Uri.parse(_)).
        map(_.buildUpon()).
        map(_.appendQueryParameter(Version.version, uri.getQueryParameter(Version.version))).
        map(_.build()).
        map(uri => new HttpDelete(uri.toString))
    }
    def postProc: PostProc[Int] = (httpResponse) => {
      httpResponse.getStatusLine.getStatusCode match {
        case 200 => Some(1)
        //        case 404 => Some(0)
      }
    }
    template(uri, null, requestProc, postProc, 0)
  }
}

object SchoolProvider {
  val authority = classOf[SchoolProvider].getName

  val schoolAll = 1
  val schoolEach = schoolAll + 1

  val url = f"content://${authority}/school"
  val uri = Uri.parse(url)
  val uriMatcher = new UriMatcher(UriMatcher.NO_MATCH)

  val school = "school"

  val schoolId = "_id"
  val raw = "raw"

  // database 에서는 학교유형 각각에 대한 이름을 알려주지 않는다.
  val schoolTypeText: Map[Int, Int] = Map((1 -> R.string.textElementarySchool), (2 -> R.string.textMiddleSchool), (3 -> R.string.textHighSchool))

  uriMatcher.addURI(authority, "school", schoolAll)
  uriMatcher.addURI(authority, "school/#", schoolEach)
}
