/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.content._
import android.database.{ Cursor, MatrixCursor }
import android.net.Uri
import org.apache.http.client.methods._
import org.apache.http.entity.StringEntity
import org.apache.http.protocol.HTTP
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316._

/**
 * Created by i on 3/21/15.
 */
class StudentProvider extends AbstractContentProvider with StudentContactProvider with StudentSchoolHistoryProvider with StudentDuesProvider {
  override def onCreate(): Boolean = {
    true
  }

  override def getType(uri: Uri): String = StudentProvider.uriMatcher.`match`(uri) match {
    case StudentProvider.`studentAll` => f"vnd.android.cursor.dir/${classOf[StudentModel].getName}"
    case StudentProvider.`studentEach` => f"vnd.android.cursor.item/${classOf[StudentModel].getName}"
  }

  override def update(uri: Uri, values: ContentValues, selection: String, selectionArgs: Array[String]): Int = {
    StudentProvider.uriMatcher.`match`(uri) match {
      case StudentProvider.studentEach => updateStudentModel(uri, values)
      case StudentContactProvider.studentContactEach => updateStudentContactModel(uri, values)
      case StudentSchoolHistoryProvider.studentSchoolHistoryEach => updateStudentSchoolHistoryModel(uri, values)
      case StudentDuesProvider.studentDuesEach => updateStudentDuesModel(uri, values)
    }
  }

  override def insert(uri: Uri, values: ContentValues): Uri = {
    StudentProvider.uriMatcher.`match`(uri) match {
      case StudentProvider.studentAll => insertStudentModel(uri, values)
      case StudentContactProvider.studentContactAll => insertStudentContactModel(uri, values)
      case StudentSchoolHistoryProvider.studentSchoolHistoryAll => insertStudentSchoolHistoryModel(uri, values)
      case StudentDuesProvider.studentDuesAll => insertStudentDuesModel(uri, values)
    }
  }

  override def delete(uri: Uri, selection: String, selectionArgs: Array[String]): Int = {
    StudentProvider.uriMatcher.`match`(uri) match {
      case StudentProvider.studentEach => deleteStudentModel(uri, null)
      case StudentContactProvider.studentContactEach => deleteStudentContactModel(uri, null)
      case StudentSchoolHistoryProvider.studentSchoolHistoryEach => deleteStudentSchoolHistoryModel(uri, null)
      case StudentDuesProvider.studentDuesEach => deleteStudentDuesModel(uri, null)
    }
  }

  override def query(uri: Uri, projection: Array[String], selection: String, selectionArgs: Array[String], sortOrder: String): Cursor = {
    StudentProvider.uriMatcher.`match`(uri) match {
      case StudentProvider.studentAll => queryStudentAll(uri)
      case StudentProvider.studentEach => queryStudentEach(uri)
      case StudentContactProvider.studentContactAll => queryStudentContactAll(uri)
      case StudentSchoolHistoryProvider.studentSchoolHistoryAll => queryStudentSchoolHistoryAll(uri)
      case StudentDuesProvider.studentDuesAll => queryStudentDuesAll(uri)
      case StudentDuesProvider.studentDuesSummary => queryStudentDuesSummary(uri)
    }
  }

  private def queryStudentEach(uri: Uri): Cursor = {
    def requestProc: RequestProc = (preferenceValue, uri, _) => {
      Some(new HttpGet(f"${preferenceValue.urlHead}/${StudentProvider.student}/${uri.getLastPathSegment}")).
        chain(_.addHeader("Accept", AbstractContentProvider.accept))
    }
    def postProc: PostProc[Cursor] = httpResponse => {
      Some(new MatrixCursor(List(StudentProvider.studentId,
        StudentProvider.raw).toArray)).
        chain { matrixCursor =>
          Option(SampleObjectMapper.readValue[StudentModel](httpResponse.getEntity.getContent)).
            foreach { model =>
              matrixCursor.
                newRow().
                add(model.id.orNull).
                add(SampleObjectMapper.writeValueAsString(model)) // ContentValues 와 Cursor 를 사용한 주고받기가 너무 번거롭다. 차라리 이렇게 하는것이 낫겠다.
            }
        }
    }
    template[Cursor](uri, null, requestProc, postProc, null)
  }

  private def queryStudentAll(uri: Uri): Cursor = {
    def requestProc: RequestProc = (preferenceValue, uri, _) => {
      Option(f"${preferenceValue.urlHead}/${StudentProvider.student}").
        map(Uri.parse(_)).
        map(_.buildUpon()).
        flatMap { build =>
          Option(uri.getQueryParameter(StudentProvider.currentAttendingStudent)).
            map(x => build.appendQueryParameter(StudentProvider.currentAttendingStudent, x)).
            orElse(Option(build))
        }.flatMap { build =>
          Option(uri.getQueryParameter(StudentProvider.search)).
            map(x => build.appendQueryParameter(StudentProvider.search, x)).
            orElse(Option(build))
        }.flatMap { build =>
          Option(uri.getQueryParameter(StudentProvider.schoolId)).
            map(x => build.appendQueryParameter(StudentProvider.schoolId, x)).
            orElse(Option(build))
        }.map(_.build()).
        map(uri => new HttpGet(uri.toString)).
        chain(_.addHeader("Accept", AbstractContentProvider.accept))
    }
    def postProc: PostProc[Cursor] = httpResponse => {
      Some(new MatrixCursor(List(StudentProvider.studentId,
        StudentProvider.raw).toArray)).
        chain { matrixCursor =>
          SampleObjectMapper.readValue[List[StudentModel]](httpResponse.getEntity.getContent).
            foreach { model =>
              matrixCursor.
                newRow().
                add(model.id.orNull).
                add(SampleObjectMapper.writeValueAsString(model)) // ContentValues 와 Cursor 를 사용한 주고받기가 너무 번거롭다. 차라리 이렇게 하는것이 낫겠다.
            }
        }
    }
    template[Cursor](uri, null, requestProc, postProc, null)
  }

  private def updateStudentModel(uri: Uri, values: ContentValues): Int = {
    def requestProc: RequestProc = (preferenceValue, _, values) => {
      Some(new HttpPut(f"${preferenceValue.urlHead}/${StudentProvider.student}/${uri.getLastPathSegment}")).chain { httpPut =>
        Some(values.getAsString(StudentProvider.raw)).
          map(new StringEntity(_, HTTP.UTF_8)).
          foreach(httpPut.setEntity(_))
        httpPut.setHeader("Content-Type", AbstractContentProvider.contentType)
      }
    }
    def postProc: PostProc[Int] = (httpResponse) => {
      httpResponse.getStatusLine.getStatusCode match {
        case 200 => Some(1)
        //        case 405 => Some(0) // test 에서는 404 로 했는데, 실제로는 405 가 온다.
      }
    }
    template[Int](uri, values, requestProc, postProc, 0)
  }

  private def insertStudentModel(uri: Uri, values: ContentValues): Uri = {
    def requestProc: RequestProc = (preferenceValue, _, values) => {
      Some(new HttpPost(f"${preferenceValue.urlHead}/${StudentProvider.student}")).chain { httpPost =>
        Some(values.getAsString(StudentProvider.raw)).
          map(new StringEntity(_, HTTP.UTF_8)).
          foreach(httpPost.setEntity(_))
        httpPost.setHeader("Accept", AbstractContentProvider.accept)
        httpPost.setHeader("Content-Type", AbstractContentProvider.contentType)
      }
    }
    def postProc: PostProc[Uri] = (httpResponse) => {
      SampleObjectMapper.readValue[StudentModel](httpResponse.getEntity.getContent).id.map { id =>
        ContentUris.withAppendedId(StudentProvider.uri, id)
      }
    }
    template[Uri](uri, values, requestProc, postProc, null)
  }

  private def deleteStudentModel(uri: Uri, values: ContentValues): Int = {
    def requestProc: RequestProc = (preferenceValue, uri, _) => {
      Option(f"${preferenceValue.urlHead}/${StudentProvider.student}/${uri.getLastPathSegment}").
        map(Uri.parse(_)).
        map(_.buildUpon()).
        map(_.appendQueryParameter(Version.version, uri.getQueryParameter(Version.version))).
        map(_.build()).
        map(uri => new HttpDelete(uri.toString))
    }
    def postProc: PostProc[Int] = (httpResponse) => {
      Some(SampleObjectMapper.readValue[Int](httpResponse.getEntity.getContent))
    }
    template[Int](uri, values, requestProc, postProc, 0)
  }
}

object StudentProvider {
  val studentAll: Int = 1
  val studentEach: Int = studentAll + 1

  val studentId = "_id"
  val raw = "raw" // ContentValues 와 Cursor 를 통해 자료를 주고받는 데 따른 번거로움을 피하기 위해 자료를 json 으로 변환한 다음에 주고 받기로 했다. 이 목적으로 쓴다.

  val student = "student"
  val currentAttendingStudent = "currentAttendingStudent"
  val search = "search"
  val schoolId = "schoolId"

  val uriMatcher: UriMatcher = new UriMatcher(UriMatcher.NO_MATCH)
  val authority: String = classOf[StudentProvider].getName
  val url: String = f"content://${authority}/${student}"
  val uri: Uri = Uri.parse(url)
  uriMatcher.addURI(authority, f"${student}", studentAll)
  uriMatcher.addURI(authority, f"${student}/#", studentEach)
}

