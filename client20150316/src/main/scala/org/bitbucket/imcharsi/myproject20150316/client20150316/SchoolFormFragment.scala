/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.app.{ ActionBar, Fragment }
import android.content.{ AsyncQueryHandler, ContentValues }
import android.database.MatrixCursor
import android.net.Uri
import android.os.Bundle
import android.view._
import android.widget._
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.{ SchoolModel, SchoolType, Version }

import scala.util.Random

/**
 * Created by i on 3/25/15.
 */
class SchoolFormFragment extends Fragment with FormFragmentTrait with ReadonlyOptionTrait {
  private var schoolModel: Option[SchoolModel] = None
  private var spinnerSchoolType: Option[Spinner] = None
  private var editTextSchoolName: Option[EditText] = None
  private var editTextSchoolId: Option[EditText] = None
  override protected var menu: Menu = null
  private var asyncQueryHandler: AsyncQueryHandler = null
  private val schoolTypeList: List[(Int, Int)] = List((SchoolType.unknown, R.string.textUnknown),
    (SchoolType.elementarySchool, R.string.textElementarySchool),
    (SchoolType.middleSchool, R.string.textMiddleSchool),
    (SchoolType.highSchool, R.string.textHighSchool))

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    setHasOptionsMenu(true)
    schoolModel = Option(savedInstanceState).
      orElse(Option(getArguments)).
      filter(_.containsKey(SchoolFormFragment.schoolModel)).
      map(_.getSerializable(SchoolFormFragment.schoolModel)).
      map(_.asInstanceOf[SchoolModel])
    asyncQueryHandler = new SampleAsyncQueryHandler(() => schoolModel.flatMap(_.id).isDefined)
  }

  override def onCreateOptionsMenu(menu: Menu, inflater: MenuInflater): Unit = {
    super.onCreateOptionsMenu(menu, inflater)
    inflater.inflate(R.menu.submit_record, menu)
    // xml 설정에서 기본으로 비활성화시켜놓고 id 가 정의되었을때만 활성화하도록 쓸수도 있었지만 그냥 이렇게 했다.
    this.menu = menu
    menu.findItem(R.id.menuItemDelete).setEnabled(schoolModel.flatMap(_.id).isDefined)
    processReadonlyMode()
  }

  override def onSaveInstanceState(outState: Bundle): Unit = {
    super.onSaveInstanceState(outState)
    schoolModel.foreach(outState.putSerializable(SchoolFormFragment.schoolModel, _))
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    item.getItemId match {
      case R.id.menuItemCancel =>
        getFragmentManager.popBackStack()
        true
      case R.id.menuItemSubmit => menuItemSubmitListener()
      case R.id.menuItemRandom => menuItemRandomListener()
      case R.id.menuItemDelete => menuItemDeleteListener()
      case _ => super.onOptionsItemSelected(item)
    }
  }

  override def onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle): View = {
    super.onCreateView(inflater, container, savedInstanceState)
    Some(inflater.inflate(R.layout.school_form_fragment_layout, container, false)).chain { view =>
      editTextSchoolId = Option(view.findViewById(R.id.editTextSchoolId)).
        map(_.asInstanceOf[EditText]).
        chain { editText =>
          schoolModel.flatMap(_.id).map(_.toString).foreach(editText.setText(_))
        }
      editTextSchoolName = Option(view.findViewById(R.id.editTextSchoolName)).
        map(_.asInstanceOf[EditText]).
        chain { editText =>
          schoolModel.flatMap(_.name).foreach(editText.setText(_))
        }
      spinnerSchoolType = Option(view.findViewById(R.id.spinnerSchoolType)).
        map(_.asInstanceOf[Spinner]).
        chain(initSchoolTypeSpinner(_, schoolModel.flatMap(_.schoolType)))
    }.get
  }

  override def onActivityCreated(savedInstanceState: Bundle): Unit = {
    super.onActivityCreated(savedInstanceState)
    getActivity.getActionBar.setTitle(R.string.titleSchoolFormFragment)
    // 항상, action bar 에서 tab 을 그리고 싶은 쪽에서 다시 그리도록 한다. 따라서 여기에서 지웠어도, back stack 을 통해 회면이 복귀되면 lifecycle 에 따라 화면을 다시 그리는 쪽에서 필요하면 tab 을 다시 그리는 것이다.
    getActivity.getActionBar.removeAllTabs()
    getActivity.getActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD)
  }

  private def menuItemDeleteListener(): Boolean = {
    editTextSchoolId.
      map(_.getText).
      map(_.toString).
      filterNot(_.isEmpty).
      map(_.toInt).
      map(id => f"${SchoolProvider.url}/${id}").
      map(Uri.parse(_)).
      map(_.buildUpon()).
      map(_.appendQueryParameter(Version.version, schoolModel.flatMap(_.version).map(_.toString).get)).
      map(_.build()).
      foreach { uri =>
        asyncQueryHandler.startDelete(Random.nextInt(), null, uri, null, null)
      }
    true
  }

  private def menuItemRandomListener(): Boolean = {
    editTextSchoolName.foreach(_.setText(Random.nextInt().toString))
    spinnerSchoolType.foreach(_.setSelection(Random.nextInt(schoolTypeList.length)))
    true
  }

  private def menuItemSubmitListener(): Boolean = {
    menuItemList.map(menu.findItem(_)).foreach(_.setEnabled(false))
    val schoolId = editTextSchoolId.map(_.getText).map(_.toString).filterNot(_.isEmpty).map(_.toInt)
    val schoolName = editTextSchoolName.map(_.getText).map(_.toString)
    val schoolType = spinnerSchoolType.map(_.getSelectedItemId).
      map(_.toInt).
      filter(schoolTypeList.map(_._1).filter(_ != SchoolType.unknown).contains(_))
    schoolModel.
      map(_.copy(id = schoolId, name = schoolName, schoolType = schoolType)).
      map(SampleObjectMapper.writeValueAsString(_)).
      flatMap { json =>
        Some(new ContentValues()).chain(_.put(SchoolProvider.raw, json))
      }.foreach { values =>
        if (schoolId.isEmpty) {
          asyncQueryHandler.startInsert(Random.nextInt(), null, SchoolProvider.uri, values)
        } else {
          schoolId.
            map(id => f"${SchoolProvider.url}/${id}").
            map(Uri.parse(_)).
            foreach { uri =>
              asyncQueryHandler.startUpdate(Random.nextInt(), null, uri, values, null, null)
            }
        }
      }
    true
  }

  private def initSchoolTypeSpinner(spinner: Spinner, schoolType: Option[Int]): Unit = {
    Some(new MatrixCursor(Array("_id", "text"))).chain { cursor =>
      schoolTypeList.foreach {
        case (a, b) =>
          cursor.newRow().add(a).add(getResources.getString(b))
      }
    }.map { cursor =>
      new SimpleCursorAdapter(getActivity, android.R.layout.simple_spinner_item, cursor, Array("text"), Array(android.R.id.text1), 0)
    }.chain(_.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)).
      chain(spinner.setAdapter(_))
    // database 에 저장할 때는 null 로 저장하고, client 에서 다룰 때는 0 으로 다뤄야 한다. Spinner 는 값의 부재를 다루지 않는다. database 에서 slick 을 거쳐 오면 None 으로 올 수 있다. 따라서 .orElse 로 바꿔준다.
    schoolType.orElse(Option(0)).
      map(schoolTypeList.map(_._1).indexOf(_)). // spinner 는 선택을 위치로 다룬다.
      foreach(spinner.setSelection(_)) // 그래서 spinner 에서 사용되는 자료의 순서를 기준으로, 인자로 주어진 schoolType 을 spinner 선택 위치로 바꾼다.
  }
}

object SchoolFormFragment {
  val TAG = classOf[SchoolFormFragment].getName
  val schoolModel = "schoolModel"
}
