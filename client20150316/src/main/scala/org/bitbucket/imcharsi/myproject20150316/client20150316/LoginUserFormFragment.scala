/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.app.{ ActionBar, Fragment }
import android.content.{ AsyncQueryHandler, ContentValues }
import android.net.Uri
import android.os.Bundle
import android.view._
import android.widget.{ EditText, Toast, ToggleButton }
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.{ LoginUserModel, Version }

import scala.util.Random

/**
 * Created by i on 4/8/15.
 */
class LoginUserFormFragment extends Fragment with FormFragmentTrait with ReadonlyOptionTrait {
  private var loginUserModel: Option[LoginUserModel] = None
  private var editTextLoginUserId: Option[EditText] = None
  private var editTextLoginUserName: Option[EditText] = None
  private var editTextLoginPassword: Option[EditText] = None
  private var editTextLoginPasswordAgain: Option[EditText] = None
  private var editTextPersonName: Option[EditText] = None
  private var toggleAvailable: Option[ToggleButton] = None
  private var asyncQueryHandler: AsyncQueryHandler = null
  protected override var menu: Menu = null

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    loginUserModel = Option(savedInstanceState).
      orElse(Option(getArguments)).
      filter(_.containsKey(LoginUserFormFragment.loginUser)).
      map(_.getSerializable(LoginUserFormFragment.loginUser)).
      map(_.asInstanceOf[LoginUserModel])
    setHasOptionsMenu(true)
    asyncQueryHandler = new SampleAsyncQueryHandler(() => loginUserModel.flatMap(_.id).isDefined)
  }

  override def onSaveInstanceState(outState: Bundle): Unit = {
    super.onSaveInstanceState(outState)
    loginUserModel.foreach(outState.putSerializable(LoginUserFormFragment.loginUser, _))
  }

  override def onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle): View = {
    super.onCreateView(inflater, container, savedInstanceState)
    val view = inflater.inflate(R.layout.login_user_form_layout, container, false)
    editTextLoginUserId = Option(view.findViewById(R.id.editTextLoginUserId)).map(_.asInstanceOf[EditText]).chain { view =>
      loginUserModel.flatMap(_.id).map(_.toString).foreach(view.setText(_))
    }
    editTextLoginUserName = Option(view.findViewById(R.id.editTextLoginUserName)).map(_.asInstanceOf[EditText]).chain { view =>
      loginUserModel.flatMap(_.userName).foreach(view.setText(_))
    }
    editTextLoginPassword = Option(view.findViewById(R.id.editTextLoginPassword)).map(_.asInstanceOf[EditText]).chain { view =>
      loginUserModel.flatMap(_.password).foreach(view.setText(_))
    }
    editTextLoginPasswordAgain = Option(view.findViewById(R.id.editTextLoginPasswordAgain)).map(_.asInstanceOf[EditText]).chain { view =>
      loginUserModel.flatMap(_.password).foreach(view.setText(_))
    }
    editTextPersonName = Option(view.findViewById(R.id.editTextPersonName)).map(_.asInstanceOf[EditText]).chain { view =>
      loginUserModel.flatMap(_.personName).foreach(view.setText(_))
    }
    toggleAvailable = Option(view.findViewById(R.id.toggleAvailable)).map(_.asInstanceOf[ToggleButton]).chain { view =>
      loginUserModel.flatMap(_.available).orElse(Option(false)).foreach(view.setChecked(_))
    }
    view
  }

  override def onCreateOptionsMenu(menu: Menu, inflater: MenuInflater): Unit = {
    super.onCreateOptionsMenu(menu, inflater)
    inflater.inflate(R.menu.submit_record, menu)
    menu.findItem(R.id.menuItemDelete).setEnabled(loginUserModel.flatMap(_.id).isDefined)
    this.menu = menu
    processReadonlyMode()
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    item.getItemId match {
      case R.id.menuItemSubmit =>
        menuItemList.map(id => menu.findItem(id)).foreach(_.setEnabled(false))
        prepareModel() match {
          case Some(model) =>
            val suffix = model.id match {
              case Some(id) => f"/${id}"
              case None => ""
            }
            val uri = Uri.parse(LoginUserProvider.url + suffix)
            Option(SampleObjectMapper.writeValueAsString(model)).flatMap { json =>
              Option(new ContentValues()).chain(_.put(LoginUserProvider.raw, json))
            }.foreach { values =>
              model.id match {
                case Some(id) => asyncQueryHandler.startUpdate(Random.nextInt(), null, uri, values, null, null)
                case None => asyncQueryHandler.startInsert(Random.nextInt(), null, uri, values)
              }
            }
          case None =>
            menuItemList.filter(_ != R.id.menuItemDelete).map(id => menu.findItem(id)).foreach(_.setEnabled(true))
            menu.findItem(R.id.menuItemDelete).setEnabled(loginUserModel.flatMap(_.id).isDefined)
        }
        true
      case R.id.menuItemCancel =>
        getFragmentManager.popBackStack()
        true
      case R.id.menuItemRandom =>
        editTextLoginUserName.foreach(_.setText(Random.nextInt(1000).toString))
        editTextPersonName.foreach(_.setText(Random.nextInt().toString))
        toggleAvailable.foreach(_.setChecked(Random.nextBoolean()))
        true
      case R.id.menuItemDelete =>
        menuItemList.map(id => menu.findItem(id)).foreach(_.setEnabled(false))
        Option(Uri.parse(f"${LoginUserProvider.url}")).
          map(_.buildUpon()).
          map(_.appendPath(loginUserModel.flatMap(_.id).map(_.toString).get)).
          map(_.appendQueryParameter(Version.version, loginUserModel.flatMap(_.version).map(_.toString).get)).
          map(_.build()).
          foreach { uri =>
            asyncQueryHandler.startDelete(Random.nextInt(), null, uri, null, null)
          }
        true
      case _ => super.onOptionsItemSelected(item)
    }
  }

  override def onActivityCreated(savedInstanceState: Bundle): Unit = {
    super.onActivityCreated(savedInstanceState)
    getActivity.getActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD)
    getActivity.getActionBar.setTitle(R.string.titleLoginUserFormFragment)
  }

  private def prepareModel(): Option[LoginUserModel] = {
    val result = loginUserModel.map(_.copy(
      id = editTextLoginUserId.map(_.getText).map(_.toString).filterNot(_.isEmpty).map(_.toInt),
      userName = editTextLoginUserName.map(_.getText).map(_.toString),
      password = editTextLoginPassword.map(_.getText).map(_.toString),
      personName = editTextPersonName.map(_.getText).map(_.toString),
      available = toggleAvailable.map(_.isChecked))).filter(x => x.password == editTextLoginPasswordAgain.map(_.getText).map(_.toString)).
      filter(_.checkUsername).
      filter(_.checkPassword)
    if (result.isEmpty) {
      Toast.makeText(getActivity, getResources.getString(R.string.messageIncorrectPassword), Toast.LENGTH_SHORT).show()
    }
    result
  }
}

object LoginUserFormFragment {
  val TAG = classOf[LoginUserFormFragment].getName
  val loginUser = "loginUser"
}
