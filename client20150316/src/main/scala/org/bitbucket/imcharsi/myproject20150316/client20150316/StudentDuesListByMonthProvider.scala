/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.content.{ ContentValues, UriMatcher }
import android.database.{ Cursor, MatrixCursor }
import android.net.Uri
import org.apache.http.client.methods.{ HttpGet, HttpPost }
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.{ StudentDuesModel, StudentDuesSummaryModel }

import scala.collection.JavaConversions

/**
 * Created by i on 4/5/15.
 */
class StudentDuesListByMonthProvider extends AbstractContentProvider {
  override def getType(uri: Uri): String = ???

  // 쓸일 없다.
  override def update(uri: Uri, values: ContentValues, selection: String, selectionArgs: Array[String]): Int = ???

  override def insert(uri: Uri, values: ContentValues): Uri = {
    StudentDuesListByMonthProvider.uriMatcher.`match`(uri) match {
      case StudentDuesListByMonthProvider.studentDuesListByMonthAll => innerInsert(uri)
    }
  }

  // 쓸일 없다.
  override def delete(uri: Uri, selection: String, selectionArgs: Array[String]): Int = ???

  override def onCreate(): Boolean = true

  override def query(uri: Uri, projection: Array[String], selection: String, selectionArgs: Array[String], sortOrder: String): Cursor = {
    StudentDuesListByMonthProvider.uriMatcher.`match`(uri) match {
      case StudentDuesListByMonthProvider.studentDuesListByMonthAll => innerQuery(uri)
      case StudentDuesListByMonthProvider.studentDuesListByMonthSummary => innerQuerySummary(uri)
    }
  }

  private def innerInsert(uri: Uri): Uri = {
    def requestProc: RequestProc = (preferenceValue, uri, _) => {
      val (studentDues, year, month) = JavaConversions.collectionAsScalaIterable(uri.getPathSegments).toList match {
        case (a @ StudentDuesListByMonthProvider.studentDues) :: year :: month :: Nil => (a, year, month)
      }
      Option(f"${preferenceValue.urlHead}/${studentDues}/${year}/${month}").
        map(Uri.parse(_)).
        map(uri => new HttpPost(uri.toString))
    }
    def postProc: PostProc[Uri] = (response) => {
      Option(response.getEntity.getContent).
        map(SampleObjectMapper.readValue[Int](_)).map { list =>
          uri
        }
    }
    template(uri, null, requestProc, postProc, null)
  }

  private def innerQuery(uri: Uri): Cursor = {
    def requestProc: RequestProc = (preferenceValue, uri, _) => {
      val (studentDues, year, month) = JavaConversions.collectionAsScalaIterable(uri.getPathSegments).toList match {
        case (a @ StudentDuesListByMonthProvider.studentDues) :: year :: month :: Nil => (a, year, month)
      }
      Option(f"${preferenceValue.urlHead}/${studentDues}/${year}/${month}").
        map(Uri.parse(_)).
        map(uri => new HttpGet(uri.toString)).
        chain(_.setHeader("Accept", AbstractContentProvider.accept))
    }
    def postProc: PostProc[Cursor] = (response) => {
      Option(response.getEntity.getContent).
        map(SampleObjectMapper.readValue[List[StudentDuesModel]](_)).map { list =>
          val cursor = new MatrixCursor(Array(StudentDuesListByMonthProvider.studentDuesId, StudentDuesListByMonthProvider.raw))
          list.foreach(model => cursor.newRow().add(model.id.get).add(SampleObjectMapper.writeValueAsString(model)))
          cursor
        }
    }
    template(uri, null, requestProc, postProc, null)
  }

  protected def innerQuerySummary(uri: Uri): Cursor = {
    def requestProc: RequestProc = (preferenceValue, uri, _) => {
      val (studentDues, year, month, summary) = JavaConversions.collectionAsScalaIterable(uri.getPathSegments).toList match {
        case (a @ StudentDuesListByMonthProvider.studentDues) :: year :: month :: (summary @ StudentDuesListByMonthProvider.summary) :: Nil => (a, year, month, summary)
      }
      Option(f"${preferenceValue.urlHead}/${studentDues}/${year}/${month}/${summary}").
        map(Uri.parse(_)).
        map(uri => new HttpGet(uri.toString)).
        chain(_.setHeader("Accept", AbstractContentProvider.accept))
    }
    def postProc: PostProc[Cursor] = httpResponse => {
      getContext.getResources.getString(R.string.textElementarySchool)
      Some(new MatrixCursor(List(
        StudentDuesProvider.studentDuesId,
        StudentDuesProvider.raw).toArray)).
        chain { matrixCursor =>
          val model = SampleObjectMapper.readValue[StudentDuesSummaryModel](httpResponse.getEntity.getContent)
          matrixCursor.
            newRow().
            add(1).
            add(SampleObjectMapper.writeValueAsString(model))
        }
    }
    template[Cursor](uri, null, requestProc, postProc, null)
  }
}

object StudentDuesListByMonthProvider {
  val authority = classOf[StudentDuesListByMonthProvider].getName
  val studentDuesListByMonthAll: Int = 1
  val studentDuesListByMonthSummary: Int = 2

  val studentDues = "studentDues"
  val summary = "summary"
  val studentDuesId = "_id"
  val raw = "raw"

  val uriMatcher = new UriMatcher(UriMatcher.NO_MATCH)
  uriMatcher.addURI(authority, f"${studentDues}/#/#", studentDuesListByMonthAll)
  uriMatcher.addURI(authority, f"${studentDues}/#/#/${summary}", studentDuesListByMonthSummary)
}
