/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.app.FragmentTransaction
import android.view.MenuItem.OnMenuItemClickListener
import android.view.View.OnClickListener
import android.view.{ MenuItem, View }
import android.widget.CompoundButton
import android.widget.CompoundButton.OnCheckedChangeListener

/**
 * Created by i on 3/20/15.
 */
object SampleUtil {

  private class OnClickListenerWrapper(l: View => Any) extends OnClickListener {
    override def onClick(view: View) = l(view)
  }

  implicit class FragmentTransactionUtil(t: FragmentTransaction) {
    def transaction(x: FragmentTransaction => Unit): Unit = {
      x(t)
      t.commit()
    }
  }

  private class OnMenuItemClickListenerWrapper(l: MenuItem => Boolean) extends OnMenuItemClickListener {
    override def onMenuItemClick(item: MenuItem): Boolean = l(item)
  }

  private class OnCheckedChangeListenerWrapper(l: (CompoundButton, Boolean) => Any) extends OnCheckedChangeListener {
    override def onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean): Unit = l(buttonView, isChecked)
  }

  implicit def onClickListenerUtil(l: View => Any): OnClickListener = new OnClickListenerWrapper(l)

  implicit def onCheckedChangeListenerUtil(l: (CompoundButton, Boolean) => Any): OnCheckedChangeListener = new OnCheckedChangeListenerWrapper(l)

  implicit def onMenuItemClickListenerUtil(l: MenuItem => Boolean): OnMenuItemClickListener = new OnMenuItemClickListenerWrapper(l)

  implicit class OptionChain[T](o: Option[T]) {
    def chain(f: T => Any): Option[T] = {
      o.foreach(f)
      o
    }
  }

}
