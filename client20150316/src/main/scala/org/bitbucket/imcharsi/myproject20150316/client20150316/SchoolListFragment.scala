/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import android.app.ListFragment
import android.app.LoaderManager.LoaderCallbacks
import android.content.{ CursorLoader, Loader }
import android.database.Cursor
import android.os.Bundle
import android.view.ContextMenu.ContextMenuInfo
import android.view._
import android.widget.AdapterView.AdapterContextMenuInfo
import android.widget.{ ListView, SimpleCursorAdapter, TextView }
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.SchoolModel

import scala.util.Random

/**
 * Created by i on 3/25/15.
 */
class SchoolListFragment extends ListFragment with ListFragmentTrait with ReadonlyOptionTrait {
  private var loaderCallbacks: LoaderCallbacks[Cursor] = null
  override protected var simpleCursorAdapter: SimpleCursorAdapter = null
  private var searchCondition: Option[Int] = None
  override protected var menu: Menu = null

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    setHasOptionsMenu(true)
    loaderCallbacks = new SchoolLoaderCallbacks
    searchCondition = Option(savedInstanceState).
      orElse(Option(getArguments)).
      filter(_.containsKey(SchoolListFragment.searchCondition)).
      map(_.getInt(SchoolListFragment.searchCondition))
  }

  override def onStart(): Unit = {
    super.onStart()
    registerForContextMenu(getListView)
  }

  override def onCreateOptionsMenu(menu: Menu, inflater: MenuInflater): Unit = {
    super.onCreateOptionsMenu(menu, inflater)
    inflater.inflate(R.menu.basic_record_list, menu)
    this.menu = menu
    processReadonlyMode()
  }

  override def onSaveInstanceState(outState: Bundle): Unit = {
    super.onSaveInstanceState(outState)
    searchCondition.foreach(outState.putInt(SchoolListFragment.searchCondition, _))
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    item.getItemId match {
      case R.id.menuItemCreate =>
        getFragmentManager.beginTransaction().transaction { t =>
          Some(SchoolModel(None, None, Some(Random.nextInt().toString), Some(Random.nextInt(3) + 1))).flatMap { model =>
            Some(new Bundle()).chain(_.putSerializable(SchoolFormFragment.schoolModel, model))
          }.flatMap { bundle =>
            Some(new SchoolFormFragment).chain(_.setArguments(bundle))
          }.foreach(t.replace(R.id.main_activity_layout, _, SchoolFormFragment.TAG))
          t.addToBackStack(null)
        }
        true
      case _ => super.onOptionsItemSelected(item)
    }
  }

  override def onListItemClick(l: ListView, v: View, position: Int, id: Long): Unit = {
    super.onListItemClick(l, v, position, id)
    Option(simpleCursorAdapter.getCursor).map { cursor =>
      cursor.moveToPosition(position)
      cursor.getString(cursor.getColumnIndex(SchoolProvider.raw))
    }.map { json =>
      SampleObjectMapper.readValue[SchoolModel](json)
    }.map(model => SearchCondition(None, None, model.id)).
      flatMap { searchCondition =>
        Some(new Bundle()).chain(_.putSerializable(StudentListOuterFragment.searchCondition, searchCondition))
      }.flatMap { bundle =>
        Some(new StudentListOuterFragment).chain(_.setArguments(bundle))
      }.foreach { fragment =>
        getFragmentManager.beginTransaction().transaction { t =>
          t.replace(R.id.main_activity_layout, fragment, StudentListOuterFragment.TAG)
          t.addToBackStack(null)
        }
      }
  }

  override def onActivityCreated(savedInstanceState: Bundle): Unit = {
    super.onActivityCreated(savedInstanceState)
    // 아래가 onCreate 에 있으면 detach 에서 다시 attach 를 썼을 때, onCreate lifecycle 이 시작되지 않으면서 결국 아래의 필요한 문장도 실행되지 않게 된다.
    getActivity.getActionBar.setTitle(R.string.titleSchoolListFragment)
    simpleCursorAdapter = new SampleSimpleCursorAdapter
    setListAdapter(simpleCursorAdapter)
  }

  override def onResume(): Unit = {
    super.onResume()
    getLoaderManager.restartLoader(0, null, loaderCallbacks)
  }

  override def onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenuInfo): Unit = {
    super.onCreateContextMenu(menu, v, menuInfo)
    getActivity.getMenuInflater.inflate(R.menu.edit_context_menu, menu)
  }

  override def onContextItemSelected(item: MenuItem): Boolean = {
    item.getItemId match {
      case R.id.menuItemEdit =>
        val cursor = simpleCursorAdapter.getCursor
        Option(simpleCursorAdapter.getCursor).map { cursor =>
          cursor.moveToPosition(item.getMenuInfo.asInstanceOf[AdapterContextMenuInfo].position)
          cursor.getString(cursor.getColumnIndex(SchoolProvider.raw))
        }.map { json =>
          SampleObjectMapper.readValue[SchoolModel](json)
        }.flatMap { model =>
          Some(new Bundle()).chain(_.putSerializable(SchoolFormFragment.schoolModel, model))
        }.flatMap { bundle =>
          Some(new SchoolFormFragment).chain(_.setArguments(bundle))
        }.foreach { fragment =>
          getFragmentManager.beginTransaction().transaction { t =>
            t.replace(R.id.main_activity_layout, fragment, SchoolFormFragment.TAG)
            t.addToBackStack(null)
          }
        }
        true
      case _ => super.onContextItemSelected(item)
    }
  }

  private class SampleSimpleCursorAdapter extends SimpleCursorAdapter(getActivity, android.R.layout.simple_list_item_2, null, Array(), Array(), 0) {
    override def getView(position: Int, convertView: View, parent: ViewGroup): View = {
      val view = super.getView(position, convertView, parent)
      getCursor.moveToPosition(position)
      val schoolModel = Option(getCursor.getColumnIndex(SchoolProvider.raw)).map(getCursor.getString(_)).map(SampleObjectMapper.readValue[SchoolModel](_))
      val unknown = Option(getResources.getString(R.string.textUnknown))
      Option(view.findViewById(android.R.id.text1)).map(_.asInstanceOf[TextView]).
        foreach(view => schoolModel.flatMap(_.name).orElse(unknown).foreach(view.setText(_)))
      Option(view.findViewById(android.R.id.text2)).map(_.asInstanceOf[TextView]).
        foreach(view => schoolModel.flatMap(_.schoolType).flatMap(SchoolProvider.schoolTypeText.get(_)).map(getResources.getString(_)).orElse(unknown).foreach(view.setText(_)))
      view
    }
  }

  private class SchoolLoaderCallbacks extends CommonLoaderCallbacks {
    override def onCreateLoader(id: Int, args: Bundle): Loader[Cursor] = {
      setListShown(false)
      // http://stackoverflow.com/questions/2959316/how-to-add-parameters-to-a-http-get-request-in-android
      Some(SchoolProvider.uri.buildUpon()).flatMap { builder =>
        searchCondition.map { x => builder.appendQueryParameter("searchCondition", x.toString) }
      }.orElse(Option(SchoolProvider.uri.buildUpon())).map(_.build()).map { uri =>
        new CursorLoader(getActivity, uri, null, null, null, null)
      }.get
    }
  }
}

object SchoolListFragment {
  val TAG = classOf[SchoolListFragment].getName
  val TAG_ALL = classOf[SchoolListFragment].getName + "All"
  val TAG_ELEMENTARY = classOf[SchoolListFragment].getName + "Elementary"
  val TAG_MIDDLE = classOf[SchoolListFragment].getName + "Middle"
  val TAG_HIGH = classOf[SchoolListFragment].getName + "High"
  val searchCondition = "searchCondition"
}
