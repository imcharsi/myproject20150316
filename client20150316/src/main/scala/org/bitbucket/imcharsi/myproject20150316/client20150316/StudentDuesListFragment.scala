/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import java.text.{ DateFormat, NumberFormat }
import java.util.Locale

import android.app.LoaderManager.LoaderCallbacks
import android.app.{ ActionBar, ListFragment }
import android.content.{ CursorLoader, Loader }
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.view._
import android.widget.{ ImageView, ListView, SimpleCursorAdapter, TextView }
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.{ StudentDuesModel, StudentDuesSummaryModel, StudentModel }

/**
 * Created by i on 4/3/15.
 */
class StudentDuesListFragment extends ListFragment with TimeZoneTrait with ListFragmentTrait with ReadonlyOptionTrait {
  override protected var simpleCursorAdapter: SimpleCursorAdapter = null
  private var studentModel: Option[StudentModel] = None
  private var loaderCallbacks: LoaderCallbacks[Cursor] = null
  private var loaderCallbacks2: LoaderCallbacks[Cursor] = null
  private var currencyFormat: NumberFormat = null
  override protected var menu: Menu = null

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    simpleCursorAdapter = new SampleSimpleCursorAdapter
    loaderCallbacks = new SampleLoaderCallbacks
    loaderCallbacks2 = new SampleLoaderCallbacks2
    studentModel = Option(savedInstanceState).orElse(Option(getArguments)).
      filter(_.containsKey(StudentDuesListFragment.student)).
      map(_.getSerializable(StudentDuesListFragment.student)).
      map(_.asInstanceOf[StudentModel])
    currencyFormat = NumberFormat.getCurrencyInstance(Locale.getDefault)
    setListAdapter(simpleCursorAdapter)
    setHasOptionsMenu(true)
  }

  override def onSaveInstanceState(outState: Bundle): Unit = {
    super.onSaveInstanceState(outState)
    studentModel.foreach(outState.putSerializable(StudentDuesListFragment.student, _))
  }

  override def onCreateOptionsMenu(menu: Menu, inflater: MenuInflater): Unit = {
    super.onCreateOptionsMenu(menu, inflater)
    inflater.inflate(R.menu.basic_record_list, menu)
    this.menu = menu
    processReadonlyMode()
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    item.getItemId match {
      case R.id.menuItemCreate =>
        Option(StudentDuesModel(None, None, studentModel.flatMap(_.id), None, None, None, None, studentModel)).
          flatMap { model =>
            Option(new Bundle()).chain(_.putSerializable(StudentDuesFormFragment.studentDues, model))
          }.flatMap { bundle =>
            Option(new StudentDuesFormFragment).chain(_.setArguments(bundle))
          }.foreach { fragment =>
            getFragmentManager.beginTransaction().transaction { t =>
              t.replace(R.id.main_activity_layout, fragment, StudentDuesFormFragment.TAG)
              t.addToBackStack(null)
            }
          }
        true
      case _ => super.onOptionsItemSelected(item)
    }
  }

  override def onActivityCreated(savedInstanceState: Bundle): Unit = {
    super.onActivityCreated(savedInstanceState)
    getActivity.getActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD)
    getActivity.getActionBar.setTitle(R.string.titleStudentDuesListFragment)
  }

  override def onResume(): Unit = {
    super.onResume()
    getLoaderManager.restartLoader(0, null, loaderCallbacks)
    getLoaderManager.restartLoader(1, null, loaderCallbacks2)
  }

  private class SampleLoaderCallbacks extends CommonLoaderCallbacks {
    override def onCreateLoader(id: Int, args: Bundle): Loader[Cursor] = {
      setListShown(false)
      val uri = studentModel.flatMap(_.id).map(id => f"${StudentProvider.url}/${id}/${StudentDuesProvider.studentDues}").map(Uri.parse(_))
      new CursorLoader(getActivity, uri.get, null, null, null, null)
    }
  }

  private class SampleLoaderCallbacks2 extends LoaderCallbacks[Cursor] {
    override def onLoaderReset(loader: Loader[Cursor]): Unit = {}

    override def onLoadFinished(loader: Loader[Cursor], data: Cursor): Unit = {
      if (data != null) {
        data.moveToFirst()
        val model = Option(data.getColumnIndex(StudentDuesListByMonthProvider.raw)).map(data.getString(_)).map(SampleObjectMapper.readValue[StudentDuesSummaryModel](_))
        Option(getView.findViewById(R.id.textAllSum)).map(_.asInstanceOf[TextView]).foreach { view =>
          model.flatMap(_.allSum).orElse(Option(0)).map(currencyFormat.format(_)).foreach(view.setText(_))
        }
        Option(getView.findViewById(R.id.textCompleteSum)).map(_.asInstanceOf[TextView]).foreach { view =>
          model.flatMap(_.completeSum).orElse(Option(0)).map(currencyFormat.format(_)).foreach(view.setText(_))
        }
        Option(getView.findViewById(R.id.textIncompleteSum)).map(_.asInstanceOf[TextView]).foreach { view =>
          model.flatMap(_.incompleteSum).orElse(Option(0)).map(currencyFormat.format(_)).foreach(view.setText(_))
        }
      }
    }

    override def onCreateLoader(id: Int, args: Bundle): Loader[Cursor] = {
      val uri = studentModel.flatMap(_.id).map(id => f"${StudentProvider.url}/${id}/${StudentDuesProvider.studentDues}/${StudentDuesProvider.summary}").map(Uri.parse(_))
      new CursorLoader(getActivity, uri.get, null, null, null, null)
    }
  }

  override def onListItemClick(l: ListView, v: View, position: Int, id: Long): Unit = {
    super.onListItemClick(l, v, position, id)
    simpleCursorAdapter.getCursor.moveToPosition(position)
    Option(simpleCursorAdapter.getCursor.getColumnIndex(StudentDuesProvider.raw)).
      map(simpleCursorAdapter.getCursor.getString(_)).
      map(SampleObjectMapper.readValue[StudentDuesModel](_)).flatMap { model =>
        Option(new Bundle()).chain(_.putSerializable(StudentDuesFormFragment.studentDues, model))
      }.flatMap { bundle =>
        Option(new StudentDuesFormFragment).chain(_.setArguments(bundle))
      }.foreach { fragment =>
        getFragmentManager.beginTransaction().transaction { t =>
          t.replace(R.id.main_activity_layout, fragment, StudentDuesFormFragment.TAG)
          t.addToBackStack(null)
        }
      }
  }

  private class SampleSimpleCursorAdapter extends SimpleCursorAdapter(getActivity, R.layout.student_dues_list_item, null, Array(), Array(), 0) {
    private val dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.getDefault)
    // 여기서 timezone 을 바꿔야 하는 이유는, database 에서는 database 가 설치된 system 의 timezone 에 맞춰서 시각을 저장하기 때문이다.
    // 문제의 원인은, 사용자로부터 받는 날짜입력이 사람이 알아보기 쉬운 형태의 일부입력이기 때문에, timezone 에 따른 차이가 번역된 날짜문장에서 빠지게 되고,
    // 결국 입력칸에 들어가는 날짜문장이 실제 날짜와 약간 달라지게 되는데, 이 문장을 다시 날짜로 번역할때 시각이 완전히 틀려지게 되는 것이다.
    // 예를 들면, 이 timezone 에서는 1월 1일 00:00:00 이었는데 저 timezone 에서는 12월 31일 23:00:00 이라 하자.
    // 번역된 문장 12월 31일을 날짜로 바꾸면 12월 31일 00:00:00 로 해석되어 server 로 보내지게 되고 server 는 이것을 그대로 저장하는데,
    // 저장한 것을 다시 받으면 client 는 12월 31일 00:00:00 과 12월 30일 23:00:00 로 받게 되는데, timezone 에 따른 차이일뿐 날짜 자체는 같지만,
    // 이것을 다시 문장으로 만들면 시각은 빠지고 날짜만 번역하여 12월 30일 이 되고, 이것을 또 날짜로 바꾸면 12월 30일 00:00:00 으로 바꾸는 것이다.
    // 애초에 문장 12월 31일 을 12월 31일 23:00:00 으로 제대로 바꿨더라면 아무 문제가 없는 것이다.
    // 그런데, 입력칸에서는 시각을 쓰지않고, 시각을 따로 저장하지도 않는다. 따라서, 이와 같이 timezone 을 바꿔준다.
    dateFormat.setTimeZone(initTimeZone())

    override def getView(position: Int, convertView: View, parent: ViewGroup): View = {
      val view = super.getView(position, convertView, parent)
      val unknown = Option(getResources.getString(R.string.textUnknown))
      getCursor.moveToPosition(position)
      val studentDuesModel = Option(getCursor.getColumnIndex(StudentDuesProvider.raw)).map(getCursor.getString(_)).map(SampleObjectMapper.readValue[StudentDuesModel](_))
      Option(view.findViewById(R.id.textStudentName)).
        map(_.asInstanceOf[TextView]).
        foreach(view => studentDuesModel.flatMap(_.student).flatMap(_.name).orElse(unknown).foreach(view.setText(_)))
      Option(view.findViewById(R.id.textAxisPayDate)).
        map(_.asInstanceOf[TextView]).
        foreach(view => studentDuesModel.flatMap(_.axisPayDate).map(dateFormat.format(_)).orElse(unknown).foreach(view.setText(_)))
      Option(view.findViewById(R.id.textPlannedPayDate)).
        map(_.asInstanceOf[TextView]).
        foreach(view => studentDuesModel.flatMap(_.plannedPayDate).map(dateFormat.format(_)).orElse(unknown).foreach(view.setText(_)))
      val drawableId = studentDuesModel.flatMap(_.actuallyPayDate) match {
        case Some(_) => R.drawable.check
        case _ => R.drawable.question
      }
      Option(view.findViewById(R.id.imageComplete)).
        map(_.asInstanceOf[ImageView]).foreach(_.setImageResource(drawableId))
      view
    }
  }

  override def onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle): View = {
    super.onCreateView(inflater, container, savedInstanceState)
    inflater.inflate(R.layout.student_dues_list_layout, container, false)
  }
}

object StudentDuesListFragment {
  val TAG = classOf[StudentDuesListFragment].getName
  val student = "student"
}
