/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import java.text.DateFormat
import java.util.{ Calendar, Locale }

import android.app.DatePickerDialog.OnDateSetListener
import android.app.LoaderManager.LoaderCallbacks
import android.app.{ DatePickerDialog, Fragment }
import android.content.{ AsyncQueryHandler, ContentValues, CursorLoader, Loader }
import android.database.{ Cursor, MatrixCursor, MergeCursor }
import android.os.Bundle
import android.util.Log
import android.view._
import android.widget.AdapterView.OnItemSelectedListener
import android.widget._
import org.bitbucket.imcharsi.myproject20150316.client20150316.SampleUtil._
import org.bitbucket.imcharsi.myproject20150316.model20150316.{ SchoolModel, SchoolType, StudentSchoolHistoryModel, Version }

import scala.util.Random

/**
 * Created by i on 3/28/15.
 */
class StudentSchoolHistoryFormFragment extends Fragment with TimeZoneTrait with FormFragmentTrait with ReadonlyOptionTrait {
  private var schoolLoaderCallbacks: LoaderCallbacks[Cursor] = null
  private var spinnerSchool: Option[Spinner] = None
  private var spinnerCurrentGrade: Option[Spinner] = None
  private var schoolSimpleCursorAdapter: SimpleCursorAdapter = null
  private var currentGradeSimpleCursorAdapter: SimpleCursorAdapter = null
  private var studentSchoolHistoryModel: Option[StudentSchoolHistoryModel] = None
  //  private var datePickerCurrentDat: Option[DatePicker] = None
  private var editTextCurrentDate: Option[EditText] = None
  private var editTextStudentSchoolHistoryId: Option[EditText] = None
  private var editTextStudentName: Option[EditText] = None
  private var editTextClassNumber: Option[EditText] = None
  private var toggleCurrentStudentSchoolHistory: Option[ToggleButton] = None
  // 내가 잘못 알고 있나. spinner 는 기계를 회전시킬 때 상태가 유지되지 않는 듯 하다?
  private var currentSelectedSchoolId: Option[Int] = None
  private var currentSelectedGrade: Option[Int] = None
  protected override var menu: Menu = null
  private var asyncQueryHandler: AsyncQueryHandler = null
  private var dateFormat: DateFormat = null
  private var calendar: Calendar = null

  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState)
    schoolLoaderCallbacks = new SchoolLoaderCallbacks
    schoolSimpleCursorAdapter = new SampleSimpleCursorAdapter
    schoolSimpleCursorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    studentSchoolHistoryModel = Option(savedInstanceState).
      orElse(Option(getArguments)).
      filter(_.containsKey(StudentSchoolHistoryFormFragment.studentSchoolHistoryModel)).
      map(_.getSerializable(StudentSchoolHistoryFormFragment.studentSchoolHistoryModel)).
      map(_.asInstanceOf[StudentSchoolHistoryModel])
    currentSelectedSchoolId = Option(savedInstanceState).
      filter(_.containsKey(StudentSchoolHistoryFormFragment.currentSelectedSchoolId)).
      map(_.getInt(StudentSchoolHistoryFormFragment.currentSelectedSchoolId)).
      orElse(studentSchoolHistoryModel.flatMap(_.schoolId)) // 앞에서 저장했던 상태가 없다면, 최초로 전달된 내용을 쓴다.
    currentSelectedGrade = Option(savedInstanceState).
      filter(_.containsKey(StudentSchoolHistoryFormFragment.currentSelectedGrade)).
      map(_.getInt(StudentSchoolHistoryFormFragment.currentSelectedGrade)).
      orElse(studentSchoolHistoryModel.flatMap(_.currentGrade)) // 앞에서 저장했던 상태가 없다면, 최초로 전달된 내용을 쓴다.
    currentGradeSimpleCursorAdapter = new SimpleCursorAdapter(getActivity, android.R.layout.simple_spinner_item, null, Array(StudentSchoolHistoryFormFragment.grade), Array(android.R.id.text1), 0)
    currentGradeSimpleCursorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
    getActivity.getActionBar.setTitle(R.string.titleStudentSchoolHistoryFormFragment)
    setHasOptionsMenu(true)
    asyncQueryHandler = new SampleAsyncQueryHandler(() => studentSchoolHistoryModel.flatMap(_.id).isDefined)
    dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.getDefault)
    dateFormat.setTimeZone(initTimeZone())
    calendar = Calendar.getInstance(Locale.getDefault)
    calendar.setTimeZone(initTimeZone())
  }

  override def onCreateOptionsMenu(menu: Menu, inflater: MenuInflater): Unit = {
    super.onCreateOptionsMenu(menu, inflater)
    inflater.inflate(R.menu.submit_record, menu)
    menu.findItem(R.id.menuItemDelete).setEnabled(studentSchoolHistoryModel.flatMap(_.id).isDefined)
    this.menu = menu
    processReadonlyMode()
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    item.getItemId match {
      case R.id.menuItemSubmit =>
        List(R.id.menuItemCancel, R.id.menuItemSubmit, R.id.menuItemRandom, R.id.menuItemDelete).
          flatMap(id => Option(menu.findItem(id)).toList).
          foreach(_.setEnabled(false))
        val schoolCursor = schoolSimpleCursorAdapter.getCursor
        val schoolId = spinnerSchool.map(_.getSelectedItemPosition).filter(_ != 0).
          chain { position =>
            schoolCursor.moveToPosition(position)
          }.map(_ => schoolCursor.getInt(schoolCursor.getColumnIndex(SchoolProvider.schoolId)))
        val gradeCursor = currentGradeSimpleCursorAdapter.getCursor
        val grade = spinnerCurrentGrade.map(_.getSelectedItemPosition).filter(_ != 0).
          chain { position =>
            gradeCursor.moveToPosition(position)
          }.map(_ => gradeCursor.getInt(gradeCursor.getColumnIndex(StudentSchoolHistoryFormFragment.grade)))
        val classNumber = editTextClassNumber.map(_.getText).map(_.toString).filterNot(_.isEmpty).map(_.toInt)
        val currentDate = editTextCurrentDate.map(_.getText).map(_.toString).filterNot(_.isEmpty).map(dateFormat.parse(_))
        val currentStudentSchoolHistory = toggleCurrentStudentSchoolHistory.map(_.isChecked)
        val newModel = studentSchoolHistoryModel.map(_.copy(schoolId = schoolId, currentDate = currentDate, currentGrade = grade, classNumber = classNumber, currentStudentSchoolHistory = currentStudentSchoolHistory))
        studentSchoolHistoryModel.foreach(m => Log.i(getClass.getName, m.toString))
        newModel.foreach(m => Log.i(getClass.getName, m.toString))
        val values = newModel.map(SampleObjectMapper.writeValueAsString(_)).flatMap { json =>
          Some(new ContentValues()).chain(_.put(StudentSchoolHistoryProvider.raw, json))
        }
        val uri = Option(StudentProvider.uri.buildUpon()).chain { uri =>
          studentSchoolHistoryModel.flatMap(_.studentId).map(_.toString).foreach(uri.appendPath(_))
        }.chain(_.appendPath("studentSchoolHistory")).
          chain { build =>
            newModel.flatMap(_.id).map(_.toString).foreach(build.appendPath(_))
          }.map(_.build())
        values.zip(uri).foreach {
          case (values, uri) =>
            newModel.flatMap(_.id).isDefined match {
              case true => asyncQueryHandler.startUpdate(Random.nextInt(), null, uri, values, null, null)
              case false => asyncQueryHandler.startInsert(Random.nextInt(), null, uri, values)
            }
        }
        true
      case R.id.menuItemCancel =>
        getFragmentManager.popBackStack()
        true
      case R.id.menuItemRandom =>
        spinnerSchool.foreach { spinner =>
          spinner.setSelection(Random.nextInt(schoolSimpleCursorAdapter.getCursor.getCount))
        }
        spinnerCurrentGrade.foreach { spinner =>
          spinner.setSelection(Random.nextInt(currentGradeSimpleCursorAdapter.getCursor.getCount))
        }
        calendar.set(Calendar.YEAR, 2015)
        calendar.set(Calendar.DAY_OF_YEAR, Random.nextInt(366))
        editTextCurrentDate.foreach(_.setText(dateFormat.format(calendar.getTime)))
        editTextClassNumber.foreach(_.setText(Random.nextInt(10).toString))
        toggleCurrentStudentSchoolHistory.foreach(_.setChecked(Random.nextBoolean()))
        true
      case R.id.menuItemDelete =>
        val uri = Option(StudentProvider.uri.buildUpon()).chain { uri =>
          studentSchoolHistoryModel.flatMap(_.studentId).map(_.toString).foreach(uri.appendPath(_))
        }.chain(_.appendPath("studentSchoolHistory")).
          chain { build =>
            studentSchoolHistoryModel.flatMap(_.id).map(_.toString).foreach(build.appendPath(_))
          }.map(_.appendQueryParameter(Version.version, studentSchoolHistoryModel.flatMap(_.version).map(_.toString).get)).
          map(_.build()).
          foreach(uri => asyncQueryHandler.startDelete(Random.nextInt(), null, uri, null, null))
        true
      case _ => super.onOptionsItemSelected(item)
    }
  }

  override def onSaveInstanceState(outState: Bundle): Unit = {
    super.onSaveInstanceState(outState)
    studentSchoolHistoryModel.foreach(outState.putSerializable(StudentSchoolHistoryFormFragment.studentSchoolHistoryModel, _))
    // 아래와 같이 각각 저장하는 것이 나을까. 아니면 studentSchoolHistoryModel 을 바꿔가면서 저장하는 것이 나을까.
    currentSelectedSchoolId.foreach(outState.putInt(StudentSchoolHistoryFormFragment.currentSelectedSchoolId, _))
    currentSelectedGrade.foreach(outState.putInt(StudentSchoolHistoryFormFragment.currentSelectedGrade, _))
  }

  private class SpinnerSchoolListener extends OnItemSelectedListener {
    override def onNothingSelected(parent: AdapterView[_]): Unit = {

    }

    override def onItemSelected(parent: AdapterView[_], view: View, position: Int, id: Long): Unit = {
      // 여기서 하는 일은, 학교를 선택했을 때, 학교유형에 따라 학년의 선택목록을 바꾸는 것이다.
      val cursor = schoolSimpleCursorAdapter.getCursor
      cursor.moveToPosition(position)
      val schoolModel = Option(cursor.getColumnIndex(SchoolProvider.raw)).
        flatMap(json => Option(cursor.getString(json))).
        map(SampleObjectMapper.readValue[SchoolModel](_)) // 학교목록에서 선택한 학교의 자료를 먼저 구한뒤
      val gradeList = schoolModel.flatMap(_.schoolType).toList.
        flatMap { schoolType => // 선택한 학교유형에 따라 학년목록을 구하고
          schoolType match {
            case SchoolType.elementarySchool => SchoolType.elementarySchoolGradeRange
            case SchoolType.middleSchool => SchoolType.middleSchoolGradeRange
            case SchoolType.highSchool => SchoolType.highSchoolGradeRange
            case _ => Nil
          }
        }
      Some(new MatrixCursor(Array("_id", StudentSchoolHistoryFormFragment.grade))).chain { cursor =>
        cursor.newRow().add(0).add(getResources.getString(R.string.textUnknown))
      }.chain { cursor =>
        gradeList.foreach(gradeNumber => cursor.newRow().add(gradeNumber).add(gradeNumber)) // cursor 에 넣는다.
      }.foreach { cursor =>
        currentGradeSimpleCursorAdapter.changeCursor(cursor)
      }
      currentSelectedSchoolId = schoolModel.flatMap(_.id)
      // 단순하게, 이전에 저장했던 학년정보가 있으면 그대로 쓰는데, 만약 이번에 그려지는 학년 목록에, 앞서 선택했던 학년이 없으면 앞의 선택을 무시한다.
      // 예를 들어, 앞에서 초등학교 6학년을 선택했는데, 뒤에서 중학교를 골랐다면, 자동으로 앞의 선택이 없어지도록 하는 것이다.
      currentSelectedGrade = currentSelectedGrade. // 따라서, 앞의 학년선택과 같거나 선택이 풀리거나 둘중 하나다.
        filter(gradeList.contains(_)).
        orElse(Option(0)). // 아니면 선택을 하지 않은 것으로 본다.
        chain(currentGrade => spinnerCurrentGrade.chain(_.setSelection(currentGrade))).
        filter(_ != 0) // position 이 0 이면 선택이 없다는 의미이다. 0 으로 저장할것이 아니라 None 으로 저장해야 scala 의미에 맞다.
    }
  }

  private class SpinnerGradeListener extends OnItemSelectedListener {
    override def onItemSelected(parent: AdapterView[_], view: View, position: Int, id: Long): Unit = {
      val cursor = currentGradeSimpleCursorAdapter.getCursor
      cursor.moveToPosition(position)
      currentSelectedGrade = Option(position).filter(_ > 0). // 가정을 하고 있다. cursor 안의 자료의 위치가 자료의 의미와 같다.
        chain(cursor.moveToPosition(_)).
        map(_ => cursor.getInt(cursor.getColumnIndex(StudentSchoolHistoryFormFragment.grade)))
    }

    override def onNothingSelected(parent: AdapterView[_]): Unit = {}
  }

  override def onCreateView(inflater: LayoutInflater, container: ViewGroup, savedInstanceState: Bundle): View = {
    super.onCreateView(inflater, container, savedInstanceState)
    Option(inflater.inflate(R.layout.student_school_history_form_fragment_layout, container, false)).chain { view =>
      spinnerSchool = Option(view.findViewById(R.id.spinnerSchool)).map(_.asInstanceOf[Spinner]).chain { spinner =>
        spinner.setAdapter(schoolSimpleCursorAdapter)
        spinner.setOnItemSelectedListener(new SpinnerSchoolListener)
      }
      spinnerCurrentGrade = Option(view.findViewById(R.id.spinnerCurrentGrade)).map(_.asInstanceOf[Spinner]).
        chain { spinner =>
          spinner.setAdapter(currentGradeSimpleCursorAdapter)
          spinner.setOnItemSelectedListener(new SpinnerGradeListener)
        }
      editTextCurrentDate = Option(view.findViewById(R.id.editTextCurrentDate)).map(_.asInstanceOf[EditText]).
        chain { editText =>
          studentSchoolHistoryModel.flatMap(_.currentDate).
            map(dateFormat.format(_)).
            foreach(editText.setText(_))
          editText.setOnClickListener(currentDateClickListener(_: View))
        }
      Option(view.findViewById(R.id.buttonCurrentDate)).
        map(_.asInstanceOf[ImageButton]).
        foreach(_.setOnClickListener((_: View) => editTextCurrentDate.foreach(_.setText(""))))
      editTextStudentSchoolHistoryId = Option(view.findViewById(R.id.editTextStudentSchoolHistoryId)).map(_.asInstanceOf[EditText]).
        chain { editText =>
          studentSchoolHistoryModel.flatMap(_.id).map(_.toString).foreach(editText.setText(_))
        }
      editTextStudentName = Option(view.findViewById(R.id.editTextStudentName)).map(_.asInstanceOf[EditText]).
        chain { editText =>
          studentSchoolHistoryModel.flatMap(_.student).flatMap(_.name).foreach(editText.setText(_))
        }
      editTextClassNumber = Option(view.findViewById(R.id.editTextClassNumber)).map(_.asInstanceOf[EditText]).
        chain { editText =>
          studentSchoolHistoryModel.flatMap(_.classNumber).map(_.toString).foreach(editText.setText(_))
        }
      toggleCurrentStudentSchoolHistory = Option(view.findViewById(R.id.toggleCurrentStudentSchoolHistory)).
        map(_.asInstanceOf[ToggleButton]).
        chain { toggle => studentSchoolHistoryModel.flatMap(_.currentStudentSchoolHistory).orElse(Some(false)).foreach(toggle.setChecked(_)) }
    }.get
  }

  override def onResume(): Unit = {
    super.onResume()
    getLoaderManager.restartLoader(0, null, schoolLoaderCallbacks)
  }

  private def currentDateClickListener(v: View): Unit = {
    studentSchoolHistoryModel.flatMap(_.currentDate).foreach(calendar.setTime(_))
    val dialog = new DatePickerDialog(getActivity, new SampleDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
    dialog.show()
  }

  private class SampleDateSetListener extends OnDateSetListener {
    override def onDateSet(view: DatePicker, year: Int, monthOfYear: Int, dayOfMonth: Int): Unit = {
      calendar.set(Calendar.YEAR, year)
      calendar.set(Calendar.MONTH, monthOfYear)
      calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
      editTextCurrentDate.foreach(_.setText(dateFormat.format(calendar.getTime)))
    }
  }

  private class SampleSimpleCursorAdapter extends SimpleCursorAdapter(getActivity, android.R.layout.simple_spinner_item, null, Array(), Array(), 0) {
    override def getView(position: Int, convertView: View, parent: ViewGroup): View = {
      val view = super.getView(position, convertView, parent)
      getCursor.moveToPosition(position)
      val schoolModel = Option(getCursor.getColumnIndex(SchoolProvider.raw)).flatMap(json => Option(getCursor.getString(json))).map(SampleObjectMapper.readValue[SchoolModel](_))
      val unknown = Option(getResources.getString(R.string.textUnknown))
      Option(view.findViewById(android.R.id.text1)).map(_.asInstanceOf[TextView]).
        foreach(view => schoolModel.flatMap(_.name).orElse(unknown).foreach(view.setText(_)))
      //      Option(view.findViewById(android.R.id.text2)).map(_.asInstanceOf[TextView]).
      //        foreach(view => schoolModel.flatMap(_.schoolType).flatMap(SchoolProvider.schoolTypeText.get(_)).map(getResources.getString(_)).orElse(unknown).foreach(view.setText(_)))
      view
    }

    override def getDropDownView(position: Int, convertView: View, parent: ViewGroup): View = {
      val view = super.getDropDownView(position, convertView, parent)
      getCursor.moveToPosition(position)
      val schoolModel = Option(getCursor.getColumnIndex(SchoolProvider.raw)).flatMap(json => Option(getCursor.getString(json))).map(SampleObjectMapper.readValue[SchoolModel](_))
      val unknown = Option(getResources.getString(R.string.textUnknown))
      Option(view.findViewById(android.R.id.text1)).map(_.asInstanceOf[TextView]).
        foreach(view => schoolModel.flatMap(_.name).orElse(unknown).foreach(view.setText(_)))
      //      Option(view.findViewById(android.R.id.text2)).map(_.asInstanceOf[TextView]).
      //        foreach(view => schoolModel.flatMap(_.schoolType).flatMap(SchoolProvider.schoolTypeText.get(_)).map(getResources.getString(_)).orElse(unknown).foreach(view.setText(_)))
      view
    }
  }

  private class SchoolLoaderCallbacks extends LoaderCallbacks[Cursor] {
    override def onCreateLoader(id: Int, args: Bundle): Loader[Cursor] = {
      // todo 학교유형도 선택할수 있도록 하면 어떨까. 일단은 모든 학교 목록이 전부 나오도록 했다. 따라서 학교목록을 정렬할 때 유형도 정렬조건에 들어가야 한다.
      new CursorLoader(getActivity, SchoolProvider.uri, null, null, null, null)
    }

    override def onLoaderReset(loader: Loader[Cursor]): Unit = {}

    override def onLoadFinished(loader: Loader[Cursor], data: Cursor): Unit = {
      spinnerSchool.map(_.getSelectedItemPosition).map(_.toString).foreach(s => Log.i(getClass.getName, s))
      Some(new MatrixCursor(Array(SchoolProvider.schoolId, SchoolProvider.raw))).
        chain(_.newRow().add(-1).add(null)). // raw 에 null 을 전달해도 된다. Option(getCursor.getString) 은 None 이 되고 unknown 으로 바뀌기 때문이다.
        map(cursor => new MergeCursor(Array(cursor, data))). // database 에서 구한 cursor 와 아무 선택도 하지않음을 뜻하는 행을 갖는 cursor 를 섞었다. spinner 는 값의 부재를 다루지 않는다.
        chain(schoolSimpleCursorAdapter.changeCursor(_)).
        foreach { cursor =>
          cursor.moveToFirst()
          // scala 에는 while 문은 있어도 break/continue 는 없다. scala.util.control.Breaks 가 있긴 한데, 일단 이렇게 썼다.
          var loopFlag = cursor.isAfterLast == false
          while (loopFlag) {
            if (Option(cursor.getInt(cursor.getColumnIndex(SchoolProvider.schoolId))) == currentSelectedSchoolId) {
              spinnerSchool.chain(_.setSelection(cursor.getPosition))
              loopFlag = false
            }
            cursor.moveToNext()
            loopFlag = cursor.isAfterLast == false
          }
        }
    }
  }

}

object StudentSchoolHistoryFormFragment {
  val TAG = classOf[StudentSchoolHistoryFormFragment].getName
  val studentSchoolHistoryModel = "studentSchoolHistoryModel"
  val grade = "grade"
  val currentSelectedSchoolId = "currentSelectedSchoolId"
  val currentSelectedGrade = "currentSelectedGrade"
}
