/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.client20150316

import java.util.logging.Logger

import android.test.ActivityInstrumentationTestCase2
import android.util.Log
import android.widget.Button

/**
 * Created by i on 3/23/15.
 */
class TestMainActivity extends ActivityInstrumentationTestCase2[MainActivity](classOf[MainActivity]) {
  var mainActivity: MainActivity = null

  override def setUp(): Unit = {
    super.setUp()
    mainActivity = getActivity
  }

  override def tearDown(): Unit = {
    mainActivity.finish()
    super.tearDown()
  }

  def testHi(): Unit = {
    val logger = Logger.getLogger(getClass.getName)
    val fragment = mainActivity.getFragmentManager.findFragmentByTag(MainFragment.TAG)
    val buttonStudentList = fragment.getView.findViewById(R.id.buttonStudentList).asInstanceOf[Button]
    Log.i(getClass.getName, fragment.getActivity.getResources.getString(R.string.titleStudentListButton))
    Log.i(getClass.getName, buttonStudentList.getText.toString)
    // 일단은 잘된다. 뜻도 모르겠고 이유도 알수 없는 예외가 test 끝에 생길때도 있고 아닐때도 있다. 각각의 test 에는 영향을 주지 않는데, 가끔씩 전체로서의 test 가 완결되지 못하도록 방해하는 뭔가가 있는것 같다.
    // 한가지 문제는, scala 로 쓴 android program 을 coverage 확인하는 방법을 모르겠다.
  }
}
