/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.webservice

import java.util.{ Calendar, Locale, TimeZone }

import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._
import org.json4s.{ DefaultFormats, Formats }
import org.scalatra._
import org.scalatra.json._

import scala.util.{ Failure, Success, Try }

/**
 * Created by i on 3/19/15.
 */
abstract class AbstractServlet extends ScalatraServlet with JacksonJsonSupport with SampleAuthSupport {
  override protected implicit def jsonFormats: Formats = DefaultFormats.lossless // 시분초까지 다룰 필요는 없는 듯하다.

  def predicateAccept: Boolean = Option(request.getHeader("Accept")).map(_.split(";").contains("application/json")).getOrElse(false)

  def predicateContentType: Boolean = Option(request.getHeader("Content-Type")).map(_.split(";").contains("application/json")).getOrElse(false)

  protected def getAll[RetType](dbWork: Session => List[RetType])(implicit m: Manifest[RetType]): ActionResult = {
    contentType = formats("json")
    Try {
      DriverSettings.database.withTransaction { implicit s => dbWork(s) }
    } match {
      case Success(x) => Ok(x)
      case Failure(x) => InternalServerError(x)
    }
  }

  protected def postEach[RetType](dbWork: Session => RetType)(implicit m: Manifest[RetType]): ActionResult = {
    contentType = formats("json")
    Try {
      DriverSettings.database.withTransaction { implicit s => dbWork(s) }
    } match {
      case Success(x) => Ok(x)
      case Failure(x) => InternalServerError(x)
    }
  }

  protected def putEach[RetType](dbWork: Session => Int)(implicit m: Manifest[RetType]): ActionResult = {
    contentType = formats("json")

    Try {
      DriverSettings.database.withTransaction { implicit s =>
        val result = dbWork(s)
        if (result != 1) {
          s.rollback()
        }
        result
      }
    } match {
      case Success(x) if x == 1 => Ok()
      case Success(x) if x != 1 => NotFound()
      case Failure(x) => InternalServerError(x)
    }
  }

  protected def getEach[RetType](dbWork: Session => Option[RetType])(implicit m: Manifest[RetType]): ActionResult = {
    contentType = formats("json")
    Try {
      DriverSettings.database.withTransaction { implicit s => dbWork(s) }
    } match {
      case Success(Some(x)) => Ok(x)
      case Success(None) => NotFound()
      case Failure(x) => InternalServerError(x)
    }
  }

  protected def deleteEach[RetType](f2: Session => Int)(implicit m: Manifest[RetType]): ActionResult = {
    contentType = formats("json")
    Try {
      DriverSettings.database.withTransaction { implicit s =>
        val result = f2(s)
        if (result != 1) {
          s.rollback()
        }
        result
      }
    } match {
      case Success(x) if x == 1 => Ok(x)
      case Success(x) if x != 1 => NotFound()
      case Failure(x) => InternalServerError(x)
    }
  }

  protected def initCalendar(): Calendar = {
    Calendar.getInstance(TimeZone.getTimeZone("Asia/Seoul"), Locale.getDefault)
  }
}
