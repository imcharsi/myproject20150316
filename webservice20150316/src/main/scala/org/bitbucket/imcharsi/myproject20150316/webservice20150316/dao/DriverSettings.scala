/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao

import java.util.Properties

import com.jolbox.bonecp.BoneCPDataSource
import net.sf.log4jdbc.DriverSpy

import scala.collection.JavaConversions
import scala.slick.driver.PostgresDriver
import scala.util.Try

/**
 * Created by i on 3/19/15.
 */
object DriverSettings {
  val driverSimple = PostgresDriver.simple

  val driver = PostgresDriver

  import driverSimple._

  // java.sql.Date 를 사용하니깐 ObjectMapper 의 setDateFormat 이 효과가 없다. java.util.Date 를 써야 한다.
  // 또한 순서가 중요하다. 아래 구절이 여기에 없으면, 그 아래의 .schema 부터 제대로 동작하지 않는다.
  implicit val dateMapper = MappedColumnType.base[java.util.Date, java.sql.Date](d => new java.sql.Date(d.getTime), d => new java.util.Date(d.getTime))

  val schemaList: List[driver.SchemaDescription] = List(StudentTable.schema, StudentContactTable.schema, SchoolTable.schema, StudentSchoolHistoryTable.schema, StudentDuesTable.schema, LoginUserTable.schema)
  private val dataSource = new BoneCPDataSource()

  private val inputStream = getClass.getResourceAsStream("/org/bitbucket/imcharsi/myproject20150316/webservice20150316/webservice/database.properties")
  private val properties = new Properties
  properties.load(inputStream)
  private val propertiesMap = JavaConversions.mapAsScalaMap(properties)
  dataSource.setDriverClass(classOf[DriverSpy].getName)
  dataSource.setJdbcUrl("jdbc:log4jdbc:postgresql://localhost/sample")
  propertiesMap.get("databaseUsername").map(_.asInstanceOf[String]).foreach(dataSource.setUsername(_))
  propertiesMap.get("databasePassword").map(_.asInstanceOf[String]).foreach(dataSource.setPassword(_))
  val database = Database.forDataSource(dataSource)

  def createTables(schemaList: List[driver.SchemaDescription]): Unit = {
    // 순서가 바뀌었다. database 종류마다 다른데, postgresql 에서는 한번 오류가 생긴 transaction 은 무조건 실패한다.
    // 반면, mysql 에서는 transaction 안에서 실패한 문장은 무시된다.
    // student table 과 student school history table 이 서로에 대해 외래키 참조를 한다. 이 경우, table 정의와 외래키 지정을 나눠서 해야 하는데,
    // slick 자체에는 이에 대한 지원이 없고, slick 이 만들어준 ddl 중에서 create table 과 alter table 을 직접 골라서 따로 실행해야 한다.
    schemaList.foreach { schema =>
      Try {
        DriverSettings.database.withTransaction { implicit s =>
          schema.createStatements.filterNot(_.toLowerCase.startsWith("alter table")).foreach { ddl =>
            s.withPreparedStatement(ddl)(_.execute)
          }
        }
      }
    }
    schemaList.foreach { schema =>
      Try {
        DriverSettings.database.withTransaction { implicit s =>
          schema.createStatements.filter(_.toLowerCase.startsWith("alter table")).foreach { ddl =>
            s.withPreparedStatement(ddl)(_.execute)
          }
        }
      }
    }
  }

  def dropTables(schemaList: List[driver.SchemaDescription]): Unit = {
    schemaList.reverse.foreach { schema =>
      Try {
        DriverSettings.database.withTransaction { implicit s =>
          schema.dropStatements.filter(_.toLowerCase.startsWith("alter table")).foreach { ddl =>
            s.withPreparedStatement(ddl)(_.execute())
          }
        }
      }
    }
    schemaList.reverse.foreach { schema =>
      Try {
        DriverSettings.database.withTransaction { implicit s =>
          schema.dropStatements.filterNot(_.toLowerCase.startsWith("alter table")).foreach { ddl =>
            s.withPreparedStatement(ddl)(_.execute())
          }
        }
      }
    }
  }
}
