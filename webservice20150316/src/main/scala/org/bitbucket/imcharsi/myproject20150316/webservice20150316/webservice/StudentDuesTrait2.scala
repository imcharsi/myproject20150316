/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.webservice

import java.util.Calendar

import org.bitbucket.imcharsi.myproject20150316.model20150316.{ StudentDuesModel, StudentDuesSummaryModel }
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.{ DriverSettings, StudentDuesTable, StudentTable }
import org.scalatra.{ ActionResult, InternalServerError, Ok }

import scala.util.{ Failure, Success, Try }

/**
 * Created by i on 3/31/15.
 */
trait StudentDuesTrait2 extends AbstractServlet {
  implicit private val dateMapper = org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.dateMapper

  import StudentDuesTrait2._

  private def predicateTabParam(t: StudentDuesTable) = {
    params.get(StudentDuesTrait2.tab) match {
      case Some(x) if x == StudentDuesTrait2.tabAll => t.id.isDefined // 항상 참이다.
      case Some(x) if x == StudentDuesTrait2.tabAlreadyPaid => t.actuallyPayDate.isDefined
      case Some(x) if x == StudentDuesTrait2.tabUnpaid => t.actuallyPayDate.isEmpty
      case None => t.id.isDefined // 인자가 아예 없으면 다 보여준다. 예상되는 인자 외에 다른 것이 주어지면 오류를 보인다.
    }
  }

  private def innerGetAll(implicit session: Session): List[StudentDuesModel] = {
    val calendar = initCalendar()
    val paramYear = params.get(StudentDuesTrait2.year).map(_.toInt)
    val paramMonth = params.get(StudentDuesTrait2.month).map(_.toInt)
    paramYear.foreach(calendar.set(Calendar.YEAR, _))
    paramMonth.foreach(month => calendar.set(Calendar.MONTH, month - 1))
    calendar.set(Calendar.DAY_OF_MONTH, 1) // 기준연월은 연과 월만 쓰지만 database 에 저장할 때는 연월일이 모두 저장되는데, 일은 항상 1일이 되도록 하자. 일단, slick 의 현재 구현에서는 검사 제약조건을 정할 수 없다.
    calendar.set(Calendar.HOUR_OF_DAY, 0) // 시분초는 직접 0으로 써줘야 한다.
    calendar.set(Calendar.MINUTE, 0)
    calendar.set(Calendar.SECOND, 0)
    calendar.set(Calendar.MILLISECOND, 0)
    val date = calendar.getTime

    val result = StudentDuesTable.filter(predicateTabParam(_)).
      joinLeft(StudentTable).on(_.studentId === _.id).
      filter { case (a, _) => a.axisPayDate === date }.
      sortBy { case (a, b) => (a.plannedPayDate.desc) }. // 일단은, 예정납부일 기준으로 정렬하자.
      list.
      map { case (a, b) => a.copy(student = b) }
    result
  }

  private def innerPostAll(): ActionResult = {
    implicit val slickSession = DriverSettings.database.createSession()
    val result = Try {
      slickSession.withTransaction {
        val paramYear = params.get("year").map(_.toInt)
        val paramMonth = params.get("month").map(_.toInt)
        val calendar = initCalendar()
        paramYear.foreach(calendar.set(Calendar.YEAR, _))
        paramMonth.foreach(month => calendar.set(Calendar.MONTH, month - 1))
        calendar.set(Calendar.DAY_OF_MONTH, 1) // 기준연월은 연과 월만 쓰지만 database 에 저장할 때는 연월일이 모두 저장되는데, 일은 항상 1일이 되도록 하자. 일단, slick 의 현재 구현에서는 검사 제약조건을 정할 수 없다.
        calendar.set(Calendar.HOUR_OF_DAY, 0) // 시분초는 직접 0으로 써줘야 한다.
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        val date = calendar.getTime

        val dataList = StudentTable.filter(_.currentAttendingStudent === Option(true)).list. // 우선, 학생기본정보에 따라, 현재 학원을 다니고 있는 것으로 표시되어 있는 학생만 골라 낸다.
          map { studentModel =>
            val plannedPayDate = studentModel.distanceOfPlannedDuesDate.map { distance => // 기준연월의 1일에, 학생기본정보에 표시되어 있는 기간차이를 더해서 납부예정일을 계산한다.
              calendar.setTime(date)
              calendar.add(Calendar.DAY_OF_YEAR, distance)
              calendar.getTime
            }
            StudentDuesModel(None, Some(1), studentModel.id, Some(date), plannedPayDate, None, studentModel.currentDuesAmount, None) // 새로 추가할 납부이력정보를 만든다.
          }
        if (dataList.map(studentDuesModel => StudentDuesTable.returning(StudentDuesTable.map(_.id)).insert(studentDuesModel)).filter(_.isDefined).length != dataList.length)
          throw new RuntimeException // 예외를 던져야, rollback 이 된다.
        dataList.length
      }
    }
    slickSession.close()
    result match {
      case Success(length) => Ok(length)
      case Failure(x) => InternalServerError(x)
    }
  }

  private def innerGetAllUnpaid(implicit session: Session): List[StudentDuesModel] = {
    StudentDuesTable.
      joinLeft(StudentTable).on(_.studentId === _.id).
      sortBy { case (a, b) => (a.axisPayDate.asc, a.plannedPayDate.asc) }. // 우선, 기준연월 기준으로 가장 오래된 미납이력을 보이고 같은 기준연월이면, 가장 오래된 예정납부일 순서로 보인다.
      list.
      map { case (a, b) => a.copy(student = b) }
  }

  private def innerSummary(): ActionResult = {
    val calendar = initCalendar()
    val paramYear = params.get(StudentDuesTrait2.year).map(_.toInt)
    val paramMonth = params.get(StudentDuesTrait2.month).map(_.toInt)
    paramYear.foreach(calendar.set(Calendar.YEAR, _))
    paramMonth.foreach(month => calendar.set(Calendar.MONTH, month - 1))
    calendar.set(Calendar.DAY_OF_MONTH, 1)
    calendar.set(Calendar.HOUR_OF_DAY, 0)
    calendar.set(Calendar.MINUTE, 0)
    calendar.set(Calendar.SECOND, 0)
    calendar.set(Calendar.MILLISECOND, 0)
    val date = calendar.getTime

    val result = Try {
      DriverSettings.database.withTransaction { implicit slickSession =>
        Query(StudentDuesTable.filter(t => predicateTabParam(t) && t.axisPayDate === date).map(_.amount).sum,
          StudentDuesTable.filter(t => predicateTabParam(t) && t.axisPayDate === date && t.actuallyPayDate.isDefined).map(_.amount).sum,
          StudentDuesTable.filter(t => predicateTabParam(t) && t.axisPayDate === date && t.actuallyPayDate.isEmpty).map(_.amount).sum).firstOption.
          map { case (a, b, c) => StudentDuesSummaryModel(a, b, c) }.
          getOrElse(StudentDuesSummaryModel(None, None, None))
      }
    }
    result match {
      case Success(x) => Ok(x)
      case Failure(x) => InternalServerError(x)
    }
  }

  // 있는 기능을 이중으로 반복할 필요는 없다. 기준연월의 납부이력목록을 알기 위해 이 url 을 사용하면 되고,
  // 각각의 이력을 열람하거나 편집할때는 /student/:studentId/studentDues/:studentDuesId 를 사용하기로 하자.
  get(f"/:${year}/:${month}", predicateAccept)(getAll(innerGetAll(_)))
  get(f"/${allUnpaid}", predicateAccept)(getAll(innerGetAllUnpaid(_)))
  post(f"/:${year}/:${month}")(innerPostAll) // 그런데, 이 url 은 악의적인 목적으로 반복해서 요청할수 있다.
  get(f"/:${year}/:${month}/${summary}", predicateAccept)(innerSummary)
}

object StudentDuesTrait2 {
  val studentDues = "studentDues"
  val year = "year"
  val month = "month"
  val tab = "tab"
  val tabAlreadyPaid = "alreadyPaid"
  val tabUnpaid = "unpaid"
  val tabAll = "all"
  val allUnpaid = "allUnpaid"
  val summary = "summary"
}
