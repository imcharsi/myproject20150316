/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.webservice

import org.bitbucket.imcharsi.myproject20150316.model20150316._
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao._

/**
 * Created by i on 3/20/15.
 */
trait StudentTrait extends AbstractServlet {

  import StudentTrait._

  get("/", predicateAccept)(getAll(innerGetAll(_)))
  post("/", predicateAccept && predicateContentType)(postEach(innerPostEach(_)))
  put(f"/:${studentId}", predicateContentType)(putEach(innerPutEach(_)))
  get(f"/:${studentId}", predicateAccept)(getEach(innerGetEach(_)))
  delete(f"/:${studentId}")(deleteEach(innerDeleteEach(_)))

  private def queryGetAll(studentName: Rep[Option[String]], schoolName: Rep[Option[String]], currentAttendingStudent: Rep[Option[Boolean]], schoolId: Rep[Option[Int]]) = {
    val studentJoin = StudentTable.filter(t =>
      Case.If(currentAttendingStudent.isDefined && t.currentAttendingStudent === currentAttendingStudent).Then(true).
        If(currentAttendingStudent.isDefined && t.currentAttendingStudent =!= currentAttendingStudent).Then(false).
        Else(true))
    val schoolHistoryJoin = StudentSchoolHistoryTable.
      joinLeft(SchoolTable.filter(_.name.like(schoolName))).on(_.schoolId === _.id)
    val remainingDuesAmountJoin = StudentDuesTable.filter(_.actuallyPayDate.isEmpty).
      groupBy(_.studentId).
      map { case (studentId, table) => (studentId, table.map(_.amount).sum) }
    studentJoin.
      joinLeft(StudentContactTable).on { case (a, b) => a.id === b.studentId && b.validity === true && b.id === a.currentStudentContactId }.
      joinLeft(schoolHistoryJoin).on { case ((a, c), (b, _)) => a.id === b.studentId && b.id === a.currentStudentSchoolHistoryId }.
      joinLeft(remainingDuesAmountJoin).on { case (((a, _), _), (studentId, _)) => a.id === studentId }.
      filter { case (((a, _), b), _) => (a.name.like(studentName) || b.flatMap(_._2).flatMap(_.name).like(schoolName)) && Case.If(schoolId.isDefined && b.flatMap(_._2).flatMap(_.id) === schoolId).Then(true).If(schoolId.isEmpty).Then(true).Else(false) }.
      sortBy {
        case (((studentTable, studentContactTable), schoolHistoryJoin), unpaidDuesJoin) =>
          (schoolHistoryJoin.flatMap { case (_, a) => a }.map(_.schoolType).asc, studentTable.name.asc)
      }
  }

  private def queryGetEach(studentId: Rep[Option[Int]]) = {
    val studentJoin = StudentTable.filter(_.id === studentId)
    val schoolHistoryJoin = StudentSchoolHistoryTable.
      joinLeft(SchoolTable).on(_.schoolId === _.id)
    val remainingDuesAmountJoin = StudentDuesTable.filter(_.actuallyPayDate.isEmpty).
      groupBy(_.studentId).
      map { case (studentId, table) => (studentId, table.map(_.amount).sum) }
    studentJoin.
      joinLeft(StudentContactTable).on { case (a, b) => a.id === b.studentId && b.validity === true && b.id === a.currentStudentContactId }.
      joinLeft(schoolHistoryJoin).on { case ((a, c), (b, _)) => a.id === b.studentId && b.id === a.currentStudentSchoolHistoryId }.
      joinLeft(remainingDuesAmountJoin).on { case (((a, _), _), (studentId, _)) => a.id === studentId }.
      sortBy {
        case (((studentTable, studentContactTable), schoolHistoryJoin), unpaidDuesJoin) =>
          (schoolHistoryJoin.flatMap { case (_, a) => a }.map(_.schoolType).asc, studentTable.name.asc)
      }
  }

  private def getMapper(x: (((StudentModel, Option[StudentContactModel]), Option[(StudentSchoolHistoryModel, Option[SchoolModel])]), Option[(Option[Int], Option[Int])])): StudentModel = {
    val (((studentResult, studentContactResult), studentSchoolHistoryResult), remainingDuesAmountResult) = x
    // 아래와 같이 할 경우, 순환참조가 없기 때문에, json 변환단계에서 순환참조로 인한 무한반복문제는 없다.
    // studentContact 와 studentSchoolHistory 에서 사용하는 student 기록과, 최종적으로 반환되는 student 기록은 서로 다른 instance 이기 때문이다.
    // 그런데 순환참조를 할 경우 무한반복을 하는가? 하다보니 이렇게 됐는데, 앞으로도 이와 유사한 상황에서는 이와 같이 순환참조를 피하는 방향으로 하기로 하자.
    val studentContact = studentContactResult.map(_.copy(student = Option(studentResult)))
    val studentSchoolHistory = studentSchoolHistoryResult.map { case (history, school) => history.copy(student = Option(studentResult), school = school) }
    val remainingDuesAmount = remainingDuesAmountResult.flatMap(_._2)
    studentResult.copy(currentStudentContact = studentContact,
      currentStudentSchoolHistory = studentSchoolHistory,
      remainingDuesAmount = remainingDuesAmount)
  }

  private val compiledQueryGetAll = Compiled(queryGetAll _)
  private val compiledQueryGetEach = Compiled(queryGetEach _)

  private def innerGetAll(implicit s: Session): List[StudentModel] = {
    params.get(paramTab) match {
      case Some(x) if x == paramTabContact =>
      case Some(x) if x == paramTabSchoolHistory =>
      case Some(x) if x == paramTabDues =>
      case _ => // 일단은, 조건검사를 하지 않고 무조건 통과시키도록 하자.
      //      case _ => throw new IllegalArgumentException // 이와 같이 오류이어야 하는 경우에 오류가 되도록 하고, 그 외의 경우에 대해서는 똑같이 계산하는 것이 차라리 편하다.
      // 각 경우에 대해 계산을 다르게 해서 얻는 이익이 크지 않다.
    }
    val search = params.get(paramSearch).map(_.replaceAll("%", "\\%")).map(x => f"%%${x}%%").orElse(Option("%"))
    val schoolId = params.get(paramSchoolId).filterNot(_.isEmpty).map(_.toInt)

    compiledQueryGetAll(search, search, params.get(paramCurrentAttendingStudent).map(_.toBoolean), schoolId).list.map(getMapper)
  }

  private def retrieveModel: Option[StudentModel] = {
    parsedBody.extractOpt[StudentModel]
  }

  private def retrieveIdList: List[Option[Int]] = {
    List(params.get(studentId).map(_.toInt))
  }

  // StudentTrait 뿐만 아니라 모든 post 의 반환값은, getEach 와 같도록 하자.
  private def innerPostEach(implicit s: Session): StudentModel = {
    // post 로 전달되는 인자에 대해서는 version 을 무시한다.
    val model: StudentModel = retrieveModel.filter(_.id.isEmpty).map(_.copy(version = Option(1))).get
    val id = StudentTable.returning(StudentTable.map(_.id)).insert(model)
    compiledQueryGetEach(id).firstOption.map(getMapper).get
  }

  private def innerPutEach(implicit s: Session): Int = {
    retrieveModel.
      filter(_.id == retrieveIdList.head).
      map { d =>
        // put 으로 전달받은 인자의 version 에 대한 추가적인 검증을 할 필요가 없다.
        // 왜냐하면, version 을 조작했을 때, 조작된 version 에 일치하는 행을 찾을수 없을 것이기 때문이다.
        StudentTable.filter(t => t.id === d.id && TableVersion.predicate(t, d)).update(d.modifyVersion())
      }.get
  }

  private def innerGetEach(implicit s: Session): Option[StudentModel] = {
    compiledQueryGetEach(retrieveIdList.head).firstOption.map(getMapper)
  }

  private def innerDeleteEach(implicit s: Session): Int = {
    // delete 에서 "version" 인자를 추가로 받는다.
    val result = StudentTable.filter(t => t.id === retrieveIdList.head && TableVersion.predicate2(t, params.get(Version.version))).delete
    if (result != 1) {
      s.rollback()
    }
    result
  }
}

object StudentTrait {
  val studentId = "studentId"
  val paramTab = "tab"
  val paramTabContact = "contact"
  val paramTabSchoolHistory = "schoolHistory"
  val paramTabDues = "dues"
  val paramSearch = "search"
  val paramSchoolId = "schoolId"
  val paramCurrentAttendingStudent = "currentAttendingStudent"
}
