/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.webservice

import org.bitbucket.imcharsi.myproject20150316.model20150316.{ LoginUserModel, Version }
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.{ LoginUserTable, TableVersion }

/**
 * Created by i on 4/7/15.
 */
class LoginUserServlet extends AbstractServlet with SampleAuthSupport {

  import LoginUserServlet._

  get("/")(getAll(innerGetAll(_)))
  get(f"/:${loginUserId}")(getEach(innerGetEach(_)))
  post("/")(postEach(innerPostEach(_)))
  put(f"/:${loginUserId}")(putEach(innerPutEach(_)))
  delete(f"/:${loginUserId}")(deleteEach(innerDeleteEach(_)))

  private def innerGetAll(implicit slickSession: Session): List[LoginUserModel] = {
    LoginUserTable.sortBy(_.id.asc).list
  }

  private def innerPostEach(implicit slickSession: Session): LoginUserModel = {
    parsedBody.extractOpt[LoginUserModel].
      map(_.copy(version = Option(1))).
      filter(_.id.isEmpty).
      filter(_.checkUsername).
      filter(_.checkPassword).
      map { model =>
        model.copy(id = LoginUserTable.returning(LoginUserTable.map(_.id)).insert(model))
      }.get
  }

  private def innerPutEach(implicit slickSession: Session): Int = {
    parsedBody.extractOpt[LoginUserModel].
      filter(_.id == params.get(loginUserId).map(_.toInt)).
      filter(_.checkUsername).
      filter(_.checkPassword).
      map { model =>
        // 자기가 자기의 계정을 비사용으로 할수 없다. 서로가 서로를 비사용으로 하는 것은 되나.
        if (model.id == user.id && model.available.forall(_ == false))
          throw new IllegalArgumentException
        LoginUserTable.filter(t => t.id === model.id && TableVersion.predicate(t, model)).update(model.modifyVersion())
      }.get
  }

  private def innerDeleteEach(implicit slickSession: Session): Int = {
    params.get(loginUserId).map(_.toInt).map { id =>
      // 자기가 자기를 지우는 것은 안된다. 그럼 서로가 서로를 지우는 것은 되나.
      if (user.id.filter(_ == id).isDefined)
        throw new IllegalArgumentException
      LoginUserTable.filter(t => t.id === id && TableVersion.predicate2(t, params.get(Version.version))).delete
    }.get
  }

  // 서로가 서로를 지우거나 비사용으로 하는 것도 가능하지 않다. 왜냐하면, 매 http 요청마다 인증이 이루어지기 때문이다.
  // 어느 한쪽이 비사용으로 바뀌거나 지워지면, 비사용으로 바뀌거나 지워진 그 계정으로부터의 일체의 http 요청은 거절되기 때문이다.

  private def innerGetEach(implicit session: Session): Option[LoginUserModel] = {
    LoginUserTable.filter(_.id === params.get(loginUserId).map(_.toInt)).firstOption
  }
}

object LoginUserServlet {
  val loginUserId = "loginUserId"
  val loginUser = "loginUser"
}
