/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao

import org.bitbucket.imcharsi.myproject20150316.model20150316.Version
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._

/**
 * Created by i on 4/3/15.
 */
trait TableVersion[T] extends Table[T] {
  def version: Rep[Option[Int]]
}

object TableVersion {
  def predicate[M <: Version[M]](t: TableVersion[M], m: M): Rep[Option[Boolean]] = t.version === m.version
  def predicate2[M <: Version[M]](t: TableVersion[M], m: Option[String]): Rep[Option[Boolean]] = t.version === m.map(_.toInt)
  def predicate3[M <: Version[M]](t: TableVersion[M], m: Option[Int]): Rep[Option[Boolean]] = t.version === m
}
