/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.webservice

import org.bitbucket.imcharsi.myproject20150316.model20150316.{ StudentSchoolHistoryModel, Version }
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao._

/**
 * Created by i on 3/25/15.
 */
trait StudentSchoolHistoryTrait extends AbstractServlet {
  implicit private val dateMapper = DriverSettings.dateMapper

  import StudentSchoolHistoryTrait._

  delete(f"/:${studentId}/${studentSchoolHistory}/:${studentSchoolHistoryId}")(deleteEach(innerDeleteEach(_)))

  put(f"/:${studentId}/${studentSchoolHistory}/:${studentSchoolHistoryId}", predicateContentType)(putEach(innerPutEach(_)))

  post(f"/:${studentId}/${studentSchoolHistory}", predicateAccept && predicateContentType)(postEach(innerPostEach(_)))

  get(f"/:${studentId}/${studentSchoolHistory}", predicateAccept)(getAll(innerGetAll(_)))

  get(f"/:${studentId}/${studentSchoolHistory}/:${studentSchoolHistoryId}", predicateAccept)(getEach(innerGetEach(_)))

  private def innerGetAll(implicit session: Session): List[StudentSchoolHistoryModel] = {
    StudentSchoolHistoryTable.
      filter(_.studentId === params.get(studentId).map(_.toInt)).
      joinLeft(StudentTable).
      on { case (a, b) => a.studentId === b.id }.
      joinLeft(SchoolTable).
      on { case ((a, _), b) => a.schoolId === b.id }.
      sortBy { case ((a, _), _) => a.currentDate.desc }.
      list.
      map {
        case ((a, b), c) =>
          a.copy(student = b, school = c, currentStudentSchoolHistory = Option(a.id == b.flatMap(_.currentStudentSchoolHistoryId))) // id 에 대한 null 검사를 할필요가 없는데, id 는 무조건 null 이 아니기 때문이다.
      }
  }

  private def innerPostEach(implicit session: Session): StudentSchoolHistoryModel = {
    parsedBody.extractOpt[StudentSchoolHistoryModel].map(_.copy(version = Some(1))).
      filter(_.id.isEmpty).
      filter(_.studentId == params.get(studentId).map(_.toInt)).
      map { model =>
        val id = StudentSchoolHistoryTable.returning(StudentSchoolHistoryTable.map(_.id)).insert(model)
        model.currentStudentSchoolHistory.filter(_ == true).foreach { _ =>
          StudentTable.filter(_.id === model.studentId).firstOption.
            map(_.copy(currentStudentSchoolHistoryId = id)).
            foreach { model =>
              if (StudentTable.filter(t => t.id === model.id && t.version === model.version).
                update(model.copy(version = model.version.map(_ + 1))) != 1)
                throw new RuntimeException
            }
        }
        id
      }.flatMap { id =>
        StudentSchoolHistoryTable.
          filter(t => t.studentId === params.get(studentId).map(_.toInt) && t.id === id).
          joinLeft(StudentTable).
          on { case (a, b) => a.studentId === b.id }.
          joinLeft(SchoolTable).
          on { case ((a, _), b) => a.schoolId === b.id }.
          firstOption.
          map {
            case ((a, b), c) =>
              a.copy(student = b, school = c, currentStudentSchoolHistory = Option(a.id == b.flatMap(_.currentStudentSchoolHistoryId))) // id 에 대한 null 검사를 할필요가 없는데, id 는 무조건 null 이 아니기 때문이다.
          }
      }.get
  }

  private def innerPutEach(implicit session: Session): Int = {
    parsedBody.extractOpt[StudentSchoolHistoryModel].
      filter(_.id == params.get(studentSchoolHistoryId).map(_.toInt)).
      filter(_.studentId == params.get(studentId).map(_.toInt)).
      map { model =>
        // 우선, 재학이력을 저장하고
        val result = StudentSchoolHistoryTable.filter(t => t.id === model.id && TableVersion.predicate(t, model)).
          update(model.modifyVersion())
        // 학생기본정보의 현재재학이력이 이 재학이력을 가리키거나
        StudentTable.filter(_.id === model.studentId).firstOption.
          // 전달된 인자의 currentSchoolHistory 가 참이면
          filter(_.currentStudentSchoolHistoryId == model.id || model.currentStudentSchoolHistory == Option(true)).
          map { studentModel =>
            // 학생기본정보의 현재재학이력을 다른 것으로 바꾼다.
            // 학생기본정보의 현재재학이력이 이 재학이력을 가리키고 있었는데,
            // 전달된 인자의 currentSchoolHistory 가 참이면 바뀔 현재재학이력은 이 재학이력 그대로이고,
            // 전달된 인자의 currentSchoolHistory 가 거짓이면 아무것도 가리키지 않는다.
            // 학생기본정보의 현재재학이력이 이 재학이력을 가리키고 있지 않았는데, 전달된 인자의 currentSchoolHistory 가 참이면, 바뀔 현재재학이력은 이 재학이력이 된다.
            val newCurrentStudentSchoolHistoryId = model.currentStudentSchoolHistory.filter(_ == true).flatMap(_ => model.id).orElse(None)
            if (StudentTable.filter(t => t.id === studentModel.id && TableVersion.predicate(t, studentModel)).
              update(studentModel.copy(currentStudentSchoolHistoryId = newCurrentStudentSchoolHistoryId).modifyVersion()) != 1)
              throw new RuntimeException
          }
        result
      }.get
  }

  private def innerDeleteEach(implicit session: Session): Int = {
    StudentSchoolHistoryTable.
      filter(t => t.id === params.get(studentSchoolHistoryId).map(_.toInt) && t.studentId === params.get(studentId).map(_.toInt) && TableVersion.predicate2(t, params.get(Version.version))).
      delete
  }

  private def innerGetEach(implicit session: Session): Option[StudentSchoolHistoryModel] = {
    StudentSchoolHistoryTable.
      filter(t => t.studentId === params.get(studentId).map(_.toInt) && t.id === params.get(studentSchoolHistoryId).map(_.toInt)).
      joinLeft(StudentTable).
      on { case (a, b) => a.studentId === b.id }.
      joinLeft(SchoolTable).
      on { case ((a, _), b) => a.schoolId === b.id }.
      firstOption.
      map {
        case ((a, b), c) =>
          a.copy(student = b, school = c, currentStudentSchoolHistory = Option(a.id == b.flatMap(_.currentStudentSchoolHistoryId))) // id 에 대한 null 검사를 할필요가 없는데, id 는 무조건 null 이 아니기 때문이다.
      }
  }
}

object StudentSchoolHistoryTrait {
  val studentId = "studentId"
  val studentSchoolHistory = "studentSchoolHistory"
  val studentSchoolHistoryId = "studentSchoolHistoryId"
}
