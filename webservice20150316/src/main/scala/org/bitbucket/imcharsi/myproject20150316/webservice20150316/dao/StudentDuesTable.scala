/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao

import java.util.Date

import org.bitbucket.imcharsi.myproject20150316.model20150316.{ StudentDuesModel, StudentDuesModelEx }
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._

/**
 * Created by i on 3/30/15.
 */
class StudentDuesTable(t: Tag) extends Table[StudentDuesModel](t, "student_dues_table") with TableVersion[StudentDuesModel] {
  implicit val dateMapper = DriverSettings.dateMapper

  def id = column[Option[Int]]("id", O.PrimaryKey, O.AutoInc)

  override def version = column[Option[Int]]("version", O.NotNull)

  def studentId = column[Option[Int]]("student_id")

  // 기준연월을 뜻하는 이 항목의 일은 1이어야 하고 시분초/milli초 모두 0이어야 한다. json 변환에 따라 milli 초까지 변환하게 되고 database 에도 그대로 저장된다.
  // 이에 대한 검증/보정을 webservice 에서 할지말지는 좀더 생각해보자.
  def axisPayDate = column[Option[Date]]("axis_pay_date")

  def plannedPayDate = column[Option[Date]]("planned_pay_date")

  def actuallyPayDate = column[Option[Date]]("actually_pay_date")

  def amount = column[Option[Int]]("amount", O.NotNull) // 미납금 합계를 계산할 때, null 이 있으면 결과는 null 이다.

  def foreignKey1 = foreignKey(f"${tableName}_fkey1", studentId, StudentTable)(_.id)

  def unique1 = index(f"${tableName}_uq1", (studentId, axisPayDate), true) // 학생 한사람, 기준연월 한개에 대해서 한개의 납부이력만 있을 수 있다.

  override def * = (id, version, studentId, axisPayDate, plannedPayDate, actuallyPayDate, amount) <> ((StudentDuesModelEx.apply _).tupled, StudentDuesModelEx.unapply)
}

object StudentDuesTable extends TableQuery(t => new StudentDuesTable(t))
