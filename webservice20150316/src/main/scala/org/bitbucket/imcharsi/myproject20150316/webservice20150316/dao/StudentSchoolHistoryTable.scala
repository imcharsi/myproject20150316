/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao

import java.util.Date

import org.bitbucket.imcharsi.myproject20150316.model20150316.{ StudentSchoolHistoryModel, StudentSchoolHistoryModelEx }
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._

/**
 * Created by i on 3/25/15.
 */
class StudentSchoolHistoryTable(t: Tag) extends Table[StudentSchoolHistoryModel](t, "student_school_hist_table") with TableVersion[StudentSchoolHistoryModel] {
  implicit val dateMapper = DriverSettings.dateMapper

  def id = column[Option[Int]]("id", O.PrimaryKey, O.AutoInc)

  override def version = column[Option[Int]]("version", O.NotNull)

  def studentId = column[Option[Int]]("student_id")

  //  def schoolType = column[Option[Int]]("school_type")

  def schoolId = column[Option[Int]]("school_id")

  def currentDate = column[Option[Date]]("current_date")

  def currentGrade = column[Option[Int]]("current_grade")

  def classNumber = column[Option[Int]]("class_number")

  def foreignKey1 = foreignKey(f"${tableName}_fkey1", studentId, StudentTable)(_.id)

  def foreignKey2 = foreignKey(f"${tableName}_fkey2", schoolId, SchoolTable)(_.id)

  override def * = (id, version, studentId, /*schoolType,*/ schoolId, currentDate, currentGrade, classNumber) <> ((StudentSchoolHistoryModelEx.apply _).tupled, StudentSchoolHistoryModelEx.unapply)
}

object StudentSchoolHistoryTable extends TableQuery(t => new StudentSchoolHistoryTable(t))
