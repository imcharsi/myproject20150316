/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.webservice

import javax.servlet.http.{ HttpServletRequest, HttpServletResponse }

import org.bitbucket.imcharsi.myproject20150316.model20150316.LoginUserModel
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.{ DriverSettings, LoginUserTable }
import org.scalatra.ScalatraBase
import org.scalatra.auth.strategy.{ BasicAuthStrategy, BasicAuthSupport }
import org.scalatra.auth.{ ScentryConfig, ScentrySupport }

import scala.util.{ Failure, Success, Try }

/**
 * Created by i on 4/7/15.
 */
class SampleAuthStrategies(protected override val app: ScalatraBase, realm: String) extends BasicAuthStrategy[LoginUserModel](app, realm) {
  override protected def getUserId(user: LoginUserModel)(implicit request: HttpServletRequest, response: HttpServletResponse): String = user.userName.get

  override protected def validate(userName: String, password: String)(implicit request: HttpServletRequest, response: HttpServletResponse): Option[LoginUserModel] = {
    Try {
      DriverSettings.database.withTransaction { implicit slickSession =>
        LoginUserTable.filter(t => t.userName === Option(userName) && t.password === Option(password) && t.available === Option(true)).firstOption
      }
    } match {
      case Success(x) => x
      case Failure(x) => throw x
    }
  }
}

// http://www.scalatra.org/2.2/guides/http/authentication.html

trait SampleAuthSupport extends ScentrySupport[LoginUserModel] with BasicAuthSupport[LoginUserModel] {
  self: ScalatraBase =>

  override def realm: String = "hi"

  override protected def scentryConfig: ScentryConfiguration = (new ScentryConfig {}).asInstanceOf[ScentryConfiguration]

  override protected def fromSession: PartialFunction[String, LoginUserModel] = {
    case id => LoginUserModel(None, None, Option(id), None, None, None)
  }

  override protected def toSession: PartialFunction[LoginUserModel, String] = {
    case user => user.userName.get
  }

  override protected def configureScentry(): Unit = {
    scentry.unauthenticated {
      scentry.strategies("Basic").unauthenticated()
    }
  }

  override protected def registerAuthStrategies(): Unit = {
    super.registerAuthStrategies()
    scentry.register("Basic", app => new SampleAuthStrategies(app, realm))
  }

  before() {
    basicAuth()
  }
}
