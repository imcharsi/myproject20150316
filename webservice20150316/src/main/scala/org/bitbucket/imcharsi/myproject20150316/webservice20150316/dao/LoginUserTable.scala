/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao

//import DriverSettings.driver._
import org.bitbucket.imcharsi.myproject20150316.model20150316.LoginUserModel
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._

/**
 * Created by i on 4/7/15.
 */
class LoginUserTable(t: Tag) extends Table[LoginUserModel](t, "login_user_table") with TableVersion[LoginUserModel] {
  def id = column[Option[Int]]("id", O.PrimaryKey, O.AutoInc)

  override def version = column[Option[Int]]("version", O.NotNull)

  def userName = column[Option[String]]("username", O.NotNull)

  def password = column[Option[String]]("password", O.NotNull)

  def personName = column[Option[String]]("person_name")

  def available = column[Option[Boolean]]("available", O.NotNull)

  def unique1 = index(f"${tableName}_uq1", userName, true)

  def * = (id, version, userName, password, personName, available) <> (LoginUserModel.tupled, LoginUserModel.unapply)
}

object LoginUserTable extends TableQuery(t => new LoginUserTable(t))
