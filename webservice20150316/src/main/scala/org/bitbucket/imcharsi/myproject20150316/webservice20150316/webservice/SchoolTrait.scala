/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.webservice

import org.bitbucket.imcharsi.myproject20150316.model20150316.{ SchoolModel, SchoolType, Version }
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.{ SchoolTable, TableVersion }

/**
 * Created by i on 3/25/15.
 */
trait SchoolTrait extends AbstractServlet {
  import SchoolTrait._

  // todo 학교를 기준으로 학생을 고르고 싶으면 어떻게 할것인가.

  get("/", predicateAccept)(getAll(innerGetAll(_)))

  post("/", predicateAccept && predicateContentType)(postEach(innerPostEach(_)))

  put(f"/:${schoolId}", predicateContentType)(putEach(innerPutEach(_)))

  delete(f"/:${schoolId}")(deleteEach(innerDeleteEach(_)))

  get(f"/:${schoolId}", predicateAccept)(getEach(innerGetEach(_)))

  private def innerGetAll(implicit session: Session): List[SchoolModel] = {
    val searchCondition = params.get("searchCondition").filterNot(_.isEmpty).map(_.toInt).filterNot(_ == 0)
    searchCondition match {
      case Some(_) => SchoolTable.filter(t => t.schoolType === searchCondition).sortBy(_.id.asc).list
      case None => SchoolTable.sortBy(_.id.asc).list
    }
  }

  private def innerPostEach(implicit session: Session): SchoolModel = {
    parsedBody.extractOpt[SchoolModel].
      map(_.copy(version = Option(1))).
      filter(_.id.isEmpty).
      filter(checkSchoolType(_)).
      map { model =>
        model.copy(id = SchoolTable.returning(SchoolTable.map(_.id)).insert(model))
      }.get
  }

  private def checkSchoolType(m: SchoolModel): Boolean = {
    // todo schoolType 이 none 이면 .getOrElse(true) 로 한다. 선택을 하지 않아도 되기 때문이다. 허용되는 값 외의 다른 값을 쓰면 안된다. 이것을 database 에서 검사 제약조건으로 써놔야 한다. 아직은 slick 에서 검사 제약조건을 쓰는 기능이 없다.
    m.schoolType.map(t => SchoolType.schoolTypeList.contains(t)).getOrElse(true)
  }

  private def pred[M <: Version[M]](t: TableVersion[M], m: M): Rep[Option[Boolean]] = t.version === m.version

  private def innerPutEach(implicit session: Session): Int = {
    parsedBody.extractOpt[SchoolModel].
      filter(_.id == params.get(schoolId).map(_.toInt)).
      filter(checkSchoolType(_)).
      map { model =>
        SchoolTable.filter(t => t.id === model.id && TableVersion.predicate(t, model)).update(model.modifyVersion())
      }.get
  }

  private def innerDeleteEach(implicit session: Session): Int = {
    params.get(schoolId).map(_.toInt).map { id =>
      SchoolTable.filter(t => t.id === id && TableVersion.predicate2(t, params.get(Version.version))).delete
    }.get
  }

  private def innerGetEach(implicit session: Session): Option[SchoolModel] = {
    SchoolTable.filter(_.id === params.get(schoolId).map(_.toInt)).firstOption
  }
}

object SchoolTrait {
  val schoolId = "schoolId"
}