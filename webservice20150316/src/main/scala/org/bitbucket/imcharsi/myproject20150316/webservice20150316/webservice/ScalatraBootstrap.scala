/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.webservice

import javax.servlet.ServletContext

import org.scalatra.LifeCycle

/**
 * Created by i on 3/19/15.
 */
class ScalatraBootstrap extends LifeCycle {
  override def init(context: ServletContext): Unit = {
    super.init(context)
    // 이 구절은, test 를 쉽게 하기 위해 썼다. 실제로 쓸때는 이구절을 써서는 안된다. 매번 기동을 시작할때마다 database 내용을 지우고 새로 할수는 없기 때문이다.
    //    DriverSettings.dropTables(DriverSettings.schemaList)
    //    DriverSettings.createTables(DriverSettings.schemaList)
    context mount(classOf[StudentServlet], f"/${StudentServlet.student}/*")
    context mount(classOf[SchoolServlet], f"/${SchoolServlet.school}/*")
    context mount(classOf[StudentDuesServlet], f"/${StudentDuesServlet.studentDues}/*")
    context mount(classOf[LoginUserServlet], f"/${LoginUserServlet.loginUser}/*")
  }
}
