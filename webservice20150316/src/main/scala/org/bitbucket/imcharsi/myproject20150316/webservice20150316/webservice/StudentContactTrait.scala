/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.webservice

import org.bitbucket.imcharsi.myproject20150316.model20150316.{ StudentContactModel, Version }
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.{ StudentContactTable, StudentTable, TableVersion }

/**
 * Created by i on 3/20/15.
 */
trait StudentContactTrait extends AbstractServlet {
  import StudentContactTrait._

  get(f"/:${studentId}/${studentContact}", predicateAccept)(getAll(innerGetAll(_)))
  post(f"/:${studentId}/${studentContact}", predicateAccept && predicateContentType)(postEach(innerPostEach(_)))
  put(f"/:${studentId}/${studentContact}/:${studentContactId}", predicateContentType)(putEach(innerPutEach(_)))
  get(f"/:${studentId}/${studentContact}/:${studentContactId}", predicateAccept)(getEach(innerGetEach(_)))
  delete(f"/:${studentId}/${studentContact}/:${studentContactId}")(deleteEach(innerDeleteEach(_)))

  private def innerGetAll(implicit s: Session): List[StudentContactModel] = {
    StudentContactTable.
      joinLeft(StudentTable).on(_.studentId === _.id). // StudentModel 은 계산열을 가지고 있지만, 이와 같이 StudentModel 을 구하는 것이 목적이 아닐 때는 StudentModel 의 계산열은 구하지 않기로 한다.
      filter {
        case (a, b) => a.studentId === params.get(studentId).map(_.toInt)
      }.sortBy {
        case (a, b) => a.id.asc
      }.list.map {
        case (a, b) => a.copy(student = b, currentStudentContact = Option(b.flatMap(_.currentStudentContactId) == a.id))
      }
  }

  private def retrieveModel: Option[StudentContactModel] = parsedBody.extractOpt[StudentContactModel]

  private def innerPostEach(implicit s: Session): StudentContactModel = {
    retrieveModel.map(_.copy(version = Some(1))).
      filter(_.id.isEmpty).
      filter(_.studentId == params.get(studentId).map(_.toInt)).
      map { model =>
        val id = StudentContactTable.returning(StudentContactTable.map(_.id)).insert(model)
        // 계산열의 값을 인식하여, student contact url 에서 반대로 student 의 current student contact id 를 바꿀수 있도록 한다.
        // post 에서는, currentStudentContact==true 일때만 student model 을 바꾼다.
        model.currentStudentContact.filter(_ == true).foreach { _ =>
          val studentModel = StudentTable.filter(_.id === model.studentId).firstOption
          // database 에 뭔가 하면 무조건 version 을 바꾼다.
          if (StudentTable.filter(t => t.id === model.studentId && TableVersion.predicate3(t, studentModel.flatMap(_.version))).
            update(studentModel.map(_.copy(currentStudentContactId = id).modifyVersion()).get) != 1) {
            throw new RuntimeException
          }
        }
        id
      }.flatMap { id =>
        StudentContactTable.
          joinLeft(StudentTable).on(_.studentId === _.id).
          filter {
            case (a, b) => a.id === id
          }.firstOption
      }.map {
        case (a, b) => a.copy(student = b, currentStudentContact = Option(b.flatMap(_.currentStudentContactId) == a.id))
      }.get
  }

  private def innerPutEach(implicit s: Session): Int = {
    retrieveModel.
      filter(_.id == params.get(studentContactId).map(_.toInt)).
      filter(_.studentId == params.get(studentId).map(_.toInt)).
      map { x =>
        val result = StudentContactTable.
          filter(t => t.id === x.id && t.studentId === x.studentId && TableVersion.predicate(t, x)).
          update(x.modifyVersion())
        // 전달받은 인자의 currentStudentContact==true 이거나, 이 연락처가 현재연락처였다면, 무조건 student model 도 바꾸기로 하자.
        // 위 두 조건은 다른 말인데, currentStudentContact==true 이면, 앞으로 이 연락처를 현재연락처로 하겠다는 뜻이고,
        // 이 연락처가 지금까지 현재연락처였다면, 이번 변경으로 인해 이 연락처가 더이상 현재연락처가 되지 않을수도 있다는 말이다.
        StudentTable.filter(_.id === x.studentId).firstOption.
          filter(_.currentStudentContactId == x.id || x.currentStudentContact == Some(true)).
          map(_.copy(currentStudentContactId = x.currentStudentContact.filter(_ == true).flatMap(_ => x.id).orElse(None))).
          map { studentModel =>
            StudentTable.filter(t => t.id === studentModel.id && TableVersion.predicate(t, studentModel)).
              update(studentModel.modifyVersion())
          }.foreach { result =>
            result match {
              case 1 =>
              case _ => throw new RuntimeException
            }
          }
        result
      }.get
  }

  private def innerGetEach(implicit s: Session): Option[StudentContactModel] = {
    StudentContactTable.
      joinLeft(StudentTable).on(_.studentId === _.id).
      filter {
        case (a, b) =>
          a.studentId === params.get(studentId).map(_.toInt) &&
            a.id === params.get(studentContactId).map(_.toInt)
      }.firstOption.map {
        case (a, b) => a.copy(student = b, currentStudentContact = Option(b.flatMap(_.currentStudentContactId) == a.id))
      }
  }

  private def innerDeleteEach(implicit s: Session): Int = {
    // 지우기에 앞서, 이 연락처가 현재연락처로 지정되어 있다면, 현재연락처 지정을 먼저 풀고 지워야 한다.
    // 이 과정을 수작업으로 하도록 하는게 나을까. 자동으로 하는게 나을까.
    // 그런데 자동으로 한다면, 어떤 database 는 on delete null 이 되기도 하고 안되기도 한다.
    // 일단은, 수작업으로 하도록 하자. 지워보고 안지워지면 현재연락처로 지정되어 있지는 않은지 본인이 직접 확인하고, 지정을 풀어낸 다음, 다시 지우기를 시도해야 한다.
    StudentContactTable.
      filter {
        case a =>
          a.studentId === params.get(studentId).map(_.toInt) &&
            a.id === params.get(studentContactId).map(_.toInt) &&
            TableVersion.predicate2(a, params.get(Version.version))
      }.delete
  }
}

object StudentContactTrait {
  val studentId = "studentId"
  val studentContact = "studentContact"
  val studentContactId = "studentContactId"
}
