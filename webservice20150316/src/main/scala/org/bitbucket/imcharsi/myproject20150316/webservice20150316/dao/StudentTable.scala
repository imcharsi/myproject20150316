/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao

import org.bitbucket.imcharsi.myproject20150316.model20150316.{ StudentModel, StudentModelEx }
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._

/**
 * Created by i on 3/19/15.
 */
class StudentTable(t: Tag) extends Table[StudentModel](t, "student_table") with TableVersion[StudentModel] {
  def id = column[Option[Int]]("id", O.PrimaryKey, O.AutoInc)

  override def version = column[Option[Int]]("version", O.NotNull)

  def name = column[Option[String]]("name")

  def currentStudentSchoolHistoryId = column[Option[Int]]("current_school_hist_id")

  def currentStudentContactId = column[Option[Int]]("current_contact_id")

  def currentDuesAmount = column[Option[Int]]("current_dues_amount")

  def distanceOfPlannedDuesDay = column[Option[Int]]("distance_of_pdd")

  def currentAttendingStudent = column[Option[Boolean]]("current_attending")

  // fixme 아마도 이와 같이 구성하면 slick 은 ddl 을 제대로 실행하지 못할 것 같다. 만약 student table 의 ddl 을 먼저 실행한다면 student school history table 이 아직 없는 상태에서 외래키 지정을 하게 되기 때문이다. 반대 순서로 해도 마찬가지이다. 따라서 create table 문과 alter table 문을 분리해야 하는데, slick 에 이와 같은 기능은 있나. 없다. 일단, 직접 문장 내용을 보고 나눠서 써야 한다.
  def foreignKey1 = foreignKey(f"${tableName}_fkey1", currentStudentSchoolHistoryId, StudentSchoolHistoryTable)(_.id)

  override def * = (id, version, name, currentStudentSchoolHistoryId, currentStudentContactId, currentDuesAmount, distanceOfPlannedDuesDay, currentAttendingStudent) <> ((StudentModelEx.apply _).tupled, StudentModelEx.unapply)
}

object StudentTable extends TableQuery(x => new StudentTable(x))