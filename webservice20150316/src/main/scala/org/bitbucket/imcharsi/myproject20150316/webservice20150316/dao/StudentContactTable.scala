/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao

import org.bitbucket.imcharsi.myproject20150316.model20150316.{ StudentContactModel, StudentContactModelEx }
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._

/**
 * Created by i on 3/19/15.
 */
class StudentContactTable(t: Tag) extends Table[StudentContactModel](t, "student_contact_table") with TableVersion[StudentContactModel] {
  def id = column[Option[Int]]("id", O.PrimaryKey, O.AutoInc)

  def version = column[Option[Int]]("version", O.NotNull)

  def callNumber = column[Option[String]]("call_number")

  def address = column[Option[String]]("adress")

  def studentId = column[Option[Int]]("student_id")

  def validity = column[Option[Boolean]]("validity")

  def foreignKeyOne = foreignKey(f"${tableName}_fkey1", studentId, StudentTable)(t => t.id)

  override def * = (id, version, callNumber, address, studentId, validity) <> ((StudentContactModelEx.apply _).tupled, StudentContactModelEx.unapply)
}

object StudentContactTable extends TableQuery(x => new StudentContactTable(x))
