/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.webservice

import org.bitbucket.imcharsi.myproject20150316.model20150316.{ StudentDuesModel, StudentDuesSummaryModel, Version }
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.{ DriverSettings, StudentDuesTable, StudentTable, TableVersion }
import org.scalatra.{ ActionResult, InternalServerError, Ok }

import scala.util.{ Failure, Success, Try }

/**
 * Created by i on 3/30/15.
 */
trait StudentDuesTrait extends AbstractServlet {
  implicit private val dateMapper = DriverSettings.dateMapper

  // 이 기능은, 학생 각각의 회비납부이력을 다룰 때 쓰는 것이고,
  // 기준연월을 기준으로 전체 학생들의 회비납부여부 확인등을 하고 싶을 때는 다른 url 을 쓰는 것이 좀더 취지에 맞는 듯하다.
  // 예를 들면, /studentDues/2015/03/student/ 와 같이 하면, 2015년 03월의 학생 전체의 회비 납부이력을 학생들을 기준으로 보여주는 것이다.
  // 이 기능을 통해서 보여지는 학생 목록은, 납부이력이 있든 없든 전체 학생목록을 보여주고 거기에 더해서 납부이력까지 같이 보여줄수도 있고,
  // 납부이력이 있는 학생들만 목록에서 보여줄수도 있도록 하자.

  import StudentDuesTrait._

  private def innerGetAll(implicit session: Session): List[StudentDuesModel] = {
    StudentDuesTable.
      joinLeft(StudentTable).on(_.studentId === _.id).
      filter { case (a, _) => a.studentId === params.get(studentId).filterNot(_.isEmpty).map(_.toInt) }.
      sortBy { case (a, _) => a.axisPayDate.desc }. // 가장 최근 납부이력이 가장 위로 오도록 하자.
      list.
      map { case (a, b) => a.copy(student = b) }
  }

  private def innerGetEach(implicit session: Session): Option[StudentDuesModel] = {
    StudentDuesTable.
      joinLeft(StudentTable).on(_.studentId === _.id).
      filter { case (a, _) => a.studentId === params.get(studentId).map(_.toInt) && a.id === params.get(studentDuesId).map(_.toInt) }.
      firstOption.
      map { case (a, b) => a.copy(student = b) }
  }

  private def innerPostEach(implicit session: Session): StudentDuesModel = {
    parsedBody.extractOpt[StudentDuesModel].map(_.copy(version = Some(1))).map { model =>
      StudentDuesTable.returning(StudentDuesTable.map(_.id)).insert(model)
    }.flatMap { id =>
      StudentDuesTable.
        joinLeft(StudentTable).on(_.studentId === _.id).
        filter { case (a, _) => a.studentId === params.get(studentId).map(_.toInt) && a.id === id }.
        firstOption.
        map { case (a, b) => a.copy(student = b) }
    }.get
  }

  private def innerPutEach(implicit session: Session): Int = {
    val paramStudentId = params.get(studentId).map(_.toInt)
    val paramStudentDuesId = params.get(studentDuesId).map(_.toInt)
    parsedBody.extractOpt[StudentDuesModel].filter(_.studentId == paramStudentId).map { model =>
      StudentDuesTable.filter(t => t.studentId === paramStudentId && t.id === paramStudentDuesId && TableVersion.predicate(t, model)).update(model.modifyVersion())
    }.get
  }

  private def innerDeleteEach(implicit session: Session): Int = {
    val paramStudentId = params.get(studentId).map(_.toInt)
    val paramStudentDuesId = params.get(studentDuesId).map(_.toInt)
    StudentDuesTable.filter(t => t.studentId === paramStudentId && t.id === paramStudentDuesId && TableVersion.predicate2(t, params.get(Version.version))).delete
  }

  private def innerSummary(): ActionResult = {
    val joinA = StudentDuesTable.filter(t => t.studentId === params.get(studentId).map(_.toInt)).
      groupBy(_.studentId).map {
        case (studentId, table) =>
          (studentId, table.map(_.amount).sum)
      }
    val joinB = StudentDuesTable.filter(t => t.studentId === params.get(studentId).map(_.toInt) && t.actuallyPayDate.isDefined).
      groupBy(_.studentId).map {
        case (studentId, table) =>
          (studentId, table.map(_.amount).sum)
      }
    val joinC = StudentDuesTable.filter(t => t.studentId === params.get(studentId).map(_.toInt) && t.actuallyPayDate.isEmpty).
      groupBy(_.studentId).map {
        case (studentId, table) =>
          (studentId, table.map(_.amount).sum)
      }
    val result = Try {
      DriverSettings.database.withTransaction { implicit slickSession =>
        joinA.joinLeft(joinB).on { case ((a, _), (b, _)) => a === b }.
          joinLeft(joinC).on { case (((a, _), _), (b, _)) => a === b }.
          map {
            case (((a, b), c), d) =>
              (a, b, c.flatMap(_._2), d.flatMap(_._2))
          }.firstOption.
          map {
            case (_, a, b, c) =>
              StudentDuesSummaryModel(a, b, c)
          }.getOrElse(StudentDuesSummaryModel(None, None, None))
      }
    }
    result match {
      case Success(x) => Ok(x)
      case Failure(x) => InternalServerError(x)
    }
  }

  get(f"/:${studentId}/${studentDues}", predicateAccept)(getAll(innerGetAll(_)))

  post(f"/:${studentId}/${studentDues}", predicateAccept && predicateContentType)(postEach(innerPostEach(_)))

  put(f"/:${studentId}/${studentDues}/:${studentDuesId}", predicateContentType)(putEach(innerPutEach(_)))

  delete(f"/:${studentId}/${studentDues}/:${studentDuesId}")(deleteEach(innerDeleteEach(_)))

  get(f"/:${studentId}/${studentDues}/:${studentDuesId}", predicateAccept)(getEach(innerGetEach(_)))

  get(f"/:${studentId}/${studentDues}/${summary}", predicateAccept)(innerSummary)
}

object StudentDuesTrait {
  val studentId = "studentId"
  val studentDues = "studentDues"
  val studentDuesId = "studentDuesId"
  val summary = "summary"
}
