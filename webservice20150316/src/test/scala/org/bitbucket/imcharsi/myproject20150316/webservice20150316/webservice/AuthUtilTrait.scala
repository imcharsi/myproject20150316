/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.webservice

import org.apache.commons.codec.binary.Base64
import org.bitbucket.imcharsi.myproject20150316.model20150316.LoginUserModel
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.{ DriverSettings, LoginUserTable }

/**
 * Created by i on 4/8/15.
 */
trait AuthUtilTrait {
  val testLoginUserModel = LoginUserModel(None, Option(1), Option("hi"), Option("there"), Option("#1"), Option(true))

  protected def initTestLogin(): Unit = {
    DriverSettings.database.withTransaction { implicit s =>
      Option(testLoginUserModel).flatMap { model =>
        val id = LoginUserTable.returning(LoginUserTable.map(_.id)).insert(model)
        LoginUserTable.filter(_.id === id).firstOption
      }
    }
  }

  protected def generateTestAuthenticationHeader(): (String, String) = {
    ("Authorization", "Basic " + Base64.encodeBase64String(f"${testLoginUserModel.userName.get}:${testLoginUserModel.password.get}".getBytes))
  }

}
