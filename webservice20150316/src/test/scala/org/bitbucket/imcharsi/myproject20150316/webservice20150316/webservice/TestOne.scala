/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.webservice

import java.util.Calendar

import org.bitbucket.imcharsi.myproject20150316.model20150316._
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao._
import org.scalatra.ScalatraServlet

/**
 * Created by i on 3/19/15.
 */
class TestOne extends AbstractTest {
  override val servletList: Map[Class[_ <: ScalatraServlet], String] = Map((classOf[StudentServlet] -> f"/${StudentServlet.student}/*"), (classOf[SchoolServlet] -> f"/${SchoolServlet.school}/*"), (classOf[StudentDuesServlet] -> f"/${StudentDuesServlet.studentDues}/*"))
  override val schemaList: List[DriverSettings.driver.SchemaDescription] = List(StudentTable.schema, StudentContactTable.schema, SchoolTable.schema, StudentSchoolHistoryTable.schema, StudentDuesTable.schema, LoginUserTable.schema)

  var studentModel: Option[StudentModel] = None
  var studentModel2: Option[StudentModel] = None
  var studentModel3: Option[StudentModel] = None
  var studentModelList: List[StudentModel] = Nil
  var studentContactModel: Option[StudentContactModel] = None
  var studentContactModel2: Option[StudentContactModel] = None
  var studentContactModelList: List[StudentContactModel] = Nil
  var schoolModel: Option[SchoolModel] = None
  var schoolModel2: Option[SchoolModel] = None
  var schoolModelList: List[SchoolModel] = Nil
  var studentSchoolHistoryModel: Option[StudentSchoolHistoryModel] = None
  var studentSchoolHistoryModel2: Option[StudentSchoolHistoryModel] = None
  var studentSchoolHistoryModelList: List[StudentSchoolHistoryModel] = Nil
  var studentDuesModel: Option[StudentDuesModel] = None
  var studentDuesModel2: Option[StudentDuesModel] = None
  var studentDuesModelList: List[StudentDuesModel] = Nil

  // 모든 post test 에서 반환값이 getEach 와 같은지 확인해야 하는데, 이 단계에서는 들어있는 값이 없으므로 확인할 것도 없다. 이 다음단계에서부터 전부 확인해야 한다.
  test(f"POST  /${StudentServlet.student}") {
    val model = StudentModelEx(None, None, Some("hi"), None, None, None, None, Some(true)) // 기준연월별 납부이력 검사는 개별 검사에서 한다. 여기서는 조건검색을 위한 준비를 하기로 하자.
    postEachTemplate[StudentModel](model, _ => f"/${StudentServlet.student}", (a, b) => a.copy(id = b.id, version = b.version) == b && b.version.isDefined, a => studentModel = Option(a))
  }

  test(f"GET each  /${StudentServlet.student}/:${StudentTrait.studentId}") {
    studentModel.foreach { model =>
      getEachTemplate[StudentModel](model,
        model => f"/${StudentServlet.student}/${model.id.get}",
        (a, b) => a == b.copy(remainingDuesAmount = None, currentStudentSchoolHistory = None, currentStudentContact = None),
        a => studentModel = Option(a))
    }
  }

  test(f"GET all  /${StudentServlet.student}") {
    getAllTemplate[StudentModel](_ => f"/${StudentServlet.student}",
      a => a.map(m => Option(m)) == List(studentModel),
      a => studentModelList = a)
  }

  test(f"PUT  /${StudentServlet.student}/:${StudentTrait.studentId}") {
    val sampleModel = studentModel.map(_.copy(name = Some("bye")))
    sampleModel.foreach { model =>
      putEachTemplate[StudentModel](model, t => f"/student/${t.id.get}")
    }
    sampleModel.foreach { model =>
      getEachTemplate[StudentModel](model,
        model => f"/${StudentServlet.student}/${model.id.get}",
        (a, b) => a.copy(version = a.version.map(_ + 1)) == b.copy(remainingDuesAmount = None, currentStudentSchoolHistory = None, currentStudentContact = None),
        a => studentModel = Option(a))
    }
  }

  test("PUT 404 coverage") {
    studentModel.foreach { studentModel =>
      put(f"/${StudentServlet.student}/${studentModel.id.get - 1}",
        objectMapper.writeValueAsString(studentModel.copy(id = studentModel.id.map(_ - 1))),
        headerAuthentication ++ headerContentType) {
          status should (equal(404)) // coverage
        }
    }
  }

  test("PUT 500 coverage") {
    studentModel.foreach { studentModel =>
      put(f"/${StudentServlet.student}/failId",
        objectMapper.writeValueAsString(studentModel.copy(name = Some("bye"))),
        headerAuthentication ++ headerContentType) {
          status should (equal(500)) // coverage
        }
    }
  }

  test("GET each 404 coverage") {
    studentModel.foreach { studentModel =>
      get(f"/${StudentServlet.student}/${studentModel.id.get + 1}", Nil, headerAuthentication ++ headerAccept) {
        status should (equal(404)) // coverage
      }
    }
  }

  test("GET each 500 coverage") {
    studentModel.foreach { studentModel =>
      get(f"/${StudentServlet.student}/failId", Nil, headerAuthentication ++ headerAccept) {
        status should (equal(500)) // coverage
      }
    }
  }

  test(f"POST  /${StudentServlet.student} (2)") {
    val model = StudentModelEx(None, None, Some("hi"), None, None, None, None, Some(false))
    postEachTemplate[StudentModel](model, _ => "/student", (a, b) => a.copy(id = b.id, version = b.version) == b && b.version.isDefined, a => studentModel2 = Option(a))
  }

  test(f"GET all  /${StudentServlet.student} currentAttendingStudent==true") {
    get(f"/${StudentServlet.student}", List((StudentTrait.paramCurrentAttendingStudent, true.toString)), headerAuthentication ++ headerAccept) {
      status.should(equal(200))
      objectMapper.readValue[List[StudentModel]](body).map(x => Option(x)).should(equal(List(studentModel)))
    }
  }

  test(f"GET all  /${StudentServlet.student} currentAttendingStudent==false") {
    get(f"/${StudentServlet.student}", List((StudentTrait.paramCurrentAttendingStudent, false.toString)), headerAuthentication ++ headerAccept) {
      status.should(equal(200))
      objectMapper.readValue[List[StudentModel]](body).map(x => Option(x)).should(equal(List(studentModel2)))
    }
  }

  test(f"GET all  /${StudentServlet.student} currentAttendingStudent==null") {
    getAllTemplate[StudentModel](_ => f"/${StudentServlet.student}",
      a => a.map(m => Option(m)) == List(studentModel, studentModel2),
      a => studentModelList = a)
  }

  test(f"DELETE  /${StudentServlet.student} (2)") {
    studentModel2.foreach { model =>
      deleteEachTemplate[StudentModel](model, x => f"/student/${x.id.get}", x => List((Version.version, x.version.map(_.toString).get)), _ => None) // 다음에 있을 404 검사를 위해 참조대상을 바꾸지 않고 남겨둔다.
    }
  }

  test("DELETE 404 coverage") {
    studentModel2.foreach { studentModel =>
      delete(f"/student/${studentModel.id.get + 1}", List((Version.version, studentModel.version.map(_.toString).get)), headerAuthentication) {
        status should (equal(404)) // coverage
      }
    }
  }

  test("DELETE 500 coverage") {
    studentModel2.foreach { studentModel =>
      delete(f"/student/failId", List((Version.version, studentModel.version.map(_.toString).get)), headerAuthentication) {
        status should (equal(500)) // coverage
      }
    }
  }

  test(f"POST  /${StudentServlet.student}/:${StudentTrait.studentId}/${StudentContactTrait.studentContact}") {
    studentModel.map(x => StudentContactModel(None, None, Some("#1234"), Some("##1234"), x.id, Option(true), Option(false), None)).
      foreach { model =>
        postEachTemplate[StudentContactModel](model,
          m => f"/${StudentServlet.student}/${m.studentId.get}/${StudentContactTrait.studentContact}",
          (a, b) => {
            a.copy(id = b.id, version = b.version) == b.copy(student = None) && b.student.isDefined && b.version.isDefined
          },
          a => studentContactModel = Option(a))
      }
  }

  test(f"GET each  /${StudentServlet.student}/:${StudentTrait.studentId}/${StudentContactTrait.studentContact}/:${StudentContactTrait.studentContactId}") {
    studentContactModel.foreach { model =>
      getEachTemplate[StudentContactModel](model,
        m => f"/${StudentServlet.student}/${m.studentId.get}/${StudentContactTrait.studentContact}/${m.id.get}",
        (a, b) => a == b,
        a => studentContactModel = Option(a))
    }
  }

  test(f"PUT  /${StudentServlet.student}/:${StudentTrait.studentId}/${StudentContactTrait.studentContact}/:${StudentContactTrait.studentContactId}") {
    val sampleModel = studentContactModel.map(_.copy(callNumber = Some("#1235")))
    sampleModel.foreach { model =>
      putEachTemplate[StudentContactModel](model, model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentContactTrait.studentContact}/${model.id.get}")
    }
    sampleModel.foreach { model =>
      getEachTemplate[StudentContactModel](model,
        model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentContactTrait.studentContact}/${model.id.get}",
        (a, b) => a.copy(version = a.version.map(_ + 1)) == b,
        a => studentContactModel = Option(a))
    }
  }

  test(f"GET all  /${StudentServlet.student}/:${StudentTrait.studentId}/${StudentContactTrait.studentContact}") {
    studentModel.foreach { model =>
      getAllTemplate[StudentContactModel](_ => f"/${StudentServlet.student}/${model.id.get}/${StudentContactTrait.studentContact}",
        list => list.map(x => Option(x)) == List(studentContactModel),
        list => studentContactModelList = list)
    }
  }

  test("GET all 500 coverage") {
    studentContactModel.foreach { studentContactModel =>
      get(
        f"/${StudentServlet.student}/failId/${StudentContactTrait.studentContact}", Nil, headerAuthentication ++ headerAccept) {
          status should (equal(500))
        }
    }
  }

  test(f"POST  /${StudentServlet.student}/:${StudentTrait.studentId}/${StudentContactTrait.studentContact} (2)") {
    studentModel.map(x => StudentContactModel(None, None, Some("#1234"), Some("##1234"), x.id, Option(true), Option(false), None)).
      foreach { model =>
        postEachTemplate[StudentContactModel](model,
          m => f"/${StudentServlet.student}/${m.studentId.get}/${StudentContactTrait.studentContact}",
          (a, b) => a.copy(id = b.id, version = b.version) == b.copy(student = None) && b.student.isDefined,
          a => studentContactModel2 = Option(a))
      }
  }
  test(f"DELETE  /${StudentServlet.student}/:${StudentTrait.studentId}/${StudentContactTrait.studentContact}/:${StudentContactTrait.studentContactId}") {
    studentContactModel2.foreach { model =>
      deleteEachTemplate[StudentContactModel](model, model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentContactTrait.studentContact}/${model.id.get}", x => List((Version.version, x.version.map(_.toString).get)), _ => None)
      deleteEachTemplate404[StudentContactModel](model, model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentContactTrait.studentContact}/${model.id.get}", x => List((Version.version, x.version.map(_.toString).get)))
    }
  }

  test("POST 500 coverage") {
    studentContactModel.foreach { studentContactModel =>
      post(
        f"/${StudentServlet.student}/failId/${StudentContactTrait.studentContact}",
        objectMapper.writeValueAsString(StudentContactModelEx(None, None, Some("#1234"), Some("##1234"), Some(1), Option(true))),
        headerAuthentication ++ headerContentType ++ headerAccept) {
          status should (equal(500)) // coverage
        }
    }
  }

  test(f"POST  /${SchoolServlet.school}") {
    val model = SchoolModel(None, None, Some("#123"), Some(SchoolType.middleSchool))
    postEachTemplate[SchoolModel](model, _ => f"/${SchoolServlet.school}", (a, b) => a.copy(id = b.id, version = b.version) == b && b.version.isDefined, a => schoolModel = Option(a))
  }

  test(f"GET all  /${StudentServlet.student} name(true)") {
    get(f"/${StudentServlet.student}", List((StudentTrait.paramSearch, studentModel.flatMap(_.name).get)), headerAuthentication ++ headerAccept) {
      status.should(equal(200))
      objectMapper.readValue[List[StudentModel]](body).map(_.name).should(equal(List(studentModel).map(_.flatMap(_.name))))
    }
  }

  test(f"GET all  /${StudentServlet.student} name(false)") {
    get(f"/${StudentServlet.student}", List((StudentTrait.paramSearch, studentModel.flatMap(_.name).map(_ + "1").get)), headerAuthentication ++ headerAccept) {
      status.should(equal(200))
      objectMapper.readValue[List[StudentModel]](body).should(equal(Nil))
    }
  }

  test(f"GET each  /${SchoolServlet.school}/:${SchoolTrait.schoolId}") {
    schoolModel.foreach { model =>
      getEachTemplate[SchoolModel](model, model => f"/${SchoolServlet.school}/${model.id.get}", (a, b) => a == b, a => schoolModel = Option(a))
    }
  }

  test(f"PUT  /${SchoolServlet.school}/:${SchoolTrait.schoolId}") {
    val sampleModel = schoolModel.map(_.copy(name = Some("#2345")))
    sampleModel.foreach { model =>
      putEachTemplate[SchoolModel](model, model => f"/${SchoolServlet.school}/${model.id.get}")
    }
    sampleModel.foreach { model =>
      getEachTemplate[SchoolModel](model, model => f"/${SchoolServlet.school}/${model.id.get}", (a, b) => a.copy(version = a.version.map(_ + 1)) == b, a => schoolModel = Option(a))
    }
  }

  test(f"GET all  /${SchoolServlet.school}") {
    getAllTemplate[SchoolModel](_ => f"/${SchoolServlet.school}", list => list.map(x => Option(x)) == List(schoolModel), list => schoolModelList = list)
  }

  test(f"POST  /${SchoolServlet.school} (2)") {
    val model = SchoolModel(None, None, Some("#123"), Some(SchoolType.middleSchool))
    postEachTemplate[SchoolModel](model, _ => f"/${SchoolServlet.school}", (a, b) => a.copy(id = b.id, version = b.version) == b && b.version.isDefined, a => schoolModel2 = Option(a))
  }

  test(f"DELETE  /${SchoolServlet.school} (2)") {
    schoolModel2.foreach { model =>
      deleteEachTemplate[SchoolModel](model, model => f"/${SchoolServlet.school}/${model.id.get}", x => List((Version.version, x.version.map(_.toString).get)), _ => None)
      deleteEachTemplate404[SchoolModel](model, model => f"/${SchoolServlet.school}/${model.id.get}", x => List((Version.version, x.version.map(_.toString).get)))
    }
  }

  test(f"POST  /${StudentServlet.student}/:${StudentTrait.studentId}/${StudentSchoolHistoryTrait.studentSchoolHistory}") {
    val model = StudentSchoolHistoryModelEx(None, None, studentModel.flatMap(_.id), schoolModel.flatMap(_.id), Some(calendar.getTime), Some(1), Some(2))
    postEachTemplate[StudentSchoolHistoryModel](model,
      model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentSchoolHistoryTrait.studentSchoolHistory}",
      // 계산열을 제외한 나머지 모두의 값이 같은지, 계산열은 제대로 전달되었는지 확인한다.
      (a, b) => a.copy(id = b.id, version = b.version, currentStudentSchoolHistory = b.currentStudentSchoolHistory) == b.copy(student = None, school = None) && b.student.isDefined && b.school.isDefined && b.version.isDefined,
      model => studentSchoolHistoryModel = Option(model))
  }

  test(f"GET each  /${StudentServlet.student}/:${StudentTrait.studentId}/${StudentSchoolHistoryTrait.studentSchoolHistory}/:${StudentSchoolHistoryTrait.studentSchoolHistoryId}") {
    studentSchoolHistoryModel.foreach { model =>
      getEachTemplate[StudentSchoolHistoryModel](model,
        model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentSchoolHistoryTrait.studentSchoolHistory}/${model.id.get}",
        (a, b) => a == b,
        model => studentSchoolHistoryModel = Option(model))
    }
  }

  test(f"PUT  /${StudentServlet.student}/:${StudentTrait.studentId}/${StudentSchoolHistoryTrait.studentSchoolHistory}/:${StudentSchoolHistoryTrait.studentSchoolHistoryId}") {
    val sampleModel = studentSchoolHistoryModel.map(_.copy(classNumber = Option(123)))
    sampleModel.foreach { model =>
      putEachTemplate[StudentSchoolHistoryModel](model,
        model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentSchoolHistoryTrait.studentSchoolHistory}/${model.id.get}")
    }
    sampleModel.foreach { model =>
      getEachTemplate[StudentSchoolHistoryModel](model,
        model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentSchoolHistoryTrait.studentSchoolHistory}/${model.id.get}",
        (a, b) => a.copy(version = a.version.map(_ + 1)) == b,
        model => studentSchoolHistoryModel = Option(model))
    }
  }

  test(f"GET all  /${StudentServlet.student}/:${StudentTrait.studentId}/${StudentSchoolHistoryTrait.studentSchoolHistory}") {
    studentModel.foreach { model =>
      getAllTemplate[StudentSchoolHistoryModel](_ => f"/${StudentServlet.student}/${model.id.get}/${StudentSchoolHistoryTrait.studentSchoolHistory}",
        list => list.map(x => Option(x)) == List(studentSchoolHistoryModel),
        list => studentSchoolHistoryModelList = list)
    }
  }

  test(f"POST  /${StudentServlet.student}/:${StudentTrait.studentId}/${StudentSchoolHistoryTrait.studentSchoolHistory} (2)") {
    val model = StudentSchoolHistoryModelEx(None, None, studentModel.flatMap(_.id), schoolModel.flatMap(_.id), Some(calendar.getTime), Some(1), Some(2))
    postEachTemplate[StudentSchoolHistoryModel](model,
      model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentSchoolHistoryTrait.studentSchoolHistory}",
      (a, b) => a.copy(id = b.id, version = b.version, currentStudentSchoolHistory = b.currentStudentSchoolHistory) == b.copy(student = None, school = None) && b.student.isDefined && b.school.isDefined && b.version.isDefined,
      model => studentSchoolHistoryModel2 = Option(model))
  }

  test(f"DELETE  /${StudentServlet.student}/:${StudentTrait.studentId}/${StudentSchoolHistoryTrait.studentSchoolHistory}/:${StudentSchoolHistoryTrait.studentSchoolHistoryId} (2)") {
    studentSchoolHistoryModel2.foreach { model =>
      deleteEachTemplate[StudentSchoolHistoryModel](model, model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentSchoolHistoryTrait.studentSchoolHistory}/${model.id.get}", x => List((Version.version, x.version.map(_.toString).get)), _ => None)
      deleteEachTemplate404[StudentSchoolHistoryModel](model, model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentSchoolHistoryTrait.studentSchoolHistory}/${model.id.get}", x => List((Version.version, x.version.map(_.toString).get)))
    }
  }

  test(f"POST  /${StudentServlet.student}/:${StudentTrait.studentId}/${StudentDuesTrait.studentDues}") {
    val model = StudentDuesModelEx(None, None, studentModel.flatMap(_.id), Some(calendar.getTime), Some(calendar.getTime), Some(calendar.getTime), Some(100))
    postEachTemplate[StudentDuesModel](model,
      model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentDuesTrait.studentDues}",
      (a, b) => a.copy(id = b.id, version = b.version) == b.copy(student = None) && b.student.isDefined && b.version.isDefined,
      model => studentDuesModel = Option(model))
  }

  test(f"GET each  /${StudentServlet.student}/:${StudentTrait.studentId}/${StudentDuesTrait.studentDues}/:${StudentDuesTrait.studentDuesId}") {
    studentDuesModel.foreach { model =>
      getEachTemplate[StudentDuesModel](model,
        model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentDuesTrait.studentDues}/${model.id.get}",
        (a, b) => a == b,
        model => studentDuesModel = Option(model))
    }
  }

  test(f"PUT  /${StudentServlet.student}/:${StudentTrait.studentId}/${StudentDuesTrait.studentDues}/:${StudentDuesTrait.studentDuesId}") {
    val model = studentDuesModel.map(_.copy(amount = Some(120)))
    model.foreach { model =>
      putEachTemplate[StudentDuesModel](model, model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentDuesTrait.studentDues}/${model.id.get}")
    }
    model.foreach { model =>
      getEachTemplate[StudentDuesModel](model,
        model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentDuesTrait.studentDues}/${model.id.get}",
        (a, b) => a.copy(version = a.version.map(_ + 1)) == b,
        model => studentDuesModel = Option(model))
    }
  }

  test(f"GET all  /${StudentServlet.student}/:${StudentTrait.studentId}/${StudentDuesTrait.studentDues}") {
    studentModel.flatMap(_.id).foreach { id =>
      getAllTemplate[StudentDuesModel](_ => f"/${StudentServlet.student}/${id}/${StudentDuesTrait.studentDues}",
        list => list.map(x => Option(x)) == List(studentDuesModel),
        list => studentDuesModelList = list)
    }
  }

  test(f"POST  /${StudentServlet.student}/:${StudentTrait.studentId}/${StudentDuesTrait.studentDues} (unique test)") {
    val model = StudentDuesModelEx(None, None, studentModel.flatMap(_.id), Some(calendar.getTime), Some(calendar.getTime), Some(calendar.getTime), Some(100))
    post(f"/${StudentServlet.student}/${model.studentId.get}/${StudentDuesTrait.studentDues}", objectMapper.writeValueAsString(model), headerAuthentication ++ headerContentType ++ headerAccept) {
      status.should(equal(500)) // unique 제약조건에 따라, 동일한 학생 동일한 기준연월에 대해서 중복해서 납부이력이 만들어져서는 안된다.
    }
  }

  test(f"POST  /${StudentServlet.student}/:${StudentTrait.studentId}/${StudentDuesTrait.studentDues} (2)") {
    calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1)
    val model = StudentDuesModelEx(None, None, studentModel.flatMap(_.id), Some(calendar.getTime), Some(calendar.getTime), Some(calendar.getTime), Some(100))
    postEachTemplate[StudentDuesModel](model,
      model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentDuesTrait.studentDues}",
      (a, b) => a.copy(id = b.id, version = b.version) == b.copy(student = None) && b.student.isDefined,
      model => studentDuesModel2 = Option(model))
  }

  test(f"DELETE  /${StudentServlet.student}/:${StudentTrait.studentId}/${StudentDuesTrait.studentDues}/:${StudentDuesTrait.studentDuesId} (2)") {
    studentDuesModel2.foreach { model =>
      deleteEachTemplate[StudentDuesModel](model, model => f"/${StudentServlet.student}/${studentDuesModel2.flatMap(_.studentId).get}/${StudentDuesTrait.studentDues}/${studentDuesModel2.flatMap(_.id).get}", x => List((Version.version, x.version.map(_.toString).get)), _ => None)
      deleteEachTemplate404[StudentDuesModel](model, model => f"/${StudentServlet.student}/${studentDuesModel2.flatMap(_.studentId).get}/${StudentDuesTrait.studentDues}/${studentDuesModel2.flatMap(_.id).get}", x => List((Version.version, x.version.map(_.toString).get)))
    }
  }
}
