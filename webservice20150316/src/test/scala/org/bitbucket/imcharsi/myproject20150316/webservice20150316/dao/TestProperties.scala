/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao

import java.io.{BufferedReader, InputStreamReader}
import java.util.Properties

import org.scalatest.FunSuite

/**
 * Created by i on 4/14/15.
 */
class TestProperties extends FunSuite {
  test("test properties") {
    val inputStream = getClass.getResourceAsStream("/org/bitbucket/imcharsi/myproject20150316/webservice20150316/webservice/database.properties")
    val properties = new Properties
    properties.load(inputStream)
    properties
  }

}
