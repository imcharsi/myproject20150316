/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.webservice

import java.util.logging.Logger

import org.bitbucket.imcharsi.myproject20150316.model20150316.{ StudentDuesModel, StudentDuesSummaryModel, StudentModel, StudentModelEx }
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao._
import org.scalatra.ScalatraServlet

/**
 * Created by i on 4/1/15.
 */
class TestStudentDuesServlet extends AbstractTest {
  override val servletList: Map[Class[_ <: ScalatraServlet], String] = Map((classOf[StudentServlet] -> f"/${StudentServlet.student}/*"), (classOf[SchoolServlet] -> f"/${SchoolServlet.school}/*"), (classOf[StudentDuesServlet] -> f"/${StudentDuesServlet.studentDues}/*"))
  override val schemaList: List[DriverSettings.driver.SchemaDescription] = List(StudentTable.schema, StudentContactTable.schema, SchoolTable.schema, StudentSchoolHistoryTable.schema, StudentDuesTable.schema, LoginUserTable.schema)

  var studentModel: Option[StudentModel] = None
  var studentModel2: Option[StudentModel] = None
  var studentModel3: Option[StudentModel] = None
  var studentModel4: Option[StudentModel] = None

  val year = 2014
  val month = 4
  // /studentDues/:year/:month url 을 위한 새로운 test 다. 따라서 아무것도 없이 새로 시작한다.

  test(f"prepare") {
    // 납부이력 일괄생성에 앞서 기준연월의 납부이력이 없다는 것을 먼저 확인한다.
    getAllTemplate[StudentDuesModel](_ => f"/${StudentDuesServlet.studentDues}/${year}/${month}", list => list.isEmpty, _ => None)
    // 납부이력 일괄생성에 사용할 학생기본정보를 만든다. 한개의 학생기본정보는 납부이력일괄생성에 필요한 자료를 갖고 있고,
    val model = StudentModelEx(None, None, Some("#123"), None, None, Some(100), Some(20), Some(true))
    postEachTemplate[StudentModel](model, _ => f"/${StudentServlet.student}", (a, b) => a.copy(id = b.id, version = b.version) == b, a => studentModel = Option(a))
    // 또 다른 한개의 학생기본정보는 납부이력일괄생성에 필요한 자료를 갖고 있지 않다.
    val model2 = StudentModelEx(None, None, Some("#123"), None, None, None, None, Some(false))
    postEachTemplate[StudentModel](model2, _ => f"/${StudentServlet.student}", (a, b) => a.copy(id = b.id, version = b.version) == b, a => studentModel2 = Option(a))
    val model3 = StudentModelEx(None, None, Some("#123"), None, None, None, None, None) // null 인지 여부만 판단했었다. false 인지 여부도 판단해야 한다.
    postEachTemplate[StudentModel](model3, _ => f"/${StudentServlet.student}", (a, b) => a.copy(id = b.id, version = b.version) == b, a => studentModel3 = Option(a))
    // 그래서, 의도한 검사결과는 전체 학생수 2명에 일괄생성으로 1개의 납부이력이 만들어져야 한다.
    val model4 = StudentModelEx(None, None, Some("#123"), None, None, Some(80), Some(22), Some(true))
    postEachTemplate[StudentModel](model, _ => f"/${StudentServlet.student}", (a, b) => a.copy(id = b.id, version = b.version) == b, a => studentModel4 = Option(a))
  }

  test(f"batch generating StudentDuesModel data") {
    post(f"/${StudentDuesServlet.studentDues}/${year}/${month}", Nil, headerAuthentication) {
      Logger.getLogger(getClass.getName).info(body)
      status.should(equal(200))
      objectMapper.readValue[Int](body).should(equal(2)) // 앞서 만들어뒀던 학생기본정보를 바탕으로 납부이력을 자동으로 일괄생성했다. 예상개수는 2개이다.
    }
    // post 요청에 대한 반환값 검사 외에 /studentDues/:year/:month url 에 대한 get 요청의 반환결과 개수도 확인한다.
    getAllTemplate[StudentDuesModel](_ => f"/${StudentDuesServlet.studentDues}/${year}/${month}", list => list.length == 2, _ => None)
    getAllTemplate[StudentModel](_ => f"/${StudentServlet.student}",
      a => a.length == 4, // 전체 학생수는 현재 4명이어야 한다.
      _ => None)
  }

  test(f"batch generating StudentDuesModel data post validation") {
    post(f"/${StudentDuesServlet.studentDues}/${year}/${month}", Nil, headerAuthentication) {
      Logger.getLogger(getClass.getName).info(body)
      status.should(equal(500)) // 중복이므로 실패해야 한다. 기준연월에 해당하는 납부이력이 한개라도 있으면 전체 일괄생성이 실패해야 한다.
    }
    // 실패한 database transaction 이 rollback 되었다는 것을 확인한다. 납부이력이 더이상 늘어나지 않았음을 확인한다.
    getAllTemplate[StudentDuesModel](_ => f"/${StudentDuesServlet.studentDues}/${year}/${month}", list => list.length == 2, _ => None)
  }

  // 아직 기납부로 바꾸지 않았으므로, 2개의 미납이력이 조회되어야 한다.
  test(f"/${StudentDuesServlet.studentDues}/${StudentDuesTrait2.allUnpaid}") {
    getAllTemplate[StudentDuesModel](_ => f"/${StudentDuesServlet.studentDues}/${StudentDuesTrait2.allUnpaid}", list => list.length == 2, _ => None)
  }

  test(f"test summary 1") {
    val sum = studentModel.flatMap(_.currentDuesAmount).get + studentModel4.flatMap(_.currentDuesAmount).get
    // 합계의 결과는 list 가 아니다.
    getEachTemplate[StudentDuesSummaryModel](null,
      _ => f"/${StudentDuesServlet.studentDues}/${year}/${month}/${StudentDuesTrait2.summary}",
      (a, b) => b.allSum == Option(sum) && b.completeSum.orElse(Option(0)) == Option(0) && b.incompleteSum == Option(sum),
      _ => None)
  }

  // /studentDues/:year/:month url 의 tab 인자 검사에 앞서 납부이력을 기납부로 바꿨다.
  test(f"change to paid status") {
    var resultList: List[StudentDuesModel] = Nil
    getAllTemplate[StudentDuesModel](_ => f"/${StudentDuesServlet.studentDues}/${year}/${month}", list => list.length == 2, resultList = _)
    val model = resultList.headOption
    val modifiedModel = model.map(_.copy(actuallyPayDate = model.flatMap(_.plannedPayDate)))
    modifiedModel.foreach { model =>
      putEachTemplate[StudentDuesModel](model, model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentDuesTrait.studentDues}/${model.id.get}")
    }
    modifiedModel.foreach { model =>
      getEachTemplate[StudentDuesModel](model,
        model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentDuesTrait.studentDues}/${model.id.get}",
        (a, b) => a.copy(version = a.version.map(_ + 1)) == b, _ => None)
    }
  }

  test(f"test 'tab' get parameter") {
    List((StudentDuesTrait2.tabAll, 2), (StudentDuesTrait2.tabAlreadyPaid, 1), (StudentDuesTrait2.tabUnpaid, 1)).
      foreach {
        case (param, expected) =>
          get(f"/${StudentDuesServlet.studentDues}/${year}/${month}", List((StudentDuesTrait2.tab, param)), headerAuthentication ++ headerAccept) {
            status.should(equal(200))
            val result = objectMapper.readValue[List[StudentDuesModel]](body)
            result.length.should(equal(expected))
          }
      }
  }

  test(f"test summary 2") {
    val sum = studentModel.flatMap(_.currentDuesAmount).get + studentModel4.flatMap(_.currentDuesAmount).get
    getEachTemplate[StudentDuesSummaryModel](null,
      _ => f"/${StudentDuesServlet.studentDues}/${year}/${month}/${StudentDuesTrait2.summary}",
      (a, b) => b.allSum == Option(sum) && b.completeSum == studentModel.flatMap(_.currentDuesAmount) && b.incompleteSum.orElse(Option(0)) == studentModel4.flatMap(_.currentDuesAmount),
      _ => None)
  }
}
