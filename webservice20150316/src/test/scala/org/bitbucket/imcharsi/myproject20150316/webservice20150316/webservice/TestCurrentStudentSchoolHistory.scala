/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.webservice

import org.bitbucket.imcharsi.myproject20150316.model20150316._
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao._
import org.scalatra.ScalatraServlet

/**
 * Created by i on 4/3/15.
 */
class TestCurrentStudentSchoolHistory extends AbstractTest {
  override val servletList: Map[Class[_ <: ScalatraServlet], String] = Map((classOf[StudentServlet] -> f"/${StudentServlet.student}/*"), (classOf[SchoolServlet] -> f"/${SchoolServlet.school}/*"), (classOf[StudentDuesServlet] -> f"/${StudentDuesServlet.studentDues}/*"))
  override val schemaList: List[DriverSettings.driver.SchemaDescription] = List(StudentTable.schema, StudentContactTable.schema, SchoolTable.schema, StudentSchoolHistoryTable.schema, StudentDuesTable.schema, LoginUserTable.schema)

  private var studentModel: Option[StudentModel] = None
  private var schoolModel: Option[SchoolModel] = None
  private var studentSchoolHistoryModel: Option[StudentSchoolHistoryModel] = None
  private var studentSchoolHistoryModel2: Option[StudentSchoolHistoryModel] = None

  // student model, school model 을 한개 만든다.
  // student school history 을 한개 만든다.
  // student model 을 다시 읽어서, currentStudentSchoolHistoryId 와 currentStudentSchoolHistory 가 None 이라는 것을 확인한다.
  // 학교이름으로 검색하여 결과가 하나도 나오지 않음을 확인한다. 학생도 있고 재학이력도 있지만, 현재재학이력이 아니기 때문이다.
  // 학생목록을 조회하면서, 학교 id 를 지정하여, 결과가 하나도 없음을 확인한다.
  // student school history 의 currentStudentSchoolHistory 를 참으로 바꿔서 put 으로 저장한다.
  // student model 을 다시 읽어서, currentStudentSchoolHistoryId 와 currentStudentSchoolHistory 가 None 이 아니라는 것을 확인한다.
  // 학교이름으로 검색하여 결과가 나오는 것을 확인한다. 현재재학이력이 있다.
  // 학생목록을 조회하면서, 학교 id 를 지정하여, 결과가 있음을 확인한다.
  // 잘못된 학교이름으로 검색하여 결과가 나오지 않음을 확인한다.
  // student school history 의 currentStudentSchoolHistory 를 거짓으로 바꿔서 put 으로 저장한다.
  // student model 을 다시 읽어서, currentStudentSchoolHistoryId 와 currentStudentSchoolHistory 가 None 이라는 것을 확인한다.
  // post 를 사용해서 현재재학이력 바꾸기를 시도한다.
  // 학생기본정보가 post 로 추가된 재학이력을 현재재학이력으로 가리키고 있는지 확인한다.
  // 처음 만들었던 재학이력은 더이상 현재재학이력이 아니라는 것을 처음 만들었던 재학이력 조회를 통해 확인한다.

  test("prepare StudentModel") {
    val model = StudentModelEx(None, None, Some("#1"), None, None, None, None, None)
    postEachTemplate[StudentModel](model, model => f"/${StudentServlet.student}",
      (a, b) => a.copy(id = b.id, version = b.version) == b,
      model => studentModel = Option(model))
    val model2 = SchoolModel(None, None, Some("#2"), None)
    postEachTemplate[SchoolModel](model2, model => f"/${SchoolServlet.school}",
      (a, b) => a.copy(id = b.id, version = b.version) == b,
      model => schoolModel = Option(model))
  }

  test("prepare StudentSchoolHistoryModel") {
    val model = StudentSchoolHistoryModelEx(None, None, studentModel.flatMap(_.id), schoolModel.flatMap(_.id), None, None, None)
    postEachTemplate[StudentSchoolHistoryModel](model,
      model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentSchoolHistoryTrait.studentSchoolHistory}",
      (a, b) => a.copy(id = b.id, version = b.version, currentStudentSchoolHistory = Some(false)) == b.copy(student = None, school = None) && b.student.isDefined,
      model => studentSchoolHistoryModel = Option(model))
  }

  test("test StudentModel's currentStudentSchoolHistoryId, currentStudentSchoolHistory 1") {
    studentModel.foreach { model =>
      getEachTemplate[StudentModel](model,
        model => f"/${StudentServlet.student}/${model.id.get}",
        (a, b) => a == b && b.currentStudentSchoolHistory.isEmpty && b.currentStudentSchoolHistoryId.isEmpty,
        model => studentModel = Option(model))
    }
  }

  test(f"GET all  /${StudentServlet.student} school name(false)") {
    get(f"/${StudentServlet.student}", List((StudentTrait.paramSearch, schoolModel.flatMap(_.name).get)), headerAuthentication ++ headerAccept) {
      status.should(equal(200))
      objectMapper.readValue[List[StudentModel]](body).should(equal(Nil))
    }
  }

  test(f"GET all  /${StudentServlet.student} schoolId 1") {
    get(f"/${StudentServlet.student}", List((StudentTrait.paramSchoolId, schoolModel.flatMap(_.id).map(_.toString).get)), headerAuthentication ++ headerAccept) {
      status.should(equal(200))
      objectMapper.readValue[List[StudentModel]](body).should(equal(Nil))
    }
  }

  test("modify StudentSchoolHistoryModel's currentStudentSchoolHistory 1") {
    val model = studentSchoolHistoryModel.map(_.copy(currentStudentSchoolHistory = Some(true)))
    model.foreach { model =>
      putEachTemplate[StudentSchoolHistoryModel](model,
        model => f"${StudentServlet.student}/${model.studentId.get}/${StudentSchoolHistoryTrait.studentSchoolHistory}/${model.id.get}")
    }
    model.foreach { model =>
      getEachTemplate[StudentSchoolHistoryModel](model,
        model => f"${StudentServlet.student}/${model.studentId.get}/${StudentSchoolHistoryTrait.studentSchoolHistory}/${model.id.get}",
        (a, b) => a.modifyVersion().copy(student = None) == b.copy(student = None), model => studentSchoolHistoryModel = Option(model))
    }
  }

  test("test StudentModel's currentStudentSchoolHistoryId, currentStudentSchoolHistory 2") {
    studentModel.foreach { model =>
      getEachTemplate[StudentModel](model,
        model => f"/${StudentServlet.student}/${model.id.get}",
        (a, b) => a.modifyVersion().copy(currentStudentSchoolHistoryId = studentSchoolHistoryModel.flatMap(_.id)) == b.copy(currentStudentSchoolHistory = None) && b.currentStudentSchoolHistory.isDefined, // currentStudentSchoolHistoryId 의 값이 존재한다는 것을 명시적으로 확인할 필요는 없는데, 의미상 포함되어 있기 때문이다.
        model => studentModel = Option(model))
    }
  }

  test(f"GET all  /${StudentServlet.student} school name(true)") { // 검색조건으로 학교이름을 써도 검색이 되어야 한다.
    get(f"/${StudentServlet.student}", List((StudentTrait.paramSearch, schoolModel.flatMap(_.name).get)), headerAuthentication ++ headerAccept) {
      status.should(equal(200))
      objectMapper.readValue[List[StudentModel]](body).flatMap(_.currentStudentSchoolHistory).flatMap(_.school).map(_.name).should(equal(List(studentModel).map(_.flatMap(_.currentStudentSchoolHistory).flatMap(_.school).flatMap(_.name))))
    }
  }

  test(f"GET all  /${StudentServlet.student} schoolId 2") {
    get(f"/${StudentServlet.student}", List((StudentTrait.paramSchoolId, schoolModel.flatMap(_.id).map(_.toString).get)), headerAuthentication ++ headerAccept) {
      status.should(equal(200))
      objectMapper.readValue[List[StudentModel]](body).flatMap(_.currentStudentSchoolHistory).flatMap(_.school).map(_.name).should(equal(List(studentModel).map(_.flatMap(_.currentStudentSchoolHistory).flatMap(_.school).flatMap(_.name))))
    }
  }

  test(f"GET all  /${StudentServlet.student} school name(false) 2") {
    get(f"/${StudentServlet.student}", List((StudentTrait.paramSearch, schoolModel.flatMap(_.name).map(_ + "1").get)), headerAuthentication ++ headerAccept) {
      status.should(equal(200))
      objectMapper.readValue[List[StudentModel]](body).should(equal(Nil))
    }
  }

  test("modify StudentSchoolHistoryModel's currentStudentSchoolHistory 2") {
    val model = studentSchoolHistoryModel.map(_.copy(currentStudentSchoolHistory = Some(false)))
    model.foreach { model =>
      putEachTemplate[StudentSchoolHistoryModel](model,
        model => f"${StudentServlet.student}/${model.studentId.get}/${StudentSchoolHistoryTrait.studentSchoolHistory}/${model.id.get}")
    }
    model.foreach { model =>
      getEachTemplate[StudentSchoolHistoryModel](model,
        model => f"${StudentServlet.student}/${model.studentId.get}/${StudentSchoolHistoryTrait.studentSchoolHistory}/${model.id.get}",
        (a, b) => a.modifyVersion().copy(student = None) == b.copy(student = None), model => studentSchoolHistoryModel = Option(model))
    }
  }

  test("test StudentModel's currentStudentSchoolHistoryId, currentStudentSchoolHistory 3") {
    studentModel.foreach { model =>
      getEachTemplate[StudentModel](model,
        model => f"/${StudentServlet.student}/${model.id.get}",
        (a, b) => a.modifyVersion().copy(currentStudentSchoolHistory = None, currentStudentSchoolHistoryId = None) == b && b.currentStudentSchoolHistory.isEmpty && b.currentStudentSchoolHistoryId.isEmpty,
        model => studentModel = Option(model))
    }
  }

  test("prepare StudentSchoolHistoryModel 2") {
    val model = StudentSchoolHistoryModelEx(None, None, studentModel.flatMap(_.id), None, None, None, None).copy(currentStudentSchoolHistory = Some(true))
    postEachTemplate[StudentSchoolHistoryModel](model,
      model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentSchoolHistoryTrait.studentSchoolHistory}",
      (a, b) => a.copy(id = b.id, version = b.version, currentStudentSchoolHistory = Some(true)) == b.copy(student = None) && b.student.isDefined,
      model => studentSchoolHistoryModel2 = Option(model))
  }

  test("test StudentModel's currentStudentSchoolHistoryId, currentStudentSchoolHistory 4") {
    studentModel.foreach { model =>
      getEachTemplate[StudentModel](model,
        model => f"/${StudentServlet.student}/${model.id.get}",
        (a, b) => a.modifyVersion().copy(currentStudentSchoolHistoryId = studentSchoolHistoryModel2.flatMap(_.id)) == b.copy(currentStudentSchoolHistory = None) && b.currentStudentSchoolHistory.isDefined,
        model => studentModel = Option(model))
    }
  }

  // 이전의 재학이력의 currentStudentSchoolHistory 는 거짓으로 조회되어야 한다. todo 이전의 재학이력의 version 도 바뀌어야 할까. 생각해보기.
  test("test StudentSchoolHistoryModel's currentStudentSchoolHistory") {
    studentSchoolHistoryModel.foreach { model =>
      getEachTemplate[StudentSchoolHistoryModel](model,
        model => f"${StudentServlet.student}/${model.studentId.get}/${StudentSchoolHistoryTrait.studentSchoolHistory}/${model.id.get}",
        (a, b) => a.copy(student = None, currentStudentSchoolHistory = Some(false)) == b.copy(student = None), model => studentSchoolHistoryModel = Option(model))
    }
  }
}
