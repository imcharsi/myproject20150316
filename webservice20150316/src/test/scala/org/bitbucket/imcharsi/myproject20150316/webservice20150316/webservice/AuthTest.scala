/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.webservice

import java.util.logging.Logger

import org.apache.commons.codec.binary.Base64
import org.bitbucket.imcharsi.myproject20150316.model20150316.LoginUserModel
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driver._
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.{ DriverSettings, LoginUserTable }
import org.scalatra.ScalatraServlet

/**
 * Created by i on 4/7/15.
 */
class AuthTest extends AbstractTest {
  override val servletList: Map[Class[_ <: ScalatraServlet], String] = Map((classOf[AuthTestServlet], "/auth/*"))
  override val schemaList: List[SchemaDescription] = List(LoginUserTable.schema)
  private val logger = Logger.getLogger(getClass.getName)
  private var loginUserModel: Option[LoginUserModel] = None

  test("auth") {
    DriverSettings.database.withTransaction { implicit s =>
      loginUserModel = Option(LoginUserModel(None, Option(1), Option("hey"), Option("there"), Option("#1"), Option(false))).flatMap { model =>
        val id = LoginUserTable.returning(LoginUserTable.map(_.id)).insert(model)
        LoginUserTable.filter(_.id === id).firstOption
      }
    }
    // http://stackoverflow.com/questions/1968416/how-to-do-http-authentication-in-android
    get("/auth", Nil, Map(("Authorization", "Basic " + Base64.encodeBase64String("hey:there".getBytes)))) {
      status.should(equal(401))
    }
    DriverSettings.database.withTransaction { implicit s =>
      loginUserModel.map(_.copy(available = Option(true))).flatMap { model =>
        LoginUserTable.filter(_.id === model.id).update(model)
        LoginUserTable.filter(_.id === model.id).firstOption
      }
    }
    get("/auth", Nil, Map(("Authorization", "Basic " + Base64.encodeBase64String("hey:there".getBytes)))) {
      status.should(equal(200))
    }
    DriverSettings.database.withTransaction { implicit s =>
      loginUserModel.map(_.copy(password = Option("guys"))).flatMap { model =>
        LoginUserTable.filter(_.id === model.id).update(model)
        LoginUserTable.filter(_.id === model.id).firstOption
      }
    }
    get("/auth", Nil, Map(("Authorization", "Basic " + Base64.encodeBase64String("hey:there".getBytes)))) {
      status.should(equal(401))
    }
  }
}
