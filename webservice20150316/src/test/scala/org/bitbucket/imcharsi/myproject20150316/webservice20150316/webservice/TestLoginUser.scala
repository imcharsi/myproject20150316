/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.webservice

import org.bitbucket.imcharsi.myproject20150316.model20150316.{ LoginUserModel, Version }
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driver._
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.{ DriverSettings, LoginUserTable }
import org.scalatra.ScalatraServlet

/**
 * Created by i on 4/7/15.
 */
class TestLoginUser extends AbstractTest {
  override val servletList: Map[Class[_ <: ScalatraServlet], String] = Map((classOf[LoginUserServlet], f"/${LoginUserServlet.loginUser}/*"))
  override val schemaList: List[SchemaDescription] = List(LoginUserTable.schema)
  private var loginUserModel: Option[LoginUserModel] = None
  private var loginUserModel2: Option[LoginUserModel] = None
  private var loginUserModelList: List[LoginUserModel] = Nil

  test(f"POST  /${LoginUserServlet.loginUser}") {
    val model = LoginUserModel(None, None, Some("hey"), Some("password"), Some("#1"), Some(true))
    postEachTemplate[LoginUserModel](model, _ => f"/${LoginUserServlet.loginUser}", (a, b) => a.copy(id = b.id, version = b.version) == b && b.version.isDefined, a => loginUserModel = Option(a))
  }

  test(f"GET each  /${LoginUserServlet.loginUser}/:${LoginUserServlet.loginUserId}") {
    loginUserModel.foreach { model =>
      getEachTemplate[LoginUserModel](model,
        model => f"/${LoginUserServlet.loginUser}/${model.id.get}",
        (a, b) => a == b,
        a => loginUserModel = Option(a))
    }
  }

  test(f"GET all  /${LoginUserServlet.loginUser}") {
    getAllTemplate[LoginUserModel](_ => f"/${LoginUserServlet.loginUser}",
      // test 가 성공하기 위해서 미리 만들어둔 login user model 은 test 에서 제외한다. 이 test 자체가 이미 인증기능을 쓰고 있기 때문에,
      // test 를 수행하려면 인증을 거쳐야 하고 id 가 만들어져 있어야 한다.
      a => a.filter(_.userName == Option("hey")).map(m => Option(m)) == List(loginUserModel),
      a => loginUserModelList = a)
  }

  test(f"PUT  /${LoginUserServlet.loginUser}/:${LoginUserServlet.loginUserId}") {
    val sampleModel = loginUserModel.map(_.copy(userName = Some("bye")))
    sampleModel.foreach { model =>
      putEachTemplate[LoginUserModel](model, t => f"/${LoginUserServlet.loginUser}/${t.id.get}")
    }
    sampleModel.foreach { model =>
      getEachTemplate[LoginUserModel](model,
        model => f"/${LoginUserServlet.loginUser}/${model.id.get}",
        (a, b) => a.copy(version = a.version.map(_ + 1)) == b,
        a => loginUserModel = Option(a))
    }
  }

  test(f"POST  /${LoginUserServlet.loginUser} (2)") {
    val model = LoginUserModel(None, None, Some("you"), Some("password"), Some("#1"), Some(true))
    postEachTemplate[LoginUserModel](model, _ => f"/${LoginUserServlet.loginUser}", (a, b) => a.copy(id = b.id, version = b.version) == b && b.version.isDefined, a => loginUserModel2 = Option(a))
  }

  test(f"DELETE  /${LoginUserServlet.loginUser} (2)") {
    loginUserModel2.foreach { model =>
      deleteEachTemplate[LoginUserModel](model, x => f"/${LoginUserServlet.loginUser}/${x.id.get}", x => List((Version.version, x.version.map(_.toString).get)), _ => None)
    }
  }

  test(f"PUT  /${LoginUserServlet.loginUser} fail") {
    DriverSettings.database.withTransaction { implicit s =>
      LoginUserTable.filter(_.userName === testLoginUserModel.userName).firstOption
    }.map(_.copy(available = Option(false))).foreach { model =>
      // 자기가 자기의 인증정보를 비사용으로 바꿀수 없어야 한다.
      put(f"/${LoginUserServlet.loginUser}/${model.id.get}", objectMapper.writeValueAsString(model), headerAuthentication ++ headerContentType) {
        status.should(equal(500))
      }
    }
  }

  test(f"DELETE  /${LoginUserServlet.loginUser} fail") {
    DriverSettings.database.withTransaction { implicit s =>
      LoginUserTable.filter(_.userName === testLoginUserModel.userName).firstOption
    }.map(_.copy(available = Option(false))).foreach { model =>
      // 자기가 자신의 인증정보를 지울수 없어야 한다.
      delete(f"/${LoginUserServlet.loginUser}/${model.id.get}", List((Version.version, model.version.map(_.toString).get)), headerAuthentication ++ headerContentType) {
        status.should(equal(500))
      }
    }
  }

  test(f"POST  /${LoginUserServlet.loginUser} fail") {
    List(
      LoginUserModel(None, None, Some("hey:"), Some("password"), Some("#1"), Some(true)),
      LoginUserModel(None, None, Some("hey"), Some(""), Some("#1"), Some(true))).foreach { model =>
        post(f"/${LoginUserServlet.loginUser}", objectMapper.writeValueAsString(model), headerAuthentication ++ headerContentType ++ headerAccept) {
          status.should(equal(500))
        }
      }
  }
}
