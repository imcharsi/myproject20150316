/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.webservice

import java.text.SimpleDateFormat
import java.util.{ Calendar, Locale, TimeZone }

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings
import org.scalatest.{ BeforeAndAfterAll, FunSuiteLike }
import org.scalatra.ScalatraServlet
import org.scalatra.test.scalatest.ScalatraFunSuite

/**
 * Created by i on 3/19/15.
 */
abstract class AbstractTest extends ScalatraFunSuite with FunSuiteLike with BeforeAndAfterAll with AuthUtilTrait {
  protected val objectMapper: ObjectMapper with ScalaObjectMapper = new ObjectMapper() with ScalaObjectMapper
  objectMapper.registerModule(DefaultScalaModule)
  private val dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
  objectMapper.setDateFormat(dateFormat)
  protected val headerContentType: Map[String, String] = Map(("Content-Type" -> "application/json"))
  protected val headerAccept: Map[String, String] = Map(("Accept" -> "application/json"))
  protected var headerAuthentication: Map[String, String] = Map.empty

  val servletList: Map[Class[_ <: ScalatraServlet], String]
  val schemaList: List[DriverSettings.driver.SchemaDescription]
  val calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Seoul"), Locale.getDefault)

  protected override def beforeAll(): Unit = {
    super.beforeAll()
    DriverSettings.createTables(schemaList)
    servletList.foreach { case (c, u) => addServlet(c, u) }

    calendar.set(Calendar.HOUR_OF_DAY, 0) // 이와 같이 시각을 정확히 맞추지 않으면 검사에서 불일치 판정을 한다.
    calendar.set(Calendar.MINUTE, 0)
    calendar.set(Calendar.SECOND, 0)
    calendar.set(Calendar.MILLISECOND, 0)
    initTestLogin()
    headerAuthentication = Map(generateTestAuthenticationHeader())
  }

  protected override def afterAll(): Unit = {
    DriverSettings.dropTables(schemaList)
    super.afterAll()
  }

  def postEachTemplate[T](model: T, urlMaker: T => String, assertion: (T, T) => Boolean, resultBackup: T => Unit)(implicit t: Manifest[T]): Unit = {
    post(urlMaker(model), objectMapper.writeValueAsString(model), headerAuthentication ++ headerContentType ++ headerAccept) {
      status.should(equal(200))
      //              parse(body).extractOpt[StudentModel] // 이유를 모르겠는데, 어느 때부터인가 .extract 가 제대로 동작하지 않을 때도 있다. 왜? 그런데, scalatra servlet 안에서는 또 잘 동작한다.
      val result = objectMapper.readValue[T](body)
      assertion(model, result).should(equal(true))
      resultBackup(result)
    }
  }

  def getEachTemplate[T](model: T, urlMaker: T => String, assertion: (T, T) => Boolean, resultBackup: T => Unit)(implicit t: Manifest[T]): Unit = {
    get(urlMaker(model), Nil, headerAuthentication ++ headerAccept) {
      status.should(equal(200))
      val result = objectMapper.readValue[T](body)
      assertion(model, result).should(equal(true))
      resultBackup(result)
    }
  }

  def getAllTemplate[T](urlMaker: Unit => String, assertion: List[T] => Boolean, resultBackup: List[T] => Unit)(implicit t: Manifest[T]): Unit = {
    get(urlMaker(), Nil, headerAuthentication ++ headerAccept) {
      status.should(equal(200))
      val result = objectMapper.readValue[List[T]](body)
      assertion(result).should(equal(true))
      resultBackup(result)
    }
  }

  def putEachTemplate[T](model: T, urlMaker: T => String)(implicit t: Manifest[T]): Unit = {
    put(urlMaker(model), objectMapper.writeValueAsString(model), headerAuthentication ++ headerContentType) {
      status.should(equal(200))
    }
  }

  def deleteEachTemplate[T](model: T, urlMaker: T => String, versionMaker: T => List[(String, String)], resultBackup: Unit => Unit)(implicit t: Manifest[T]): Unit = {
    delete(urlMaker(model), versionMaker(model), headerAuthentication) {
      status.should(equal(200))
      resultBackup()
    }
  }

  def deleteEachTemplate404[T](model: T, urlMaker: T => String, versionMaker: T => List[(String, String)])(implicit t: Manifest[T]): Unit = {
    delete(urlMaker(model), versionMaker(model), headerAuthentication) {
      status.should(equal(404))
    }
  }
}
