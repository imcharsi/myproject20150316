/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.webservice

import org.bitbucket.imcharsi.myproject20150316.model20150316.{ StudentContactModel, StudentContactModelEx, StudentModel, StudentModelEx }
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._
import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao._
import org.scalatra.ScalatraServlet

/**
 * Created by i on 4/3/15.
 */
class TestCurrentStudentContact extends AbstractTest {
  override val servletList: Map[Class[_ <: ScalatraServlet], String] = Map((classOf[StudentServlet] -> f"/${StudentServlet.student}/*"), (classOf[SchoolServlet] -> f"/${SchoolServlet.school}/*"), (classOf[StudentDuesServlet] -> f"/${StudentDuesServlet.studentDues}/*"))
  override val schemaList: List[DriverSettings.driver.SchemaDescription] = List(StudentTable.schema, StudentContactTable.schema, SchoolTable.schema, StudentSchoolHistoryTable.schema, StudentDuesTable.schema, LoginUserTable.schema)

  private var studentModel: Option[StudentModel] = None
  private var studentContactModel: Option[StudentContactModel] = None
  private var studentContactModel2: Option[StudentContactModel] = None

  // 1. student model 을 한개 만든다.
  // 2. student contact model 을 한개 만든다.
  // 3. student model 을 다시 읽어서, currentStudentContactId 와 currentStudentContact 가 None 이라는 것을 확인한다.
  // 4. student contact model 의 currentStudentContact 를 참으로 바꿔서 put 으로 저장한다.
  // 5. student model 을 다시 읽어서, currentStudentContactId 와 currentStudentContact 가 None 이 아니라는 것을 확인한다.
  // 6. student contact model 의 currentStudentContact 를 거짓으로 바꿔서 put 으로 저장한다.
  // 7. student model 을 다시 읽어서, currentStudentContactId 와 currentStudentContact 가 None 이라는 것을 확인한다.
  // 8. student contact model 의 currentStudentContact 를 참으로 하여 post 를 했을때, student model 의 currentStudentContactId 가 같이 바뀌는지 확인한다.

  test("prepare StudentModel") {
    val model = StudentModelEx(None, None, Some("#1"), None, None, None, None, None)
    postEachTemplate[StudentModel](model, model => f"/${StudentServlet.student}",
      (a, b) => a.copy(id = b.id, version = b.version) == b,
      model => studentModel = Option(model))
  }

  test("prepare StudentContactModel") {
    val model = StudentContactModelEx(None, None, None, None, studentModel.flatMap(_.id), Some(true)) // 처음부터 유효한 연락처로 하기.
    postEachTemplate[StudentContactModel](model,
      model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentContactTrait.studentContact}",
      (a, b) => a.copy(id = b.id, version = b.version, currentStudentContact = Some(false)) == b.copy(student = None) && b.student.isDefined,
      model => studentContactModel = Option(model))
  }

  test("test StudentModel's currentStudentContactId, currentStudentContact 1") {
    studentModel.foreach { model =>
      getEachTemplate[StudentModel](model,
        model => f"/${StudentServlet.student}/${model.id.get}",
        (a, b) => a == b && b.currentStudentContact.isEmpty && b.currentStudentContactId.isEmpty,
        model => studentModel = Option(model))
    }
  }

  test("modify StudentContactModel's currentStudentContact 1") {
    val model = studentContactModel.map(_.copy(currentStudentContact = Some(true)))
    model.foreach { model =>
      putEachTemplate[StudentContactModel](model,
        model => f"${StudentServlet.student}/${model.studentId.get}/${StudentContactTrait.studentContact}/${model.id.get}")
    }
    model.foreach { model =>
      getEachTemplate[StudentContactModel](model,
        model => f"${StudentServlet.student}/${model.studentId.get}/${StudentContactTrait.studentContact}/${model.id.get}",
        (a, b) => a.modifyVersion().copy(student = None) == b.copy(student = None), model => studentContactModel = Option(model))
    }
  }

  test("test StudentModel's currentStudentContactId, currentStudentContact 2") {
    studentModel.foreach { model =>
      getEachTemplate[StudentModel](model,
        model => f"/${StudentServlet.student}/${model.id.get}",
        (a, b) => a.modifyVersion().copy(currentStudentContactId = studentContactModel.flatMap(_.id)) == b.copy(currentStudentContact = None) && b.currentStudentContact.isDefined, // currentStudentContactId 의 값이 존재한다는 것을 명시적으로 확인할 필요는 없는데, 의미상 포함되어 있기 때문이다.
        model => studentModel = Option(model))
    }
  }

  test("modify StudentContactModel's currentStudentContact 2") {
    val model = studentContactModel.map(_.copy(currentStudentContact = Some(false)))
    model.foreach { model =>
      putEachTemplate[StudentContactModel](model,
        model => f"${StudentServlet.student}/${model.studentId.get}/${StudentContactTrait.studentContact}/${model.id.get}")
    }
    model.foreach { model =>
      getEachTemplate[StudentContactModel](model,
        model => f"${StudentServlet.student}/${model.studentId.get}/${StudentContactTrait.studentContact}/${model.id.get}",
        (a, b) => a.modifyVersion().copy(student = None) == b.copy(student = None), model => studentContactModel = Option(model))
    }
  }

  test("test StudentModel's currentStudentContactId, currentStudentContact 3") {
    studentModel.foreach { model =>
      getEachTemplate[StudentModel](model,
        model => f"/${StudentServlet.student}/${model.id.get}",
        (a, b) => a.modifyVersion().copy(currentStudentContact = None, currentStudentContactId = None) == b && b.currentStudentContact.isEmpty && b.currentStudentContactId.isEmpty,
        model => studentModel = Option(model))
    }
  }

  test("prepare StudentContactModel 2") {
    val model = StudentContactModelEx(None, None, None, None, studentModel.flatMap(_.id), Some(true)).copy(currentStudentContact = Some(true)) // 처음부터 유효한 연락처로, 현재연락처로 하기.
    postEachTemplate[StudentContactModel](model,
      model => f"/${StudentServlet.student}/${model.studentId.get}/${StudentContactTrait.studentContact}",
      (a, b) => a.copy(id = b.id, version = b.version, currentStudentContact = Some(true)) == b.copy(student = None) && b.student.isDefined,
      model => studentContactModel2 = Option(model))
  }

  test("test StudentModel's currentStudentContactId, currentStudentContact 4") {
    studentModel.foreach { model =>
      getEachTemplate[StudentModel](model,
        model => f"/${StudentServlet.student}/${model.id.get}",
        (a, b) => a.modifyVersion().copy(currentStudentContactId = studentContactModel2.flatMap(_.id)) == b.copy(currentStudentContact = None) && b.currentStudentContact.isDefined,
        model => studentModel = Option(model))
    }
  }

  // 이전의 연락처의 currentStudentContact 는 거짓으로 조회되어야 한다. todo 이전의 연락처의 version 도 바뀌어야 할까. 생각해보기.
  test("test StudentContactModel's currentStudentContact") {
    studentContactModel.foreach { model =>
      getEachTemplate[StudentContactModel](model,
        model => f"${StudentServlet.student}/${model.studentId.get}/${StudentContactTrait.studentContact}/${model.id.get}",
        (a, b) => a.copy(student = None, currentStudentContact = Some(false)) == b.copy(student = None), model => studentContactModel = Option(model))
    }
  }
}
