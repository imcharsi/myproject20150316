/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao

import java.util.logging.Logger

import DriverSettings._
import org.scalatest.FunSuite

import scala.util.Try

/**
 * Created by i on 4/12/15.
 */
class TestDDL extends FunSuite {
  def createTables(schemaList: List[driver.SchemaDescription]): List[String] = {
    var ddlList: List[String] = Nil
    schemaList.foreach { schema =>
      Try {
        schema.createStatements.filterNot(_.toLowerCase.startsWith("alter table")).foreach { ddl =>
          ddlList = ddl :: ddlList
        }
      }
    }
    schemaList.foreach { schema =>
      Try {
        schema.createStatements.filter(_.toLowerCase.startsWith("alter table")).foreach { ddl =>
          ddlList = ddl :: ddlList
        }
      }
    }
    ddlList.reverse
  }

  test("ddl gen") {
    createTables(DriverSettings.schemaList).foreach { ddl => println(ddl) }
  }
}
