/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.webservice

import java.text.SimpleDateFormat
import java.util.logging.Logger
import java.util.{ Calendar, Date, TimeZone }

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import org.bitbucket.imcharsi.myproject20150316.model20150316.StudentSchoolHistoryModel
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.scalatest.{ BeforeAndAfterAll, FunSuite }

/**
 * Created by i on 3/28/15.
 */
class JsonTest extends FunSuite with BeforeAndAfterAll {
  var objectMapper: ObjectMapper with ScalaObjectMapper = null
  var calendar: Calendar = null
  implicit val formats: Formats = DefaultFormats.lossless
  val logger: Logger = Logger.getLogger(getClass.getName)

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    objectMapper = new ObjectMapper() with ScalaObjectMapper
    objectMapper.registerModule(DefaultScalaModule)
    val dateFormat: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
    objectMapper.setDateFormat(dateFormat)
    calendar = Calendar.getInstance()
  }

  test("hi") {
    val date = Some(new Date(calendar.getTimeInMillis))
    val model = StudentSchoolHistoryModel(None, None, None, None, date, None, None, None, None, None)
    logger.info(model.toString)
    val json = objectMapper.writeValueAsString(model)
    logger.info(json)
    val model2 = parse(json).extract[StudentSchoolHistoryModel]
    logger.info(model2.toString)
    val model3 = objectMapper.readValue[StudentSchoolHistoryModel](json)
    logger.info(model3.toString)

    //    val timestamp = new Timestamp(calendar.getTimeInMillis)
    //    logger.info(timestamp.toString)
    //    val json2 = objectMapper.writeValueAsString(timestamp)
    //    logger.info(json2)
    //    val timestamp2 = parse(json2).extract[Timestamp]
    //    logger.info(timestamp2.toString)
  }

}
