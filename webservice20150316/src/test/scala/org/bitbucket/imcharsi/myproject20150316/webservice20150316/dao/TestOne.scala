/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao

import java.util.logging.Logger

import org.bitbucket.imcharsi.myproject20150316.webservice20150316.dao.DriverSettings.driverSimple._
import org.scalatest.{ BeforeAndAfterAll, FunSuite }

import scala.util.Try

/**
 * Created by i on 3/19/15.
 */
class TestOne extends FunSuite with BeforeAndAfterAll {
  val list = List(StudentTable.schema, SchoolTable.schema, StudentSchoolHistoryTable.schema)

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    implicit val slickSession = DriverSettings.database.createSession()
    list.foreach {
      x =>
        Try {
          //          x.create
        }
    }
    slickSession.close()
  }

  override protected def afterAll(): Unit = {
    implicit val slickSession = DriverSettings.database.createSession()
    list.reverse.foreach {
      x =>
        Try {
          //          x.drop
        }
    }
    slickSession.close()
    super.afterAll()
  }

  test("hi") {
    implicit val slickSession = DriverSettings.database.createSession()
    Try {
      list.map(_.createStatements).flatMap(_.toList).filter(_.toLowerCase.startsWith("alter table")).foreach(s => Logger.getGlobal.info(s))
      list.map(_.dropStatements).flatMap(_.toList).foreach(s => Logger.getGlobal.info(s))

      //      var studentModel = StudentModel(None, Some("stu #1"), None)
      //      studentModel = studentModel.copy(id = StudentTable.returning(StudentTable.map(_.id)).insert(studentModel))
      //      var studentContactModel = StudentContactModelEx(None, Some("1234"), Some("#1234"), studentModel.id)
      //      studentContactModel = studentContactModel.copy(id = StudentContactTable.returning(StudentContactTable.map(_.id)).insert(studentContactModel))
      //      val list1 = StudentContactTable.
      //        joinLeft(StudentTable).on(_.studentId === _.id).
      //        list.map {
      //          case (studentContent, student) => studentContent.copy(student = student)
      //        }
      //      Logger.getGlobal.info(studentModel.toString)
      //      Logger.getGlobal.info(studentContactModel.toString)
    }
  }
}
