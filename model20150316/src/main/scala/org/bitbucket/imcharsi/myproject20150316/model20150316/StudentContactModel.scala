/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.model20150316

/**
 * Created by i on 2015-03-16.
 */
case class StudentContactModel(id: Option[Int], override val version: Option[Int], callNumber: Option[String], address: Option[String], studentId: Option[Int],
    validity: Option[Boolean], // 이 연락처가 유효한 연락처인지 아닌지 구분한다.
    currentStudentContact: Option[Boolean], // 이 열도 계산열이다. 실제로 쓸때 현재연락처를 student model 을 거쳐서 편집하기에 번거롭다. 그래서 web service 에서 이 항목을 인식하도록 한다.
    student: Option[StudentModel]) extends Version[StudentContactModel] {
  // 연락처가 바뀔때 있던 연락처 기록을 편집해서 쓸 것인지, 아니면 새 연락처 기록에, 바뀐 연락처를 쓰고, 기존 연락처 기록은 보존용도로 놔둘것인지는 쓰는 사람이 판단할 문제다.
  def this() {
    this(None, None, None, None, None, None, None, None)
  }

  override def modifyVersion(): StudentContactModel = copy(version = version.map(_ + 1))
}

object StudentContactModelEx {
  def apply(id: Option[Int], version: Option[Int], callNumber: Option[String], address: Option[String], studentId: Option[Int], validity: Option[Boolean]): StudentContactModel =
    StudentContactModel(id, version, callNumber, address, studentId, validity, Option(false), None)

  def unapply(s: StudentContactModel): Option[(Option[Int], Option[Int], Option[String], Option[String], Option[Int], Option[Boolean])] =
    Some((s.id, s.version, s.callNumber, s.address, s.studentId, s.validity))
}
