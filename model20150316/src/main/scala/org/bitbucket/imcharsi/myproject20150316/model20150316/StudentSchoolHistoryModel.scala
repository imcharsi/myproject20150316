/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.model20150316

import java.util.Date

/**
 * Created by i on 3/25/15.
 */
case class StudentSchoolHistoryModel(id: Option[Int], override val version: Option[Int],
    studentId: Option[Int],
    // schoolType: Option[Int], // schoolType 은 빼고 해보자. client 입력받을 때 학교유형에 따른 학교선택을 하게 되는데, 이미 입력된 것을 편집할 때는 현재 학교의 학교유형을 참고하여 학교유형을 정하기로 하자.
    schoolId: Option[Int],
    currentDate: Option[Date],
    currentGrade: Option[Int],
    classNumber: Option[Int],
    currentStudentSchoolHistory: Option[Boolean],
    student: Option[StudentModel],
    school: Option[SchoolModel]) extends Version[StudentSchoolHistoryModel] {
  def this() = this(None, None, None, None, None, None, None, None, None, None)

  override def modifyVersion(): StudentSchoolHistoryModel = copy(version = version.map(_ + 1))
}

// currentDate 는, 이 학생의 이 재학이력의 의미는 이 항목에서 입력된 날짜부터 시작해서 유효하다는 뜻이다.
// 원래 currentDate 는 위의 주석에 쓴것처럼 쓸려고 했지만, 사용방법을 바꿨다. 학생기본정보에서 현재재학이력을 지정하도록 한 이상, 날짜는 큰 의미가 없다.
// 단순히 알아보기 쉽게 하는 용도로 쓰자. 이 재학이력이 언제의 재학이력인가를 나타내는 것이다.
// currentGrade 는 현재 학년, classNumber 는 반 번호를 뜻한다.
// schoolType 은 학교유형을 뜻한다. 초등학교/중학교/고등학교가 있다.

object StudentSchoolHistoryModelEx {
  def apply(id: Option[Int], version: Option[Int],
    studentId: Option[Int],
    // schoolType: Option[Int],
    schoolId: Option[Int],
    currentDate: Option[Date],
    currentGrade: Option[Int],
    classNumber: Option[Int]): StudentSchoolHistoryModel =
    StudentSchoolHistoryModel(id, version, studentId, /*schoolType,*/ schoolId, currentDate, currentGrade, classNumber, None, None, None)

  def unapply(m: StudentSchoolHistoryModel): Option[(Option[Int], Option[Int], Option[Int], /*Option[Int],*/ Option[Int], Option[Date], Option[Int], Option[Int])] =
    Some((m.id, m.version, m.studentId, /*m.schoolType,*/ m.schoolId, m.currentDate, m.currentGrade, m.classNumber))
}
