/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.model20150316

/**
 * Created by i on 4/7/15.
 */
case class LoginUserModel(id: Option[Int], override val version: Option[Int], userName: Option[String], password: Option[String], personName: Option[String], available: Option[Boolean]) extends Version[LoginUserModel] {
  def this() = this(None, None, None, None, None, None)

  override def modifyVersion(): LoginUserModel = copy(version = version.map(_ + 1))

  def checkUsername: Boolean = userName.flatMap(x => "^[a-zA-Z0-9_]*$".r.findFirstIn(x)).isDefined

  def checkPassword: Boolean = password.filterNot(_.isEmpty).isDefined
}
