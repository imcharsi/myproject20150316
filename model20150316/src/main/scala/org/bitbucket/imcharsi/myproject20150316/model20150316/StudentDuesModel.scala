/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.model20150316

import java.util.Date

/**
 * Created by i on 3/30/15.
 */
case class StudentDuesModel(id: Option[Int], override val version: Option[Int],
    studentId: Option[Int],
    axisPayDate: Option[Date], // 영어단어를 어떤것을 써야 할지 모르겠다. 이 항목의 취지는, 이 납부이력이 어느 연월의 회비인지 나타내는 것이다.
    // 그런데, 연과 월만 쓰거나, Date 에서 일을 무시하고 쓰거나 두가지 방법이 있는데, Date 를 쓰기로 해보자.
    plannedPayDate: Option[Date], // 이 학생이 이 회비를 언제 내기로 했는지.
    // 그런데, 이 항목과 관련하여 복잡한 문제가 하나 있는데, 이는 마치 회비후불납부와 비슷한 문제라 할 수 있다.
    // 경우에 따라서는 당월 회비를 익월말일에 납부하겠다는 경우도 있을수 있는데, 이것은 마치 후불과도 비슷하다.
    // 이 때 예정납부일을 어떻게 계산할 것인가. 이에 관한 설명은 StudentModel 의 distanceOfPlannedDuesDate 에 썼다.
    actuallyPayDate: Option[Date], // 이 학생이 이 회비를 실제로 언제 냈는지.
    amount: Option[Int], // 이 학생이 내야 할 회비가 얼마인지.
    student: Option[StudentModel]) extends Version[StudentDuesModel] {
  def this() = this(None, None, None, None, None, None, None, None)

  override def modifyVersion(): StudentDuesModel = copy(version = version.map(_ + 1))
}

// 회비가 몇월에 대한 회비인지 구분할 수 있어야 하고, 그 회비를 언제 냈는지 구분할수 있어야 한다. 둘은 실제로 다를 수 있다. 1월 회비를 2월 초에 낼수도 있고, 밀릴수도 있다.
// 회비를 실제로 냈으면 날짜가 null 이 아니고, 아직 안냈으면 null 인 것으로 하자. 회비를 냈든 안냈든 납부이력은 무조건 일괄생성하고 amount 는 내야 할 돈으로 하자.

object StudentDuesModelEx {
  def apply(id: Option[Int], version: Option[Int],
    studentId: Option[Int],
    axisPayDate: Option[Date],
    plannedPayDate: Option[Date],
    actuallyPayDate: Option[Date],
    amount: Option[Int]) =
    StudentDuesModel(id, version, studentId, axisPayDate, plannedPayDate, actuallyPayDate, amount, None)

  def unapply(x: StudentDuesModel): Option[(Option[Int], Option[Int], Option[Int], Option[Date], Option[Date], Option[Date], Option[Int])] =
    Option((x.id, x.version, x.studentId, x.axisPayDate, x.plannedPayDate, x.actuallyPayDate, x.amount))
}
