/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.model20150316

/**
 * Created by i on 2015-03-16.
 */
case class StudentModel(id: Option[Int],
    override val version: Option[Int],
    name: Option[String],
    currentStudentSchoolHistoryId: Option[Int], // 현재 재학이력
    currentStudentContactId: Option[Int], // 이 학생의 기본 연락처. 이 항목이 가리키지 않은 연락처는 사용되지 않는다는 뜻은 아니고, 주로 사용할 연락처는 이 항목으로 가리킨다는 의미이다.
    currentDuesAmount: Option[Int], // 일괄생성에서 사용할 회비액수
    distanceOfPlannedDuesDate: Option[Int], // 이 항목은 복잡한 문제가 있다.
    // StudentDuesModel 에서도 주석을 썼지만, 당월회비를 익월말일에 납부하기로 했다면, 이 항목의 숫자를 일로 보면 익월이 아닌 당월의 일이 납부일로 된다는 말인데,
    // 이는 학원과 학생사이의 계약내용과는 다른 것이다.
    // 예를 들어, 1월 회비를 2월 말에 내기로 했는데, 이 항목의 숫자를 일로 본다면 일괄생성을 할 때, 1월 말로 예정납부일이 정해지는 것이다.
    // 따라서, 이 항목의 값을 기준연월의 1일을 기준으로 예정납부일까지의 기간차이로 보도록 하는 것이 낫겠다.
    // 예를 들어, 이 항목이 60 이고 3월 회비에 대한 예정납부일을 정한다면, 3월 1일 + 30일 + 1일 + 29일 = 4월 29일 이 예정납부일이 되는 것이다.
    // 그리고 이 항목이 0 이면, 3월 1일 + 0일 = 3월 1일이 되는 것이다. 한편, 이 숫자는 무조건 더하기로 다룬다. 따라서 음수로 쓰면 기준연월의 1일보다 이른 날짜가 납부예정일로 되는 것이다.
    // 한편, 적당한 단어를 모르겠다. 거리를 뜻하는 distance 로 대충 쓰자.
    currentAttendingStudent: Option[Boolean], // 이 학생에 관한 납부이력을 일괄생성할지 여부. 쉽게 말해, 학원을 다니고 있는 학생인지 구분.
    remainingDuesAmount: Option[Int], // 계산열. 미납회비 총액.
    currentStudentSchoolHistory: Option[StudentSchoolHistoryModel], // 학생목록을 보여주는 각 tab 에서 추가로 보여줄 내용들이다. 재학이력 tab 에서는 현재 재학이력이 학생기본정보에 같이 보여야 한다.
    currentStudentContact: Option[StudentContactModel] // 연락처 tab 에서는 기본연락처로 지정된 연락처가 학생목록에서 같이 보여야 한다.
    ) extends Version[StudentModel] {
  // Android 에서는 json4s 의 parse(...).extract[...] 와 같이 쓸 수 없다. 이유를 모르겠다. 일단 간편한 방법으로 readValue(...) ᅟ를 쓰자.
  // ScalaObjectMapper 라는 기능을 써도 기본 생성자는 써줘야 동작한다.
  def this() = {
    this(None, None, None, None, None, None, None, None, None, None, None)
  }

  override def modifyVersion(): StudentModel = copy(version = version.map(_ + 1))
}

object StudentModelEx {
  def apply(id: Option[Int],
    version: Option[Int],
    name: Option[String],
    currentStudentSchoolHistoryId: Option[Int],
    currentStudentContactId: Option[Int],
    currentDuesAmount: Option[Int],
    distanceOfPlannedDuesDay: Option[Int],
    currentAttendingStudent: Option[Boolean]): StudentModel =
    StudentModel(id, version, name,
      currentStudentSchoolHistoryId,
      currentStudentContactId,
      currentDuesAmount,
      distanceOfPlannedDuesDay,
      currentAttendingStudent,
      None, None, None)

  def unapply(m: StudentModel): Option[(Option[Int], Option[Int], Option[String], Option[Int], Option[Int], Option[Int], Option[Int], Option[Boolean])] =
    Option((m.id, m.version, m.name,
      m.currentStudentSchoolHistoryId,
      m.currentStudentContactId,
      m.currentDuesAmount,
      m.distanceOfPlannedDuesDate,
      m.currentAttendingStudent))
}
