/**
 * Copyright (C) 2015 KangWoo,Lee (imcharsi@hotmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.imcharsi.myproject20150316.model20150316

/**
 * Created by i on 3/25/15.
 */
case class SchoolModel(id: Option[Int], override val version: Option[Int], name: Option[String], schoolType: Option[Int]) extends Version[SchoolModel] {
  def this() = this(None, None, None, None)

  override def modifyVersion(): SchoolModel = copy(version = version.map(_ + 1))
}
// 학교의 종류로 초등학교/중학교/고등학교가 있는데, 이것까지 database 에 넣을수도 있지만, 고정이고 바뀔일이 없을것 같다. 학교종류까지는 하지말자.
// 이렇게 하면서 생기는 몇가지 문제가 있는데, 우선 학교 유형별로 학년이 달라지는 문제를 client 에서 직접 풀어야 한다.
// 예를 들면, 매 입력마다 학년 검사를 client 에서 해야 하는 것이다.
// 한편, 학교유형이 고정적이므로, database 검사제약조건에서 이 고정값을 반영하여, 학년범위를 제약할수도 있지만, 과연 좋은 방법인가.
// 일단은, 이렇게까지는 하지 말자.

object SchoolType {
  // 될수 있으면 database 에서 모든 것을 풀어내는 것이 좋겠지만, 바뀔 일이 없어서 일단 이렇게 쓴다.
  val unknown = 0
  val elementarySchool = 1
  val middleSchool = 2
  val highSchool = 3
  val elementarySchoolGradeRange = 1.to(6).toList
  val middleSchoolGradeRange = 1.to(3).toList
  val highSchoolGradeRange = 1.to(3).toList
  val schoolGradeMap = Map((elementarySchool -> elementarySchoolGradeRange), (middleSchool -> middleSchoolGradeRange), (highSchool -> highSchoolGradeRange))
  val schoolTypeList = List(elementarySchool, middleSchool, highSchool)
}

